<?php

namespace commonprj\components\crm\specifications\accessSpecification;

use Yii;
use commonprj\components\crm\entities\permission\Permission;
use commonprj\components\core\entities\element\Element;

/**
 * Class AccessSpecification
 * @package commonprj\components\crm\specifications\accessSpecification
 */
class AccessSpecification extends Element
{
    /**
     * Присвоение свойству доменного слоя $repository - соответствующего компонента
     * @inheritdoc
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);
    }

    /**
     * @param Permission $permission
     * @return bool
     */
    public function isSatisfiedBy(Permission $permission): boolean
    {
        return $this->repository->isSatisfiedBy($this, $permission);
    }
}
