<?php

namespace commonprj\components\crm\entities\companyRole;

use commonprj\components\crm\entities\permissionGroup\PermissionGroup;
use Yii;
use commonprj\components\core\entities\element\Element;

/**
 * Class CompanyRole
 * @property ICompanyRoleRepository $repository
 * @package commonprj\components\crm\entities\companyRole
 */
class CompanyRole extends Element
{
    /**
     * Присвоение свойству доменного слоя $repository - соответствующего компонента
     * @inheritdoc
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->repository = Yii::$app->companyRoleRepository;
    }

    /**
     * @return array
     */
    public function getUserRoles(): array
    {
        return $this->repository->getUserRoles($this);
    }

    /**
     * @param int $permissionGroupId
     * @return bool
     */
    public function bindPermissionGroup(int $permissionGroupId): bool
    {
        return $this->repository->bindPermissionGroup($this, $permissionGroupId);
    }

    /**
     * @param int $permissionGroupId
     * @return bool
     */
    public function unbindPermissionGroup(int $permissionGroupId): bool
    {
        return $this->repository->unbindPermissionGroup($this, $permissionGroupId);
    }

    /**
     * @return PermissionGroup[]
     */
    public function getPermissionGroups(): array
    {
        return $this->repository->getPermissionGroups($this);
    }
}
