<?php

namespace commonprj\components\crm\entities\companyRole;

use Yii;
use commonprj\components\core\entities\element\Element;
use commonprj\components\core\entities\element\ElementDBRepository;
use commonprj\components\core\entities\property\Property;
use commonprj\components\crm\entities\permissionGroup\PermissionGroup;

class CompanyRoleDBRepository extends ElementDBRepository implements ICompanyRoleRepository
{

    /**
     * @param CompanyRole $companyRole
     * @return array
     * @throws \yii\web\HttpException
     */
    public function getUserRoles(CompanyRole $companyRole): array
    {
        $propertyId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($companyRole,
            Property::PROPERTY_SYSNAME__USER_ROLE2COMPANY_ROLE);

        $elements = $companyRole->getAssociatedEntities($propertyId);

        foreach ($elements as &$element) {
            /** @var Element $element */
            $element->properties = $element->getProperties();
        }

        return $elements;
    }

    /**
     * @param CompanyRole $companyRole
     * @param int $permissionGroupId
     * @return bool
     */
    public function bindPermissionGroup(CompanyRole $companyRole, int $permissionGroupId): bool
    {
        $propertyId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($companyRole,
            Property::PROPERTY_SYSNAME__COMPANY_ROLE2PERMISSION_GROUP);

        return $companyRole->bindAssociatedEntity($propertyId, $permissionGroupId);
    }

    /**
     * @param CompanyRole $companyRole
     * @param int $permissionGroupId
     * @return bool
     */
    public function unbindPermissionGroup(CompanyRole $companyRole, int $permissionGroupId): bool
    {
        $propertyId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($companyRole,
            Property::PROPERTY_SYSNAME__COMPANY_ROLE2PERMISSION_GROUP);

        return $companyRole->unbindAssociatedEntity($propertyId, $permissionGroupId);
    }


    /**
     * @param CompanyRole $companyRole
     * @return PermissionGroup[]
     * @throws \yii\web\HttpException
     */
    public function getPermissionGroups(CompanyRole $companyRole): array
    {
        $propertyId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($companyRole,
            Property::PROPERTY_SYSNAME__COMPANY_ROLE2PERMISSION_GROUP);

        $elements = $companyRole->getAssociatedEntities($propertyId);

        foreach ($elements as &$element) {
            /** @var Element $element */
            $element->properties = $element->getProperties();
        }

        return $elements;
    }
}