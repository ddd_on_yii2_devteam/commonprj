<?php

namespace commonprj\components\crm\entities\productOffer;

use Yii;
use commonprj\components\core\entities\element\Element;

class ProductOffer extends Element
{
    /**
     * Присвоение свойству доменного слоя $repository - соответствующего компонента
     * @inheritdoc
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->repository = Yii::$app->productOfferRepository;
    }
}
