<?php

namespace commonprj\components\crm\entities\Seller;

use commonprj\components\crm\entities\company\CompanyServiceRepository;
use commonprj\components\crm\entities\manufacturer\Manufacturer;
use Yii;
use yii\httpclient\Client;

/**
 * Class SellerServiceRepository
 * @package commonprj\components\crm\entities\Seller
 */
class SellerServiceRepository extends CompanyServiceRepository
{
    /**
     * @var Client
     */
    protected $restServer;

    /**
     * @var string
     */
    protected $baseUri = 'seller/';

    /**
     * SellerServiceRepository constructor.
     */
    public function __construct()
    {
        $this->restServer = Yii::$app->crmRestServer;
    }

    /**
     * @param Seller $seller
     * @param int $manufacturerId
     * @return bool
     */
    public function bindManufacturer(Seller $seller, int $manufacturerId)
    {
        $uri = $this->getBaseUri($seller->id . '/manufacturer/' . $manufacturerId);
        $response = $this->restServer->post($uri)->send();
        return $response->getIsOk();
    }

    /**
     * @param Seller $seller
     * @param int $manufacturerId
     * @return bool
     */
    public function unbindManufacturer(Seller $seller, int $manufacturerId)
    {
        $uri = $this->getBaseUri($seller->id . '/manufacturer/' . $manufacturerId);
        $response = $this->restServer->delete($uri)->send();
        return $response->getIsOk();
    }

    /**
     * @param Seller $seller
     * @return array|\commonprj\extendedStdComponents\BaseCrudModel[]|null
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    public function getManufacturers(Seller $seller)
    {
        $this->requestUri = $this->getBaseUri($seller->id . '/manufacturers');
        $this->requestParams = [];
        $content = $this->getApiData();

        if (!empty($content)) {
            $result = $this->getArrayOfModels($content, Manufacturer::className());
        }

        return $result ?? [];
    }


}