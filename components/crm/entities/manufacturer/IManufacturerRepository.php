<?php

namespace commonprj\components\crm\entities\manufacturer;

use commonprj\components\catalog\entities\productMaterial\ProductMaterial;
use commonprj\components\catalog\entities\component\Component;
use commonprj\components\catalog\entities\materialCollection\MaterialCollection;
use commonprj\components\catalog\entities\priceCategory\PriceCategory;
use commonprj\components\catalog\entities\productModel\ProductModel;
use commonprj\components\crm\entities\seller\Seller;

interface IManufacturerRepository
{
    /**
     * @param Manufacturer $manufacturer
     * @return Seller[]
     */
    public function getSellers(Manufacturer $manufacturer): array;

    /**
     * @param Manufacturer $manufacturer
     * @return PriceCategory[]
     */
    public function getPriceCategories(Manufacturer $manufacturer): array;

    /**
     * @param Manufacturer $manufacturer
     * @return ProductMaterial[]
     */
    public function getProductMaterials(Manufacturer $manufacturer): array;

    /**
     * @param Manufacturer $manufacturer
     * @return MaterialCollection[]
     */
    public function getMaterialCollections(Manufacturer $manufacturer): array;

    /**
     * @param Manufacturer $manufacturer
     * @return ProductModel[]
     */
    public function getProductModels(Manufacturer $manufacturer): array;
    /**
     * @param Manufacturer $manufacturer
     * @return Component[]
     */
    public function getComponents(Manufacturer $manufacturer): array;
}