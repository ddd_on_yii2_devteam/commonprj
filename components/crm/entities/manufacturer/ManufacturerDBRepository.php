<?php

namespace commonprj\components\crm\entities\manufacturer;

use commonprj\components\catalog\entities\component\Component;
use Yii;
use commonprj\components\catalog\entities\productMaterial\ProductMaterial;
use commonprj\components\catalog\entities\materialCollection\MaterialCollection;
use commonprj\components\catalog\entities\priceCategory\PriceCategory;
use commonprj\components\catalog\entities\productModel\ProductModel;
use commonprj\components\core\entities\element\Element;
use commonprj\components\core\entities\property\Property;
use commonprj\components\crm\entities\company\CompanyDBRepository;
use commonprj\components\crm\entities\seller\Seller;

class ManufacturerDBRepository extends CompanyDBRepository implements IManufacturerRepository
{
    protected $organizationCodePrefix = "%s.";

    /**
     * @param Manufacturer $manufacturer
     * @return Seller[]
     * @throws \yii\web\HttpException
     */
    public function getSellers(Manufacturer $manufacturer): array
    {
        $propertySeller2ManufacturerId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($manufacturer,
            Property::PROPERTY_SYSNAME__SELLER2MANUFACTURER);

        $sellers = $manufacturer->getAssociatedEntities($propertySeller2ManufacturerId);

        foreach ($sellers as &$seller) {
            /** @var Element $manufacturer */
            $seller->properties = $seller->getProperties();
        }

        return $sellers;
    }

    /**
     * @param Manufacturer $manufacturer
     * @return PriceCategory[]
     * @throws \yii\web\HttpException
     */
    public function getMaterialCollectionPriceCategories(Manufacturer $manufacturer): array
    {
        $propertyId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($manufacturer,
            Property::PROPERTY_SYSNAME__PRICE_CATEGORY2MANUFACTURER);

        return $manufacturer->getAssociatedEntities($propertyId);
    }

    /**
     * @param Manufacturer $manufacturer
     * @return PriceCategory[]
     * @throws \yii\web\HttpException
     */
    public function getPriceCategories(Manufacturer $manufacturer): array
    {
        $propertyId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($manufacturer,
            Property::PROPERTY_SYSNAME__PRICE_CATEGORY2MANUFACTURER);

        return $manufacturer->getAssociatedEntities($propertyId);
    }

    /**
     * @param Manufacturer $manufacturer
     * @return ProductMaterial[]
     */
    public function getProductMaterials(Manufacturer $manufacturer): array
    {
        $propertyId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($manufacturer, Property::PROPERTY_SYSNAME__PRODUCT_MATERIAL2MANUFACTURER);

        return $manufacturer->getAssociatedEntities($propertyId);
    }

    /**
     * @param Manufacturer $manufacturer
     * @return MaterialCollection[]
     * @throws \yii\web\HttpException
     */
    public function getMaterialCollections(Manufacturer $manufacturer): array
    {
        $propertyId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($manufacturer,
            Property::PROPERTY_SYSNAME__MATERIAL_COLLECTION2MANUFACTURER);

        return $manufacturer->getAssociatedEntities($propertyId);
    }

    /**
     * @param Manufacturer $manufacturer
     * @return ProductModel[]
     * @throws \yii\web\HttpException
     */
    public function getProductModels(Manufacturer $manufacturer): array
    {
        $propertyId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($manufacturer,
            Property::PROPERTY_SYSNAME__PRODUCT_MODEL2MANUFACTURER);

        return $manufacturer->getAssociatedEntities($propertyId);
    }

    /**
     * @param Manufacturer $manufacturer
     * @return Component[]
     * @throws \yii\web\HttpException
     */
    public function getComponents(Manufacturer $manufacturer): array
    {
        $propertyId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($manufacturer, Property::PROPERTY_SYSNAME__COMPONENT2MANUFACTURER);
        return $manufacturer->getAssociatedEntities($propertyId);
    }

    protected function getDefaultOrganizationCode()
    {
        return 10000;
    }

    /**
     * @return string
     */
    protected function getEntityClass()
    {
        return Manufacturer::class;
    }

}
