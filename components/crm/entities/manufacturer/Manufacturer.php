<?php

namespace commonprj\components\crm\entities\manufacturer;

use commonprj\components\core\entities\elementClass\ElementClass;
use commonprj\components\core\helpers\ClassAndContextHelper;
use Yii;
use commonprj\components\crm\entities\company\Company;
use commonprj\components\catalog\entities\productMaterial\ProductMaterial;
use commonprj\components\catalog\entities\materialCollection\MaterialCollection;
use commonprj\components\catalog\entities\priceCategory\PriceCategory;
use commonprj\components\catalog\entities\productModel\ProductModel;
use commonprj\components\crm\entities\seller\Seller;

/**
 * Class Manufacturer
 * @property IManufacturerRepository $repository
 * @package commonprj\components\crm\entities\manufacturer
 */
class Manufacturer extends Company
{
    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->repository = Yii::$app->manufacturerRepository;
    }

    /**
     * @return Seller[]
     */
    public function getSellers(): array
    {
        return $this->repository->getSellers($this);
    }

    /**
     * @return PriceCategory[]
     */
    public function getPriceCategories(): array
    {
        return $this->repository->getPriceCategories($this);
    }

    /**
     * @return ProductMaterial[]
     */
    public function getProductMaterials(): array
    {
        return $this->repository->getProductMaterials($this);
    }

    /**
     * @return MaterialCollection[]
     */
    public function getMaterialCollections(): array
    {
        return $this->repository->getMaterialCollections($this);
    }

    /**
     * @return ProductModel[]
     */
    public function getProductModels(): array
    {
        return $this->repository->getProductModels($this);
    }

    /**
     * @return array|\commonprj\components\catalog\entities\component\Component[]
     */
    public function getComponents()
    {
        return $this->repository->getComponents($this);
    }

}
