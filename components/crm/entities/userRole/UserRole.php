<?php

namespace commonprj\components\crm\entities\userRole;

use commonprj\components\crm\entities\companyRole\CompanyRole;
use commonprj\components\crm\entities\permission\Permission;
use Yii;
use commonprj\components\core\entities\element\Element;

/**
 * Class UserRole
 * @property IUserRoleRepository $repository
 * @package commonprj\components\crm\entities\userRole
 */
class UserRole extends Element
{
    /**
     * Присвоение свойству доменного слоя $repository - соответствующего компонента
     * @inheritdoc
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->repository = Yii::$app->userRoleRepository;
    }

    /**
     * @param int $permissionId
     * @return bool
     */
    public function bindPermission(int $permissionId): bool
    {
        return $this->repository->bindPermission($this, $permissionId);
    }

    /**
     * @param int $permissionId
     * @return bool
     */
    public function unbindPermission(int $permissionId): bool
    {
        return $this->repository->unbindPermission($this, $permissionId);
    }

    /**
     * @return Permission[]
     */
    public function getPermissions(): array
    {
        return $this->repository->getPermissions($this);
    }

    /**
     * @param int $companyRoleId
     * @return bool
     */
    public function bindCompanyRole($companyRoleId): bool
    {
        return $this->repository->bindCompanyRole($this, $companyRoleId);
    }

    /**
     * @param int $companyRoleId
     * @return bool
     */
    public function unbindCompanyRole($companyRoleId): bool
    {
        return $this->repository->unbindCompanyRole($this, $companyRoleId);
    }

    /**
     * @return CompanyRole
     */
    public function getCompanyRole(): CompanyRole
    {
        return $this->repository->getCompanyRole($this);
    }
}
