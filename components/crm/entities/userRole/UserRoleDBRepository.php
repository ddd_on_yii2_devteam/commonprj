<?php

namespace commonprj\components\crm\entities\userRole;

use Yii;
use commonprj\components\core\entities\element\Element;
use commonprj\components\core\entities\element\ElementDBRepository;
use commonprj\components\core\entities\property\Property;
use commonprj\components\crm\entities\companyRole\CompanyRole;
use commonprj\components\crm\entities\permission\Permission;

class UserRoleDBRepository extends ElementDBRepository implements IUserRoleRepository
{
    /**
     * @param UserRole $userRole
     * @param int $permissionId
     * @return bool
     */
    public function bindPermission(UserRole $userRole, int $permissionId): bool
    {
        $propertyId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($userRole,
            Property::PROPERTY_SYSNAME__PERMISSION2USER_ROLE);

        return $userRole->bindAssociatedEntity($propertyId, $propertyId);
    }

    /**
     * @param UserRole $userRole
     * @param int $permissionId
     * @return bool
     */
    public function unbindPermission(UserRole $userRole, int $permissionId): bool
    {
        $propertyId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($userRole,
            Property::PROPERTY_SYSNAME__PERMISSION2USER_ROLE);

        return $userRole->unbindAssociatedEntity($propertyId, $propertyId);
    }

    /**
     * @param UserRole $userRole
     * @return Permission[]
     * @throws \yii\web\HttpException
     */
    public function getPermissions(UserRole $userRole): array
    {
        $propertyId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($userRole,
            Property::PROPERTY_SYSNAME__PERMISSION2USER_ROLE);

        $elements = $userRole->getAssociatedEntities($propertyId);

        foreach ($elements as &$element) {
            /** @var Element $element */
            $element->properties = $element->getProperties();
        }

        return $elements;
    }

    /**
     * @param UserRole $userRole
     * @param int $companyRoleId
     * @return bool
     */
    public function bindCompanyRole(UserRole $userRole, int $companyRoleId): bool
    {
        $propertyId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($userRole,
            Property::PROPERTY_SYSNAME__USER_ROLE2COMPANY_ROLE);

        return $userRole->bindAssociatedEntity($propertyId, $propertyId);
    }

    /**
     * @param UserRole $userRole
     * @param int $companyRoleId
     * @return bool
     */
    public function unbindCompanyRole(UserRole $userRole, int $companyRoleId): bool
    {
        $propertyId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($userRole,
            Property::PROPERTY_SYSNAME__USER_ROLE2COMPANY_ROLE);

        return $userRole->unbindAssociatedEntity($propertyId, $propertyId);
    }


    /**
     * @param UserRole $userRole
     * @return CompanyRole
     * @throws \yii\web\HttpException
     */
    public function getCompanyRole(UserRole $userRole): CompanyRole
    {
        $propertyId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($userRole,
            Property::PROPERTY_SYSNAME__USER_ROLE2COMPANY_ROLE);

        $elements = $userRole->getAssociatedEntities($propertyId);

        foreach ($elements as &$element) {
            /** @var Element $element */
            $element->properties = $element->getProperties();
        }

        return $elements;
    }
}