<?php

namespace commonprj\components\crm\entities\userRole;

use commonprj\components\crm\entities\companyRole\CompanyRole;
use commonprj\components\crm\entities\permission\Permission;

interface IUserRoleRepository
{
    /**
     * @param UserRole $userRole
     * @param int $permissionId
     * @return bool
     */
    public function bindPermission(UserRole $userRole, int $permissionId): bool;

    /**
     * @param UserRole $userRole
     * @param int $permissionId
     * @return bool
     */
    public function unbindPermission(UserRole $userRole, int $permissionId): bool;

    /**
     * @param UserRole $userRole
     * @return Permission[]
     */
    public function getPermissions(UserRole $userRole): array;

    /**
     * @param UserRole $userRole
     * @param int $companyRoleId
     * @return bool
     */
    public function bindCompanyRole(UserRole $userRole, int $companyRoleId): bool;

    /**
     * @param UserRole $userRole
     * @param int $companyRoleId
     * @return bool
     */
    public function unbindCompanyRole(UserRole $userRole, int $companyRoleId): bool;

    /**
     * @param UserRole $userRole
     * @return CompanyRole
     */
    public function getCompanyRole(UserRole $userRole): CompanyRole;

}