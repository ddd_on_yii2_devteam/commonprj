<?php
/**
 * @package         FurniPrice
 * @subpackage      CRM
 * @category        Entity
 * @created         2018.01.22
 * @author          Vasiliy Konakov
 * @updated         2018.01.23
 * @updatedBy       Vasiliy Konakov
 */

namespace commonprj\components\crm\entities\customer;

use Yii;
use yii\db\Exception;
use yii\web\HttpException;
use commonprj\components\core\models\ElementRecord;
use commonprj\components\core\entities\property\Property;
use commonprj\components\crm\entities\user\UserDBRepository;
use commonprj\components\crm\entities\productRequest\ProductRequest;

class CustomerDBRepository extends UserDBRepository implements ICustomerRepository
{

    /**
     * getProductRequests
     * @param Customer $element
     * @return ProductRequest[]
     * @throws HttpException|Exception
     */
    public function getProductRequests(Customer $element): array
    {
        $result = [];
        foreach ($this->getAssociatedEntities($element,
            Yii::$app->propertyService->getPropertyIdByElementAndSysname($element,
                Property::PROPERTY_SYSNAME__PRODUCT_REQUEST2CUSTOMER)) as $item) {
            $result[$element->id] = self::instantiateByARAndClassName(
                ElementRecord::findOne(['id' => $item->id]), ProductRequest::class);
        }
        return $result;
    }

}