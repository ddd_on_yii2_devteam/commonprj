<?php
/**
 * @package         FurniPrice
 * @subpackage      CRM
 * @category        Entity
 * @created         2018.01.22
 * @author          Vasiliy Konakov
 * @updated         2018.01.23
 * @updatedBy       Vasiliy Konakov
 */

namespace commonprj\components\crm\entities\customer;

use yii\db\Exception;
use yii\web\HttpException;
use commonprj\components\crm\entities\productRequest\ProductRequest;

/**
 * Class ICustomerRepository
 * @package commonprj\components\crm\entities\customer
 */
interface ICustomerRepository
{

    /**
     * getProductRequests
     * @param Customer $element
     * @param int $propertyId
     * @return ProductRequest[]
     * @throws HttpException|Exception
     */
    public function getProductRequests(Customer $element): array;

}
