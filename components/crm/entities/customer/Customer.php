<?php
/**
 * @package         FurniPrice
 * @subpackage      CRM
 * @category        Entity
 * @created         2018.01.22
 * @author          Vasiliy Konakov
 * @updated         2018.01.23
 * @updatedBy       Vasiliy Konakov
 */

namespace commonprj\components\crm\entities\customer;

use Yii;
use yii\db\Exception;
use yii\web\HttpException;
use commonprj\components\crm\entities\user\User;
use commonprj\components\crm\entities\productRequest\ProductRequest;

class Customer extends User
{

    /**
     * @var CustomerDBRepository $repository
     */
    public $repository;

    /**
     * Присвоение свойству доменного слоя $repository - соответствующего компонента
     * @inheritdoc
     */

    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->repository = Yii::$app->customerRepository;
    }

    /**
     * getProductRequests
     * @return ProductRequest[]
     * @throws HttpException|Exception
     */
    public function getProductRequests(): array
    {
        return $this->repository->getProductRequests($this);
    }

}
