<?php
/**
 * @package         FurniPrice
 * @subpackage      CRM
 * @category        Entity
 * @created         2018.01.22
 * @author          Vasiliy Konakov
 * @updated         2018.01.23
 * @updatedBy       Vasiliy Konakov
 */

namespace commonprj\components\crm\entities\user;

/**
 * Class IEmployeeRepository
 * @package commonprj\components\crm\entities\employee
 */
interface IUserRepository
{


    /**
     * bindAddress
     * @param User $element
     * @param int $userId
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|Exception
     */
    public function bindAddress(User $element, $userId): bool;

    /**
     * unbindAddress
     * @param User $element
     * @param int $userId
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|Exception
     */
    public function unbindAddress(User $element, $userId): bool;

    /**
     * @param User $element
     * @param int|null $addressTypeId
     * @return array
     */
    public function getAddresses(User $element, int $addressTypeId = null): array;

    /**
     * bindUserRole
     * @param User $element
     * @param int $userId
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|Exception
     */
    public function bindUserRole(User $element, $userId): bool;

    /**
     * unbindUserRole
     * @param User $element
     * @param int $userId
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|Exception
     */
    public function unbindUserRole(User $element, $userId): bool;

    /**
     * getUserRole
     * @param User $element
     * @return null|UserRole
     * @throws HttpException|Exception
     */
    public function getUserRole(User $element): ?UserRole;

    /**
     * getAccessSpecification
     * @param User $element
     * @return null|AccessSpecification
     * @throws HttpException|Exception
     */
    public function getAccessSpecification(User $element): ?AccessSpecification;
}
