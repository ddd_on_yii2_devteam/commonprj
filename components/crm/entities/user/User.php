<?php
/**
 * @package         FurniPrice
 * @subpackage      CRM
 * @category        Entity
 * @created         2018.01.22
 * @author          Vasiliy Konakov
 * @updated         2018.01.23
 * @updatedBy       Vasiliy Konakov
 */

namespace commonprj\components\crm\entities\user;

use Yii;
use yii\db\Exception;
use yii\web\HttpException;
use commonprj\components\core\entities\element\Element;
use commonprj\components\crm\entities\accessSpecification\AccessSpecification;
use commonprj\components\crm\entities\address\Address;
use commonprj\components\crm\entities\userRole\UserRole;

abstract class User extends Element
{

    /**
     * bindAddress
     * @param int $addressId
     * @return bool
     * @throws HttpException|Exception
     */
    public function bindAddress(int $addressId): bool
    {
        return $this->repository->bindAddress($this, $addressId);
    }

    /**
     * unbindAddress
     * @param int $addressId
     * @return bool
     * @throws HttpException|Exception
     */
    public function unbindAddress(int $addressId): bool
    {
        return $this->repository->unbindAddress($this, $addressId);
    }

    /**
     * @param int|null $addressTypeId
     * @return array
     */
    public function getAddresses(int $addressTypeId = null): array
    {
        return $this->repository->getAddresses($this, $addressTypeId);
    }

    /**
     * getAccessSpecification
     */
    public function getAccessSpecification(): ?AccessSpecification
    {
        return $this->repository->getAccessSpecification();
    }

    /**
     * bindUserRole
     * @param int $userRole
     * @return bool
     * @throws HttpException|Exception
     */
    public function bindUserRole(int $userRole): bool
    {
        return $this->repository->bindUserRole($this, $userRole);
    }

    /**
     * unbindUserRole
     * @param int $userRole
     * @return bool
     * @throws HttpException|Exception
     */
    public function unbindUserRole(int $userRole): bool
    {
        return $this->repository->unbindUserRole($this, $userRole);
    }

    /**
     * getUserRole
     * @return null|UserRole
     * @throws HttpException|Exception
     */
    public function getUserRole(): ?UserRole
    {
        return $this->repository->getUserRole($this);
    }

}
