<?php

namespace commonprj\components\crm\entities\user;

use commonprj\components\crm\entities\address\Address;
use Yii;
use commonprj\components\core\entities\element\ElementDBRepository;
use commonprj\components\core\entities\property\Property;

abstract class UserDBRepository extends ElementDBRepository implements IUserRepository
{
    /**
     * @param User $element
     * @param int $userId
     * @return bool
     * @throws \commonprj\components\core\entities\element\Exception
     * @throws \yii\web\HttpException
     */
    public function bindAddress(User $element, $userId): bool
    {
        $propertyUser2AddressId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($element, Property::PROPERTY_SYSNAME__USER2ADDRESS);
        return $element->bindAssociatedEntity($propertyUser2AddressId, $userId);
    }

    /**
     * @param User $element
     * @param int $userId
     * @return bool
     * @throws \commonprj\components\core\entities\element\Exception
     * @throws \yii\web\HttpException
     */
    public function unbindAddress(User $element, $userId): bool
    {
        $propertyUser2AddressId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($element, Property::PROPERTY_SYSNAME__USER2ADDRESS);
        return $element->bindAssociatedEntity($propertyUser2AddressId, $userId);
    }

    /**
     * @param User $element
     * @param int|null $addressTypeId
     * @return array
     * @throws \yii\web\HttpException
     */
    public function getAddresses(User $element, int $addressTypeId = null): array
    {
        $propertyUser2AddressId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($element, Property::PROPERTY_SYSNAME__USER2ADDRESS);
        $addresses = $element->getAssociatedEntities($propertyUser2AddressId);

        if ($addressTypeId) {
            $addresses = $this->filterAddressesByTypeId($addresses, $addressTypeId);
        }

        return $addresses;

    }

    /**
     * @param User $element
     * @param int $userId
     * @return bool
     * @throws \commonprj\components\core\entities\element\Exception
     * @throws \yii\web\HttpException
     */
    public function bindUserRole(User $element, $userId): bool
    {
        $propertyUser2UserRoleId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($element, Property::PROPERTY_SYSNAME__USER2USER_ROLE);
        return $element->bindAssociatedEntity($propertyUser2UserRoleId, $userId);
    }

    /**
     * @param User $element
     * @param int $userId
     * @return bool
     * @throws \commonprj\components\core\entities\element\Exception
     * @throws \yii\web\HttpException
     */
    public function unbindUserRole(User $element, $userId): bool
    {
        $propertyUser2UserRoleId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($element, Property::PROPERTY_SYSNAME__USER2USER_ROLE);
        return $element->bindAssociatedEntity($propertyUser2UserRoleId, $userId);
    }

    /**
     * @param User $element
     * @return UserRole|null
     * @throws \yii\web\HttpException
     */
    public function getUserRole(User $element): ?UserRole
    {
        $propertyUser2UserRoleId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($element, Property::PROPERTY_SYSNAME__USER2USER_ROLE);
        $userRole = $element->getAssociatedEntity($propertyUser2UserRoleId);
        $userRole->properties = $userRole->getProperties();

        return $userRole;
    }

    /**
     * @param User $element
     * @return AccessSpecification|null
     * @throws \yii\web\HttpException
     */
    public function getAccessSpecification(User $element): ?AccessSpecification
    {
        $propertyUser2AccessSpecificationId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($element, Property::PROPERTY_SYSNAME__USER2ACCESS_SPECIFICATION);
        $accessSpecification = $element->getAssociatedEntity($propertyUser2AccessSpecificationId);
        $accessSpecification->properties = $accessSpecification->getProperties();

        return $accessSpecification;
    }

    /**
     * @param array $addresses
     * @param $typeId
     * @return array
     */
    private function filterAddressesByTypeId(array $addresses, $typeId) : array
    {
        $result = [];

        foreach ($addresses as /** @var Address $address  */$address)
        {
            $property = $address->getPropertyFromPropertiesBySysname('addressType');
            if ($property->value == [$typeId]) {
                $result[] = $address;
            }
        }

        return $result;
    }
}