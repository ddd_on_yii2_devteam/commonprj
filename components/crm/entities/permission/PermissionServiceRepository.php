<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 1/19/2018
 * Time: 3:20 PM
 */

namespace commonprj\components\crm\entities\permission;


use commonprj\components\crm\entities\permissionGroup\PermissionGroup;

class PermissionServiceRepository implements IPermissionRepository
{
    /**
     * @param Permission $permission
     * @param int $permissionGroupId
     * @return bool
     */
    public function bindPermissionGroup(Permission $permission, int $permissionGroupId): bool
    {
        // TODO: Implement bindPermissionGroup() method.
    }

    /**
     * @param Permission $permission
     * @param int $permissionGroupId
     * @return bool
     */
    public function unbindPermissionGroup(Permission $permission, int $permissionGroupId): bool
    {
        // TODO: Implement unbindPermissionGroup() method.
    }

    /**
     * @param Permission $permission
     * @return PermissionGroup
     */
    public function getPermissionGroup(Permission $permission): PermissionGroup
    {
        // TODO: Implement getPermissionGroup() method.
    }


}