<?php

namespace commonprj\components\crm\entities\address;

use commonprj\components\core\entities\property\Property;
use Yii;
use commonprj\components\core\entities\element\Element;

class Address extends Element
{

    const TYPE_LEGAL = 1;
    const TYPE_POSTAL = 2;
    const TYPE_ACTUAL = 3;
    const TYPE_CUSTOM = 4;
    /**
     * Possible address types
     * @var type 
     */
    public $types  = [
        'legal'     => self::TYPE_LEGAL,
        'postal'    => self::TYPE_POSTAL, 
        'actual'    => self::TYPE_ACTUAL, 
        'custom'    => self::TYPE_CUSTOM
    ];

    /**
     * Присвоение свойству доменного слоя $repository - соответствующего компонента
     * @inheritdoc
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->repository = Yii::$app->addressRepository;
    }
}