<?php

namespace commonprj\components\crm\entities\companyState;

use commonprj\components\crm\entities\permissionGroup\PermissionGroup;

interface ICompanyStateRepository
{
    /**
     * @param CompanyState $companyState
     * @param int $permissionGroupId
     * @return bool
     */
    public function bindPermissionGroup(CompanyState $companyState, int $permissionGroupId): bool;

    /**
     * @param CompanyState $companyState
     * @param int $permissionGroupId
     * @return bool
     */
    public function unbindPermissionGroup(CompanyState $companyState, int $permissionGroupId): bool;

    /**
     * @param CompanyState $companyState
     * @return PermissionGroup[]
     */
    public function getPermissionGroups(CompanyState $companyState): array;

}