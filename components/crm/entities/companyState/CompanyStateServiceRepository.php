<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 1/19/2018
 * Time: 2:58 PM
 */

namespace commonprj\components\crm\entities\companyState;


use commonprj\components\crm\entities\permissionGroup\PermissionGroup;

class CompanyStateServiceRepository implements ICompanyStateRepository
{
    /**
     * @param CompanyState $companyState
     * @param int $permissionGroupId
     * @return bool
     */
    public function bindPermissionGroup(CompanyState $companyState, int $permissionGroupId): bool
    {
        // TODO: Implement bindPermissionGroup() method.
    }

    /**
     * @param CompanyState $companyState
     * @param int $permissionGroupId
     * @return bool
     */
    public function unbindPermissionGroup(CompanyState $companyState, int $permissionGroupId): bool
    {
        // TODO: Implement unbindPermissionGroup() method.
    }


    /**
     * @param CompanyState $companyState
     * @return PermissionGroup[]
     */
    public function getPermissionGroups(CompanyState $companyState): array
    {
        // TODO: Implement getPermissionGroups() method.
    }

}