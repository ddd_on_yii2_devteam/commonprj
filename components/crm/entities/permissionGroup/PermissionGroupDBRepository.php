<?php

namespace commonprj\components\crm\entities\permissionGroup;

use Yii;
use commonprj\components\core\entities\element\Element;
use commonprj\components\core\entities\element\ElementDBRepository;
use commonprj\components\core\entities\elementClass\ElementClass;
use commonprj\components\core\entities\property\Property;
use commonprj\components\crm\entities\permission\Permission;

class PermissionGroupDBRepository extends ElementDBRepository implements IPermissionGroupRepository
{
    /**
     * @param PermissionGroup $permissionGroup
     * @param $elementClassId
     * @return bool
     */
    public function bindElementClass(PermissionGroup $permissionGroup, $elementClassId): bool
    {
        $propertyId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($permissionGroup,
            Property::PROPERTY_SYSNAME__PERMISSION_GROUP2ELEMENT_CLASS);

        return $permissionGroup->bindAssociatedEntity($propertyId, $propertyId);
    }

    /**
     * @param PermissionGroup $permissionGroup
     * @param $elementClassId
     * @return bool
     */
    public function unbindElementClass(PermissionGroup $permissionGroup, $elementClassId): bool
    {
        $propertyId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($permissionGroup,
            Property::PROPERTY_SYSNAME__PERMISSION_GROUP2ELEMENT_CLASS);

        return $permissionGroup->unbindAssociatedEntity($propertyId, $propertyId);
    }


    /**
     * @param PermissionGroup $permissionGroup
     * @return ElementClass
     */
    public function getElementClass(PermissionGroup $permissionGroup): ElementClass
    {
        $propertyId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($permissionGroup,
            Property::PROPERTY_SYSNAME__PERMISSION_GROUP2ELEMENT_CLASS);

        $elements = $permissionGroup->getAssociatedEntities($propertyId);

        foreach ($elements as &$element) {
            /** @var Element $element */
            $element->properties = $element->getProperties();
        }

        return $elements;
    }

    /**
     * @param PermissionGroup $permissionGroup
     * @return Permission[]
     */
    public function getPermissions(PermissionGroup $permissionGroup): array
    {
        $propertyId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($permissionGroup,
            Property::PROPERTY_SYSNAME__PERMISSION2PERMISSION_GROUP);

        $elements = $permissionGroup->getAssociatedEntities($propertyId);

        foreach ($elements as &$element) {
            /** @var Element $element */
            $element->properties = $element->getProperties();
        }

        return $elements;
    }
}