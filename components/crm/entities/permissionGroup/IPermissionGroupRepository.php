<?php

namespace commonprj\components\crm\entities\permissionGroup;


use commonprj\components\core\entities\elementClass\ElementClass;
use commonprj\components\crm\entities\permission\Permission;

interface IPermissionGroupRepository
{
    /**
     * @param PermissionGroup $permissionGroup
     * @param $elementClassId
     * @return bool
     */
    public function bindElementClass(PermissionGroup $permissionGroup, $elementClassId): bool;

    /**
     * @param PermissionGroup $permissionGroup
     * @param $elementClassId
     * @return bool
     */
    public function unbindElementClass(PermissionGroup $permissionGroup, $elementClassId): bool;

    /**
     * @param PermissionGroup $permissionGroup
     * @return ElementClass
     */
    public function getElementClass(PermissionGroup $permissionGroup): ElementClass;

    /**
     * @param PermissionGroup $permissionGroup
     * @return Permission[]
     */
    public function getPermissions(PermissionGroup $permissionGroup): array;

}