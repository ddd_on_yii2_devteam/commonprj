<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 1/19/2018
 * Time: 3:31 PM
 */

namespace commonprj\components\crm\entities\permissionGroup;


use commonprj\components\core\entities\elementClass\ElementClass;
use commonprj\components\crm\entities\permission\Permission;

class PermissionGroupServiceRepository implements IPermissionGroupRepository
{
    /**
     * @param PermissionGroup $permissionGroup
     * @param $elementClassId
     * @return bool
     */
    public function bindElementClass(PermissionGroup $permissionGroup, $elementClassId): bool
    {
        // TODO: Implement bindElementClass() method.
    }

    /**
     * @param PermissionGroup $permissionGroup
     * @param $elementClassId
     * @return bool
     */
    public function unbindElementClass(PermissionGroup $permissionGroup, $elementClassId): bool
    {
        // TODO: Implement unbindElementClass() method.
    }

    /**
     * @param PermissionGroup $permissionGroup
     * @return ElementClass
     */
    public function getElementClass(PermissionGroup $permissionGroup): ElementClass
    {
        // TODO: Implement getElementClass() method.
    }

    /**
     * @param PermissionGroup $permissionGroup
     * @return Permission[]
     */
    public function getPermissions(PermissionGroup $permissionGroup): array
    {
        // TODO: Implement getPermissions() method.
    }

}