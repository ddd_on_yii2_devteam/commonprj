<?php

namespace commonprj\components\crm\entities\Employee;

use commonprj\components\crm\entities\company\Company;
use commonprj\components\crm\entities\productOffer\ProductOffer;
use commonprj\components\crm\entities\user\UserServiceRepository;
use commonprj\extendedStdComponents\BaseServiceRepository;
use Yii;
use yii\httpclient\Client;

/**
 * Class EmployeeServiceRepository
 * @package commonprj\components\crm\entities\Employee
 */
class EmployeeServiceRepository extends UserServiceRepository
{
    /**
     * @var Client
     */
    protected $restServer;

    /**
     * @var string
     */
    protected $baseUri = 'employee/';

    /**
     * EmployeeServiceRepository constructor.
     */
    public function __construct()
    {
        $this->restServer = Yii::$app->crmRestServer;
    }

    /**
     * @param Employee $employee
     * @param int $companyId
     * @return bool
     */
    public function bindCompany(Employee $employee, int $companyId)
    {
        $uri = $this->getBaseUri($employee->id . '/company/' . $companyId);
        $response = $this->restServer->post($uri)->send();
        return $response->getIsOk();
    }

    /**
     * @param Employee $employee
     * @param int $companyId
     * @return bool
     */
    public function unbindCompany(Employee $employee, int $companyId)
    {
        $uri = $this->getBaseUri($employee->id . '/company/' . $companyId);
        $response = $this->restServer->delete($uri)->send();
        return $response->getIsOk();
    }

    /**
     * @param Employee $employee
     * @return \commonprj\extendedStdComponents\BaseCrudModel|null
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    public function getCompany(Employee $employee)
    {
        $this->requestUri = $this->getBaseUri($employee->id . '/company');
        $this->requestParams = [];
        $content = $this->getApiData();

        if (!empty($content)) {
            $result = $this->getOneModel($content, Company::className());
        }

        return $result ?? null;
    }

    /**
     * @param Employee $employee
     * @return array|\commonprj\extendedStdComponents\BaseCrudModel[]|null
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    public function getProductOffers(Employee $employee)
    {
        $this->requestUri = $this->getBaseUri($employee->id . '/product-offers');
        $this->requestParams = [];
        $content = $this->getApiData();

        if (!empty($content)) {
            $result = $this->getArrayOfModels($content, ProductOffer::className());
        }

        return $result ?? [];
    }

}