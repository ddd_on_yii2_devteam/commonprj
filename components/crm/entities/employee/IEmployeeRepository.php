<?php
/**
 * @package         FurniPrice
 * @subpackage      CRM
 * @category        Entity
 * @created         2018.01.22
 * @author          Vasiliy Konakov
 * @updated         2018.01.23
 * @updatedBy       Vasiliy Konakov
 */

namespace commonprj\components\crm\entities\employee;

use commonprj\components\crm\entities\company\Company;
use yii\db\Exception;
use yii\web\HttpException;
use commonprj\components\crm\entities\productOffer\ProductOffer;

/**
 * Class IEmployeeRepository
 * @package commonprj\components\crm\entities\employee
 */
interface IEmployeeRepository
{


    /**
     * bindCompany
     * @param Employee $element
     * @param int $companyId
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|Exception
     */
    public function bindCompany(Employee $element, $companyId): bool;

    /**
     * unbindCompany
     * @param Employee $element
     * @param int $companyId
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|Exception
     */
    public function unbindCompany(Employee $element, $companyId): bool;

    /**
     * getCompany
     * @param Employee $element
     * @return null|AbstractCompany
     * @throws HttpException|Exception
     */
    public function getCompany(Employee $element): ?Company;

    /**
     * getProductOffers
     * @param Employee $element
     * @return ProductOffer[]
     * @throws HttpException|Exception
     */
    public function getProductOffers(Employee $element): array;

}
