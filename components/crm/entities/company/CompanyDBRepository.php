<?php

namespace commonprj\components\crm\entities\company;

use commonprj\extendedStdComponents\IBaseCrudModel;
use Yii;
use commonprj\components\core\entities\element\ElementDBRepository;
use commonprj\components\core\entities\property\Property;
use commonprj\components\crm\entities\companyRole\CompanyRole;
use commonprj\components\crm\entities\companyState\CompanyState;
use commonprj\components\crm\entities\employee\Employee;

abstract class CompanyDBRepository extends ElementDBRepository implements ICompanyRepository
{
    private const MANUFACTURER_TYPE = 1;
    private const SELLER_TYPE = 2;
    private const SELLER_MANUFACTURER_TYPE = 3;

    protected $propertyIdMap = null;

    protected $organizationCodePrefix = "DEFAULT.";

    /**
     * @param Company $company
     * @param int $addressId
     * @return bool
     * @throws \yii\web\HttpException
     */
    public function bindAddress(Company $company, int $addressId): bool
    {
        $propertyId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($company,
            Property::PROPERTY_SYSNAME__COMPANY2ADDRESS);

        return $company->bindAssociatedEntity($propertyId, $addressId);
    }

    /**
     * @param Company $company
     * @param int $addressId
     * @return bool
     */
    public function unbindAddress(Company $company, int $addressId): bool
    {
        $propertyId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($company,
            Property::PROPERTY_SYSNAME__COMPANY2ADDRESS);

        return $company->unbindAssociatedEntity($propertyId, $addressId);
    }


    /**
     * @param Company $company
     * @param int|null $typeId
     * @return array
     */
    public function getAddresses(Company $company, int $typeId = null): array
    {
        $propertyAddressId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($company,
            Property::PROPERTY_SYSNAME__COMPANY2ADDRESS);

        $addresses = $company->getAssociatedEntities($propertyAddressId);

        if (count($addresses)) {
            $result = $this->filterAddress($addresses, $typeId);
        }

        return $result ?? [];
    }

    /**
     * @param Company $company
     * @param int $companyRoleId
     * @return bool
     * @throws \yii\web\HttpException
     */
    public function bindCompanyRole(Company $company, int $companyRoleId): bool
    {
        $propertyId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($company,
            Property::PROPERTY_SYSNAME__COMPANY2COMPANY_ROLE);

        return $company->bindAssociatedEntity($propertyId, $companyRoleId);
    }

    /**
     * @param Company $company
     * @param int $companyRoleId
     * @return bool
     */
    public function unbindCompanyRole(Company $company, int $companyRoleId): bool
    {
        $propertyId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($company,
            Property::PROPERTY_SYSNAME__COMPANY2COMPANY_ROLE);

        return $company->unbindAssociatedEntity($propertyId, $companyRoleId);
    }


    /**
     * @param Company $company
     * @return CompanyRole
     */
    public function getCompanyRole(Company $company): CompanyRole
    {
        $propertyId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($company,
            Property::PROPERTY_SYSNAME__COMPANY2COMPANY_ROLE);

        return reset($company->getAssociatedEntities($propertyId));
    }

    /**
     * @param Company $company
     * @param int $companyStateId
     * @return bool
     * @throws \yii\web\HttpException
     */
    public function bindCompanyState(Company $company, int $companyStateId): bool
    {
        $propertyId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($company,
            Property::PROPERTY_SYSNAME__COMPANY2COMPANY_STATE);

        return $company->bindAssociatedEntity($propertyId, $companyStateId);
    }

    /**
     * @param Company $company
     * @param int $companyStateId
     * @return bool
     */
    public function unbindCompanyState(Company $company, int $companyStateId): bool
    {
        $propertyId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($company,
            Property::PROPERTY_SYSNAME__COMPANY2COMPANY_STATE);

        return $company->unbindAssociatedEntity($propertyId, $companyStateId);
    }

    /**
     * @param Company $company
     * @return CompanyState
     */
    public function getCompanyState(Company $company): CompanyState
    {
        $propertyId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($company,
            Property::PROPERTY_SYSNAME__COMPANY2COMPANY_STATE);

        return reset($company->getAssociatedEntities($propertyId));
    }


    /**
     * @param array $addresses
     * @param int $typeId
     * @return array
     */
    private function filterAddress(array $addresses, int $typeId = null): array
    {
        $address = reset($addresses);

        $propertyAddressTypeId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($address, Property::PROPERTY_SYSNAME__ADDRESS_TYPE);

        foreach ($addresses as $address) {
            $address->properties = $address->getProperties();
            foreach ($address->properties as $property) {
                if ($property->id == $propertyAddressTypeId) {
                    $propertyType = $property;
                    break;
                }
            }

            if (!$typeId || $propertyType->value == $typeId) {
                $result[] = $address;
            }
        }

        return $result ?? [];
    }

    /**
     * @param Company $company
     * @return Employee[]
     */
    public function getEmployees(Company $element): array
    {
        foreach ($this->getChildren($element,
            Yii::$app->propertyService->getPropertyIdByElementAndSysname($element, Property::PROPERTY_SYSNAME__EMPLOYEE2COMPANY)) as $item) {
            $result[$item->id] = Employee::findOne($item->id);
        }
        return $result ?? [];
    }

    /**
     * @param Company $company
     * @param int $employeeId
     * @return boolean
     */
    public function bindEmployee(Company $element, int $employeeId): bool
    {
        return $this->bindChild($element,
            Yii::$app->propertyService->getPropertyIdByElementAndSysname($element, Property::PROPERTY_SYSNAME__EMPLOYEE2COMPANY), $employeeId);
    }

    /**
     * @param Company $company
     * @param int $employeeId
     * @return boolean
     */
    public function unbindEmployee(Company $element, int $employeeId): bool
    {
        return $this->unbindChild($element,
            Yii::$app->propertyService->getPropertyIdByElementAndSysname($element, Property::PROPERTY_SYSNAME__EMPLOYEE2COMPANY), $employeeId);
    }

    /**
     * Сохранение переданного объекта Element через ActiveRecord. Запись идет транзакцией в 2 таблицы:
     * element и element2element_class
     * @param Element $element
     * @return bool
     * @throws HttpException
     * @throws \yii\db\Exception
     */
    public function save(IBaseCrudModel $element, $runValidation = true, $attributes = null): bool
    {
        if ($element->isNewRecord()) {
            $organizationCode = $element->properties['organizationCode'] ?? null;
            if (!$organizationCode) {
                $organizationCode = $this->generateOrganizationCode($element->properties['country']);
            }
            $element->properties['organizationCode'] = $organizationCode;

        } else {
            foreach ($element->properties as $key => $property) {
                if ($property instanceof Property && $property->sysname == 'organizationCode') {
                    unset($element->properties[$key]);
                    break;
                }
            }
            unset($element->properties['organizationCode']);
        }

        return parent::save($element, $runValidation, $attributes); // TODO: Change the autogenerated stub
    }

    protected function generateOrganizationCode($country)
    {
        $country = is_array($country) ? reset($country) : $country;

        $lastNumber = $this->getlastNumber($country);
        $newNumber = $lastNumber + 1;
        $companyPrefix = $this->getOrganizationCodePrefix($country);
        $newCode = $companyPrefix . $newNumber;

        return $newCode;
    }

    /**
     * @throws \Exception
     */
    protected function getEntityClass()
    {
        throw new \Exception('method must be override', 500);
    }

    /**
     * @param $country
     * @throws \Exception
     */
    protected function getOrganizationCodePrefix($country)
    {
        return sprintf($this->organizationCodePrefix, strtoupper($country));
    }

    /**
     * @throws \Exception
     */
    protected function getLastNumber($country)
    {
        $companyPrefix = $this->getOrganizationCodePrefix($country);
        $entityClass = $this->getEntityClass();
        $company = $entityClass::find()
            ->where(['like', 'organizationCode', $companyPrefix])
            ->orderBy(['property.organizationCode' => SORT_DESC])
            ->one();

        if ($company) {
            foreach (($company->properties) as $property) {
                if ($property->sysname == 'organizationCode') {
                    list(, $lastNumber) = explode($companyPrefix, $property->value);
                    break;
                }
            }
        } else {
            $lastNumber = $this->getDefaultOrganizationCode();
        }

        return $lastNumber;
    }
}
