<?php

namespace commonprj\components\crm\entities\company;

use commonprj\components\core\entities\element\Element;
use commonprj\components\core\entities\elementClass\ElementClass;
use commonprj\components\core\helpers\ClassAndContextHelper;
use commonprj\components\crm\entities\companyRole\CompanyRole;
use commonprj\components\crm\entities\companyState\CompanyState;

/**
 * Class Company
 * @property ICompanyRepository $repository
 * @package commonprj\components\crm\entities\company
 */
abstract class Company extends Element
{

    const TYPE_MANUFACTURER = 1;
    const TYPE_SELLER = 2;
    const TYPE_SELLER_MANUFACTURER = 3;

    /**
     * Possible company types
     * @var type
     */
    public $types = [
        'manufacturer'        => self::TYPE_MANUFACTURER,
        'seller'              => self::TYPE_SELLER,
        'seller_manufacturer' => self::TYPE_SELLER_MANUFACTURER,
    ];

    /**
     * @param int $addressId
     * @return bool
     */
    public function bindAddress(int $addressId): bool
    {
        return $this->repository->bindAddress($this, $addressId);
    }

    /**
     * @param int $addressId
     * @return bool
     */
    public function unbindAddress(int $addressId): bool
    {
        return $this->repository->unbindAddress($this, $addressId);
    }

    /**
     * @param int|null $typeId
     * @return array
     */
    public function getAddresses(int $typeId = null)
    {
        return $this->repository->getAddresses($this, $typeId);
    }

    /**
     * @param int $companyRoleId
     * @return bool
     */
    public function bindCompanyRole(int $companyRoleId): bool
    {
        return $this->repository->bindCompanyRole($this, $companyRoleId);
    }

    /**
     * @param int $companyRoleId
     * @return bool
     */
    public function unbindCompanyRole(int $companyRoleId): bool
    {
        return $this->repository->unbindCompanyRole($this, $companyRoleId);
    }

    /**
     * @return CompanyRole
     */
    public function getCompanyRole()
    {
        return $this->repository->getCompanyRole($this);
    }

    /**
     * @param int $companyStateId
     * @return bool
     */
    public function bindCompanyState(int $companyStateId): bool
    {
        return $this->repository->bindCompanyRole($this, $companyStateId);
    }

    /**
     * @param int $companyStateId
     * @return bool
     */
    public function unbindCompanyState(int $companyStateId): bool
    {
        return $this->repository->unbindCompanyRole($this, $companyStateId);
    }

    /**
     * @return CompanyState
     */
    public function getCompanyState()
    {
        return $this->repository->getCompanyState($this);
    }

    /**
     * @param Company $company
     * @return Employee[]
     */
    public function getEmployees(): array
    {
        return $this->repository->getEmployees($this);
    }

    /**
     * @param Company $company
     * @param int $employeeId
     * @return boolean
     */
    public function bindEmployee(int $employeeId): bool
    {
        return $this->repository->bindEmployee($this, $employeeId);
    }

    /**
     * @param Company $company
     * @param int $employeeId
     * @return boolean
     */
    public function unbindEmployee(int $employeeId): bool
    {
        return $this->repository->unbindEmployee($this, $employeeId);
    }

}
