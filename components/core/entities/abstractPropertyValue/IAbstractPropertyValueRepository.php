<?php
/**
 * @package         FurniPrice
 * @subpackage      Core
 * @category        DBRepository
 * @created         17.06.2016
 * @author          daSilva.Rodrigues
 * @refactoredBy    Vasiliy Konakov
 * @updated         13.12.2017
 */

namespace commonprj\components\core\entities\abstractPropertyValue;

use commonprj\extendedStdComponents\IBaseRepository;
use yii\web\HttpException;

/**
 * Interface IAbstractPropertyValueRepository
 * @package commonprj\components\core\entities\abstractPropertyValue
 */
interface IAbstractPropertyValueRepository extends IBaseRepository
{
    /**
     * Check if the attributes fit the requirements
     * @param array $attributes
     * @return bool
     * @throws HttpException
     */
    public static function validateAbstractPropertyValues(array $attributes): bool;

    /**
     * Update value from appropriate table 'name' field for 'foreign-key' index values
     * @param array $attributes AR attributes
     * @return array
     */
    public static function updateRelevantCoreElementAttributes(array $attributes): array;

    /**
     * Returns Property Value Primary key field name
     * @return string
     */
    public function primaryKey(): string;

}
