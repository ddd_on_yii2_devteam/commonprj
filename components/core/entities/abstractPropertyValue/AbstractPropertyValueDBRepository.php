<?php
/**
 * @package         FurniPrice
 * @subpackage      Core
 * @category        DBRepository
 * @created         17.06.2016
 * @author          daSilva.Rodrigues
 * @refactoredBy    Vasiliy Konakov
 * @updated         13.12.2017
 */

namespace commonprj\components\core\entities\abstractPropertyValue;

use commonprj\components\core\models\PropertyRecord;
use commonprj\extendedStdComponents\BaseCrudModel;
use commonprj\extendedStdComponents\IBaseCrudModel;
use commonprj\services\SearchDBSynchService;
use commonprj\extendedStdComponents\BaseDBRepository;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\BaseInflector;
use yii\web\HttpException;
use Yii;

/**
 * Class AbstractPropertyValueRepository
 * @package commonprj\components\core\entities\abstractPropertyValue
 */
class AbstractPropertyValueDBRepository extends BaseDBRepository implements IAbstractPropertyValueRepository
{

    protected $propertyValueIdInSearchDB = null;

    /**
     * @param array $attributes
     * @return bool
     * @throws HttpException
     */
    public static function validateAbstractPropertyValues(array $attributes): bool
    {

        return false;
    }

    /**
     * Update value from appropriate table 'name' field for 'foreign-key' index values
     * @param array $attributes AR attributes
     * @return array
     */
    public static function updateRelevantCoreElementAttributes(array $attributes): array
    {

        /**
         * // @todo сделать поиск значений атрибута 'name' для смежных таблиц
         * $attributes['elementName'] =
         * $attributes['propertyName'] =
         * $attributes['propertyTypeName'] =
         *
         * PropertyValueRecord::find()
         * ->select(['name'])
         * ->where(['id' => $attributes['propertyTypeId']])
         * ->scalar();
         */

        return $attributes;
    }

    /**
     * Returns Property Value Primary key field name
     * @return string
     */
    public function primaryKey(): string
    {
        return AbstractPropertyValueDBRepository::PRIMARY_KEY_FIELD_NAME;
    }

    /**
     * @param BaseCrudModel $propertyValue
     * @return bool
     */
    public function update(IBaseCrudModel $propertyValue): bool
    {
        $propertyValueRecord = $this->activeRecordClass::findOne($propertyValue->id);
        $propertyValueRecord->setAttributes(self::arrayKeysCamelCase2Underscore($propertyValue->getAttributes()));

        if ($propertyValueRecord->save()) {
            $propertyValue->setAttributes(self::arrayKeysUnderscore2CamelCase($propertyValueRecord->attributes), false);

            return true;
        } else {
            $propertyValue->addErrors($propertyValueRecord->getErrors());

            return false;
        }
    }

    /**
     * @param IBaseCrudModel $entity
     * @param bool $runValidation
     * @param null $attributes
     * @return bool
     */
    public function insert(IBaseCrudModel $propertyValue, $runValidation = true, $attributes = null): bool
    {
        $this->addToSearchDatabase($propertyValue);
        $result = parent::insert($propertyValue, $runValidation, $attributes);
        return $result;
    }


    /**
     * @param IBaseCrudModel $entity
     * @param null $attributes
     * @return bool
     */
    protected function updateInternal(IBaseCrudModel $propertyValue, $attributes = null)
    {
        $transaction = Yii::$app->getDb()->beginTransaction();

        $record = new $this->activeRecordClass();
        foreach ($record->attributes() as $attribute) {
            $entityField = lcfirst(BaseInflector::camelize($attribute));
            $record->setAttribute($attribute, $propertyValue->{$entityField});

            if ($propertyValue->getOldAttributes($entityField)) {
                $record->setOldAttribute($attribute, $propertyValue->getOldAttributes($entityField));
            }
        }

        if ($propertyValue->multiplicityId == AbstractPropertyValue::MULTIPLICITY_SCALAR) {
            $searchDbSynch = new SearchDBSynchService($record, $propertyValue->multiplicityId);
            $searchDbSynch->saveScalarPropertyValue();

            if ($propertyValue->hasAttribute('searchdbPropertyValueId')) {
                $propertyValue->searchdbPropertyValueId = $searchDbSynch->getPropertyValueIdInSearchDB();
            }
        }

        $result = parent::updateInternal($propertyValue, $attributes);


        if (!$result) {
            $transaction->rollBack();
            return $record;
        }

        $transaction->commit();

        return $result;
    }

    public function addToSearchDatabase($propertyValue)
    {
        $record = new $this->activeRecordClass();
        foreach ($record->attributes() as $attribute) {
            $entityField = lcfirst(BaseInflector::camelize($attribute));
            $record->setAttribute($attribute, $propertyValue->{$entityField});

            if ($propertyValue->getOldAttributes($entityField)) {
                $record->setOldAttribute($attribute, $propertyValue->getOldAttributes($entityField));
            }
        }

        if ($propertyValue->multiplicityId == AbstractPropertyValue::MULTIPLICITY_SCALAR) {
            $searchDbSynch = new SearchDBSynchService($record, $propertyValue->multiplicityId);
            $searchDbSynch->saveScalarPropertyValue();

            if ($propertyValue->hasAttribute('searchdbPropertyValueId')) {
                $propertyValue->searchdbPropertyValueId = $searchDbSynch->getPropertyValueIdInSearchDB();
            }
        }
    }

    /**
     * @param BaseCrudModel $propertyValue
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function save2(IBaseCrudModel $propertyValue, $runValidation = true, $attributes = null): bool
    {

        if ($propertyValue->isNew()) {
            if ($propertyValue->multiplicityId == AbstractPropertyValue::MULTIPLICITY_SCALAR
                && $this->hasElementPropertyValue($propertyValue)) {
                throw new \Exception('Current Element already has current property value');
            }
        }

        $transaction = Yii::$app->getDb()->beginTransaction();
        if ($propertyValue->isNew()) {
            $record = new $this->activeRecordClass();
        } else {
            $record = (new $this->activeRecordClass())
                ->findOne($propertyValue->id);
        }

        $record->setAttributes(self::arrayKeysCamelCase2Underscore($propertyValue->getAttributes()));

        if ($propertyValue->multiplicityId == AbstractPropertyValue::MULTIPLICITY_SCALAR) {
            $searchDbSynch = new SearchDBSynchService($record, $propertyValue->multiplicityId);
            $searchDbSynch->saveScalarPropertyValue();

            if ($record->hasAttribute('searchdb_property_value_id')) {
                $record->setAttribute('searchdb_property_value_id', $searchDbSynch->getPropertyValueIdInSearchDB());
            }
        }

        $result = $record->save();

        if (!$result) {
            $transaction->rollBack();
            return $record;
        }

        $transaction->commit();

        $propertyValue->id = $record->id;

        if ($record->hasAttribute('searchdb_property_value_id')) {
            $propertyValue->searchdbPropertyValueId = $record->getAttribute('searchdb_property_value_id');
        }

        return $result;
    }

    /**
     * Check existence of value for current property and element
     * @param AbstractPropertyValue $propertyValue
     * @return bool
     */
    protected function hasElementPropertyValue(AbstractPropertyValue $propertyValue): bool
    {
        if ($propertyValue->isRelation) {
            return false;
        }

        $recordsCount = $this->activeRecordClass::find()->where([
            'property_id' => $propertyValue->propertyId,
            'element_id'  => $propertyValue->elementId,
        ])->count();

        return $recordsCount;
    }

    /**
     * @param AbstractPropertyValue $propertyValue
     * @param ActiveRecord $propertyValueRecord
     * @param int $multiplicityId
     * @return AbstractPropertyValue
     */
    public function getValue(AbstractPropertyValue $propertyValue, ActiveRecord $propertyValueRecord): AbstractPropertyValue
    {
        $propertyValue->setAttributes(self::arrayKeysUnderscore2CamelCase($propertyValueRecord->attributes), false);
        return $propertyValue;
    }

    /**
     * Возвращает строку для UNION запроса для получения свойств со значениями элемента
     * @param int $elementId
     * @return Query
     */
    public function getValueQueryBuilder(int $elementId)
    {
        $queryBuilder = (new Query())
            ->select([
                'id',
                'value' => 'cast(value as text)',
                'property_id',
                'element_id',
                'prop_class' => new Expression("'" . $this->activeRecordClass . "'"),
            ])
            ->from($this->activeRecordClass::tableName())
            ->where(['element_id' => $elementId]);

        $queryBuilder->addSelect(['searchdb_property_value_id' => new Expression(0)]);


        return $queryBuilder;
    }

    /**
     * @param int $propertyId
     * @return bool|string
     */
    protected static function getConcretePropertyValueClassByPropertyId(int $propertyId)
    {
        $propertyRecord = PropertyRecord::findOne($propertyId);
        return Yii::$app->propertyService->getPropertyValueClassByTypeId($propertyRecord->property_type_id);
    }

}
