<?php
/**
 * @package         FurniPrice
 * @subpackage      Core
 * @category        DBRepository
 * @created         20.12.2017
 * @author          Vasiliy Konakov
 */

namespace commonprj\components\core\entities\aggregationRelationValue;

use commonprj\components\core\entities\relationValue\RelationValueServiceRepository;

/**
 * Class AggregationRelationValueServiceRepository
 * @package commonprj\components\core\entities\aggregationRelationValue
 */
class AggregationRelationValueServiceRepository extends RelationValueServiceRepository
{

}
