<?php
/**
 * Created by daSilva.Rodrigues
 * Date: 18.08.2016
 */

namespace commonprj\components\core\entities\relationClass;

use commonprj\extendedStdComponents\BaseCrudModel;
use Yii;
use yii\web\HttpException;

/**
 * Class RelationClass
 * @package commonprj\components\core\entities\relationClass
 */
class RelationClass extends BaseCrudModel
{
    public $id;
    public $relationTypeId;
    public $name;
    public $sysname;
    public $description;
    public $relationGroups;
    public $elementClasses;
    public $elementClass2relationClasses;

    /**
     * Присвоение свойству доменного слоя $repository - соответствующего компонента
     * @inheritdoc
     */
    public function __construct($config = [])
    {
        throw new \Exception('This class is legasy and not have been refactored!');
        parent::__construct($config);
        $this->repository = Yii::$app->relationClassRepository;
    }

    /**
     * Удаляет запись текущего инстанса вместе со всеми зависимостями.
     */
    function delete():bool
    {
        return $this->repository->deleteRelationClassById($this->id);
    }

    /**
     * @return BaseCrudModel
     * @throws HttpException
     */
    public function update()
    {
        return $this->save();
    }

    /**
     * @return BaseCrudModel
     */
    public function save()
    {
        return $this->repository->save($this);
    }

    /**
     * @param $isRoot
     * @return mixed
     */
    public function getElementClassesByIsRoot($isRoot)
    {
        return $this->repository->getElementClassesById($this->id, $isRoot);
    }

}
