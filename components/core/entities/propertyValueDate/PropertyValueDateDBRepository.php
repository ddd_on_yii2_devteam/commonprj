<?php

namespace commonprj\components\core\entities\propertyValueDate;

use commonprj\components\core\entities\abstractPropertyValue\AbstractPropertyValueDBRepository;
use commonprj\components\core\models\PropertyValueDateRecord;

/**
 * Class DatePropertyDBRepository
 * @package commonprj\components\core\entities\propertyValueDate
 */
class PropertyValueDateDBRepository extends AbstractPropertyValueDBRepository
{
    public $activeRecordClass = PropertyValueDateRecord::class;
}