<?php

namespace commonprj\components\core\entities\propertyValueColor;

use commonprj\components\core\entities\abstractPropertyValue\AbstractPropertyValueDBRepository;
use commonprj\components\core\models\PropertyValueColorRecord;

class PropertyValueColorDBRepository extends AbstractPropertyValueDBRepository
{
    public $activeRecordClass = PropertyValueColorRecord::class;

}