<?php

namespace commonprj\components\core\entities\propertyValueColor;

use Yii;
use yii\db\ActiveRecord;
use commonprj\components\core\entities\abstractPropertyValue\AbstractPropertyValue;

/**
 * Class PropertyValueColor
 * @package commonprj\components\core\entities\propertyValueColor
 */
class PropertyValueColor extends AbstractPropertyValue
{

    public function __construct(array $config = [], ActiveRecord $activeRecord = null) {
        parent::__construct($config);
        $this->repository = Yii::$app->propertyValueColorRepository;
    }

}
