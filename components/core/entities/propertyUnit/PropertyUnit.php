<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 12.02.2018
 * Time: 11:00
 */

namespace commonprj\components\core\entities\propertyUnit;

use commonprj\extendedStdComponents\BaseCrudModel;
use Yii;

/**
 * Class PropertyUnit
 * @property PropertyUnitDBRepository $repository
 * @package commonprj\components\core\entities\propertyUnit
 */
class PropertyUnit extends BaseCrudModel
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * PropertyUnit constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->repository = Yii::$app->propertyUnitRepository;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['id', 'integer', 'skipOnEmpty' => true],
            [['name'], 'string', 'max' => 50],
            [['name'], 'unique'],
        ];
    }

    /**
     * @return array|\commonprj\components\core\entities\property\Property[]
     * @throws \yii\web\HttpException
     */
    public function getProperties()
    {
        return $this->repository->getProperties($this);
    }
}