<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 13.02.2018
 * Time: 12:52
 */

namespace commonprj\components\core\entities\propertyUnit;

use commonprj\extendedStdComponents\BaseServiceRepository;
use Yii;
use yii\httpclient\Client;

/**
 * Class PropertyUnitServiceRepository
 * @package commonprj\components\core\entities\propertyUnit
 */
class PropertyUnitServiceRepository extends BaseServiceRepository
{
    /**
     * @var Client
     */
    protected $restServer;

    /**
     * @var string
     */
    protected $baseUri = 'core/property-unit/';

    /**
     * PropertyServiceRepository constructor.
     */
    public function __construct()
    {
        $this->restServer = Yii::$app->catalogManager;
    }

    /**
     * @param PropertyUnit $propertyUnit
     * @return array
     * @throws \yii\web\HttpException
     */
    public function getProperties(PropertyUnit $propertyUnit): array
    {
        $this->requestUri = $this->getBaseUri($propertyUnit->id . '/properties');
        return $this->getApiData();
    }

}