<?php

namespace commonprj\components\core\entities\propertyValueString;

use commonprj\components\core\entities\abstractTranslateValue\AbstractTranslateValueRepository;
use commonprj\extendedStdComponents\IBaseCrudModel;
use Yii;
use commonprj\components\core\models\PropertyValueStringRecord;
use yii\db\Query;

/**
 * Class StringPropertyDBRepository
 * @package commonprj\components\core\entities\stringProperty
 */
class PropertyValueStringDBRepository extends AbstractTranslateValueRepository
{
    public $activeRecordClass = PropertyValueStringRecord::class;

    /**
     * @param IBaseCrudModel $propertyValue
     * @param bool $runValidation
     * @param null $attributes
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     */
    public function save(IBaseCrudModel $propertyValue, $runValidation = true, $attributes = null): bool
    {
        return parent::save($propertyValue, $runValidation, $attributes);
    }

    /**
     * Возвращает данные для UNION запроса для получения свойств со значениями элемента
     * @param int $elementId
     * @return Query
     */
    public function getValueQueryBuilder(int $elementId)
    {
        $queryBuilder = parent::getValueQueryBuilder($elementId);
        $queryBuilder->addSelect(['searchdb_property_value_id' => 'searchdb_property_value_id']);

        return $queryBuilder;
    }

    public function getLocalizationService()
    {
        return Yii::$app->localization;
    }

}