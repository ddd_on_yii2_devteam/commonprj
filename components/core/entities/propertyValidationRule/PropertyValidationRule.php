<?php

namespace commonprj\components\core\entities\propertyValidationRule;

use commonprj\extendedStdComponents\BaseCrudModel;
use Yii;

class PropertyValidationRule extends BaseCrudModel
{

    public $id;
    public $propertyVariantId;
    public $data;
    public $relatedProperties;

    /**
     * Присвоение свойству доменного слоя $repository - соответствующего компонента
     * @inheritdoc
     */
    public function __construct($config = [])
    {
        parent::__construct($config);
        $this->repository = Yii::$app->propertyValidationRuleRepository;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['id', 'integer', 'skipOnEmpty' => true],
        ];
    }

}