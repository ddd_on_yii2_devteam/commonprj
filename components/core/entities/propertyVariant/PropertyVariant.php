<?php

namespace commonprj\components\core\entities\propertyVariant;

use commonprj\extendedStdComponents\BaseCrudModel;
use Yii;

/**
 * Class PropertyVariant
 * @package commonprj\components\core\entities\propertyVariant
 */
class PropertyVariant extends BaseCrudModel
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var int
     */
    public $propertyId;

    /**
     * @var int
     */
    public $propertyMultiplicityId;

    /**
     * @var int
     */
    public $propertyValueId;
    public $propertySysname;

    /**
     * Присвоение свойству доменного слоя $repository - соответствующего компонента
     * PropertyVariant constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        parent::__construct($config);
        $this->repository = Yii::$app->propertyVariantRepository;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['id', 'integer', 'skipOnEmpty' => true],
            [['propertyId', 'propertyMultiplicityId', 'propertyValueId'], 'integer', 'skipOnEmpty' => true],
        ];
    }

    /**
     * @return mixed
     */
    public function getProperty()
    {
        return $this->repository->getProperty($this);
    }

    /**
     * @return mixed
     */
    public function getPropertyValue()
    {
        return $this->repository->getPropertyValue($this);
    }

    /**
     * @return mixed
     */
    public function getPropertyValidationRule()
    {
        return $this->repository->getPropertyValidationRule($this);
    }

    /**
     * @param int $elementClassId
     * @return mixed
     */
    public function getTemplate(int $elementClassId)
    {
        return $this->repository->getTemplate($this, $elementClassId);
    }

    /**
     * @return mixed
     */
    public function getElements()
    {
        return $this->repository->getElements($this);
    }

    /**
     * @param int $elementClassId
     * @return mixed
     */
    public function getElementCategories(int $elementClassId)
    {
        return $this->repository->getElementCategories($this, $elementClassId);
    }

    /**
     * Удаляет запись если не осталось связей
     * @return bool
     */
    public function tryToDelete()
    {
        try {
            $this->repository->delete($this);
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }
    /**
     * @param int $elementClass
     * @return mixed
     */
    public function getTemplateInHierarchy(int $elementClass)
    {
        return $this->repository->getTemplateInHierarchy($this, $elementClass);
    }

}
