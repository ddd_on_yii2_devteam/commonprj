<?php

namespace commonprj\components\core\entities\propertyVariant;

use commonprj\components\core\entities\abstractPropertyValue\AbstractPropertyValue;
use commonprj\components\core\entities\property\Property;
use commonprj\components\core\entities\propertyValidationRule\PropertyValidationRule;
use commonprj\components\core\entities\template\Template;
use commonprj\extendedStdComponents\BaseServiceRepository;
use Yii;
use yii\httpclient\Client;

/**
 * Class ElementCategoryServiceRepository
 * @package commonprj\components\core\entities\elementCategory
 */
class PropertyVariantServiceRepository extends BaseServiceRepository implements IPropertyVariantRepository
{
    /**
     * @var Client
     */
    protected $restServer;

    /**
     * @var string
     */
    protected $baseUri = 'core/property-variant/';

    /**
     * PropertyVariantServiceRepository constructor.
     */
    public function __construct()
    {
        $this->restServer = Yii::$app->catalogManager;
    }

    public function getProperty(PropertyVariant $propertyVariant): ?Property
    {
        // TODO: Implement getProperty() method.
    }

    public function getPropertyValue(PropertyVariant $propertyVariant): AbstractPropertyValue
    {
        // TODO: Implement getPropertyValue() method.
    }

    public function getPropertyValidationRule(PropertyVariant $propertyVariant): ?PropertyValidationRule
    {
        // TODO: Implement getPropertyValidationRule() method.
    }

    public function getTemplate(PropertyVariant $propertyVariant, int $elementClassId): ?Template
    {
        // TODO: Implement getTemplate() method.
    }

    public function getElementCategories(PropertyVariant $propertyVariant, int $elementClassId): array
    {
        // TODO: Implement getElementCategories() method.
    }



    /**
     * Возвращаяет id найденного или свежесозданного PropertyVariant
     * @param int $furnitureTypePropertyId
     * @param int $furnitureTypeId
     * @return int
     */
    public static function getPropertyVariantId(int $furnitureTypePropertyId, int $furnitureTypeId): int
    {
        $propertyVariantId = self::findPropertyVariantId($furnitureTypePropertyId, $furnitureTypeId);

        if (is_null($propertyVariantId)) {
            $propertyVariant = new PropertyVariant();
            $propertyVariant->load([
                'propertyId'      => $furnitureTypePropertyId,
                'propertyValueId' => $furnitureTypeId,
                'propertyMultiplicityId' => AbstractPropertyValue::MULTIPLICITY_SCALAR,
            ], '');
            $propertyVariant->save();
            $propertyVariantId = $propertyVariant->id;
        }

        return $propertyVariantId;
    }

    /**
     * @param int $furnitureTypePropertyId
     * @param int $furnitureTypeId
     * @return int|null
     */
    private static function findPropertyVariantId(int $furnitureTypePropertyId, int $furnitureTypeId): ?int
    {
        $response = PropertyVariant::findAll(['propertyId__eq' => $furnitureTypePropertyId, 'propertyValueId__eq' => $furnitureTypeId]);

        try {
            $propertyVariant = reset($response['items']);
        } catch (\Exception $e) {
            return null;
        }

        if ($propertyVariant instanceof PropertyVariant) {
            return $propertyVariant->id;
        }

        return null;
    }
}
