<?php
/**
 * @package         FurniPrice
 * @subpackage      Core
 * @category        DBRepository
 * @created         20.12.2017
 * @author          Vasiliy Konakov
 */

namespace commonprj\components\core\entities\hierarchyRelationValue;

use commonprj\components\core\entities\relationValue\RelationValueServiceRepository;
use commonprj\extendedStdComponents\BaseCrudModel;

/**
 * Class HierarchyRelationValueServiceRepository
 * @package commonprj\components\core\entities\hierarchyRelationValue
 */
class HierarchyRelationValueServiceRepository extends RelationValueServiceRepository
{

    /**
     * @param array $condition
     * @return array|BaseCrudModel[] Метод мапит массив доменных моделей # @todo # необходимо ревью (непонятно как использовать) @bklv
     */

    public function find(array $condition = null): array
    {
        $this->requestUri = 'common/hierarchy-relation-value'; # @todo # необходимо ревью (хардкодинг - лучше перенести в константы) @bklv
        $arModel = $this->getApiData($condition);
        return $this->getArrayOfModels($arModel);
    }
}
