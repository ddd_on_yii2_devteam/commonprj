<?php
/**
 * @package         FurniPrice
 * @subpackage      Core
 * @category        DBRepository
 * @created         20.12.2017
 * @author          Vasiliy Konakov
 */

namespace commonprj\components\core\entities\associationRelationValue;

use commonprj\components\core\entities\relationValue\RelationValueServiceRepository;

/**
 * Class AssociationRelationValueServiceRepository
 * @package commonprj\components\core\entities\associationRelationValue
 */
class AssociationRelationValueServiceRepository extends RelationValueServiceRepository
{

}
