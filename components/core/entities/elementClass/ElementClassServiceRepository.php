<?php

namespace commonprj\components\core\entities\elementClass;

use commonprj\components\core\entities\property\Property;
use commonprj\extendedStdComponents\BaseServiceRepository;
use Yii;
use yii\httpclient\Client;

/**
 * Class ElementClassServiceRepository
 * @package commonprj\components\core\entities\elementClass
 */
class ElementClassServiceRepository extends BaseServiceRepository implements IElementClassRepository
{
    /**
     * @var Client
     */
    protected $restServer;

    /**
     * @var string
     */
    protected $baseUri = 'core/element-class/';


    protected $forbiddenMethods = ['save', 'update', 'delete'];

    public function __construct()
    {
        $this->restServer = Yii::$app->catalogManager;
    }

    /**
     * @param int $elementClassId
     * @return array|\yii\db\ActiveRecord
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    public function getContext(int $elementClassId)
    {
        $this->requestUri = $this->getBaseUri($elementClassId . '/context');
        return $this->getApiData();
    }

    /**
     * @param ElementClass $elementClass
     * @param bool $relationClasses
     * @return array
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    public function getProperties(ElementClass $elementClass, $relationClasses = false): array
    {
        if ($relationClasses) {
            $this->requestUri = $this->getBaseUri($elementClass->id . '/relation-classes');
        } else {
            $this->requestUri = $this->getBaseUri($elementClass->id . '/properties');
        }

        $content = $this->getApiData();

        if (!empty($content)) {
            $result = $this->getArrayOfModels($content, Property::className());
        }

        return $result ?? [];
    }

    /**
     * @param ElementClass $elementClass
     * @param int $propertyId
     * @return bool
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    public function bindProperty(ElementClass $elementClass, int $propertyId): bool
    {
        $uri = $this->getBaseUri($elementClass->id . '/property/' . $propertyId);
        $response = $this->restServer->post($uri)->send();

        if (!$response->getIsOk()) {
            $this->throwRestServerExceptionMessage($response);
        }

        return $response->getIsOk();
    }

    /**
     * @param ElementClass $elementClass
     * @param int $propertyId
     * @return bool
     */
    public function unbindProperty(ElementClass $elementClass, int $propertyId): bool
    {
        $uri = $this->getBaseUri($elementClass->id . '/property/' . $propertyId);
        $response = $this->restServer->delete($uri)->send();

        return $response->getIsOk();
    }

}