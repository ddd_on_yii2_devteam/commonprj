<?php
/**
 * Created by daSilva.Rodrigues
 * Date: 23.09.2016
 */

namespace commonprj\components\core\entities\elementClass;

use commonprj\components\core\entities\property\Property;
use commonprj\components\core\entities\relationClass\RelationClass;
use commonprj\extendedStdComponents\BaseCrudModel;
use commonprj\extendedStdComponents\IBaseRepository;
use yii\db\ActiveRecord;
use yii\web\HttpException;

/**
 * Class ElementClassRepository
 * @package commonprj\components\core\entities\elementClass
 */
interface IElementClassRepository extends IBaseRepository
{
    /**
     * Метод возвращает объект контекста к которому принадлежит текущий класс.
     * @param int $elementClassId - Класс чей контекст нужно вернуть.
     * @return ActiveRecord
     */
    public function getContext(int $elementClassId);

    /**
     * @param $condition
     * @param bool $isRoot
     * @return RelationClass[]
     * @throws HttpException
     */
//    public function getRelationClassesById($condition, bool $isRoot);

    /**
     * @param $condition
     * @return array|\yii\db\ActiveRecord[]
     * @throws HttpException
     */
//    public function getPropertiesById($condition);

    /**
     * @param $contextNameAndClassName
     * @return BaseCrudModel
     */
//    public function getElementClassByName($contextNameAndClassName);

    /**
     * @param ElementClass $elementClass
     * @return array
     */
    public function getProperties(ElementClass $elementClass): array;

    /**
     * @param ElementClass $elementClass
     * @param int $propertyId
     * @return bool
     */
    public function bindProperty(ElementClass $elementClass, int $propertyId): bool;

    /**
     * @param ElementClass $elementClass
     * @param int $propertyId
     * @return bool
     */
    public function unbindProperty(ElementClass $elementClass, int $propertyId): bool;
}
