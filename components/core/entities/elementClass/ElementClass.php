<?php
/**
 * Created by daSilva.Rodrigues
 * Date: 17.06.2016
 */

namespace commonprj\components\core\entities\elementClass;

use commonprj\components\core\entities\property\Property;
use commonprj\components\core\models\Property2elementClassRecord;
use commonprj\extendedStdComponents\BaseCrudModel;
use commonprj\extendedStdComponents\IBaseCrudModel;
use Yii;
use yii\web\HttpException;

/**
 * Class ElementClass
 * @package commonprj\components\core\entities\elementClass
 *
 * @property integer $context_id
 * @property string $name
 */
class ElementClass extends BaseCrudModel
{
    public $id;
    public $contextId;
    public $name;
    public $description;
    public $parentId;

    /**
     * Присвоение свойству доменного слоя $repository - соответствующего компонента
     * @inheritdoc
     */
    public function __construct($config = [])
    {
        parent::__construct($config);
        $this->repository = Yii::$app->elementClassRepository;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contextId'], 'integer'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['sysname'], 'string', 'max' => 50],
        ];
    }

    /**
     * Метод возвращает объект контекста к которому принадлежит текущий класс.
     * @return string
     */
    public function getContext()
    {
        return $this->repository->getContext($this->id);
    }

    /**
     * @param bool $isRoot
     * @return \commonprj\components\core\entities\relationClass\RelationClass[]
     * @throws HttpException
     */
    public function getRelationClassesByIsRoot(bool $isRoot)
    {
        return $this->repository->getRelationClassesById($this->id, $isRoot);
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     * @throws HttpException
     */
    public function getPropertiesById()
    {
        return $this->repository->getPropertiesById($this->id);
    }

    /**
     * @param $contextNameAndClassName
     * @return ElementClass
     * @throws HttpException
     */
    public function getElementClassByName($contextNameAndClassName)
    {
        return $this->repository->getElementClassByName($contextNameAndClassName);
    }

    /**
     * @param $elementClassId
     * @param $propertyId
     * @return Property2elementClassRecord
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function createProperty2ElementClass($elementClassId, $propertyId)
    {
        return $this->repository->createProperty2ElementClass($elementClassId, $propertyId);
    }

    /**
     * @return array
     * @throws HttpException
     */
    public function getProperties(): array
    {
        return $this->repository->getProperties($this);
    }

    /**
     * @param int $propertyId
     * @return bool
     */
    public function bindProperty(int $propertyId): bool
    {
        return $this->repository->bindProperty($this, $propertyId);
    }

    /**
     * @param int $propertyId
     * @return bool
     */
    public function unbindProperty(int $propertyId): bool
    {
        return $this->repository->unbindProperty($this, $propertyId);
    }

    /**
     * @return Property[]
     * @throws HttpException
     */
    public function getRelationClasses()
    {
        return $this->repository->getProperties($this, true);
    }

    public function extraFields()
    {
        return [
            'properties' => [$this, 'getProperties'],
        ];
    }
}
