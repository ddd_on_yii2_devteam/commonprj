<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 09.02.2018
 * Time: 18:44
 */

namespace commonprj\components\core\entities\propertyGroup;
use commonprj\components\core\entities\property\Property;
use commonprj\extendedStdComponents\IBaseRepository;

/**
 * Interface IPropertyGroupDBRepository
 * @package commonprj\components\core\entities\propertyGroup
 */
interface IPropertyGroupRepository extends IBaseRepository
{
    /**
     * @param PropertyGroup $propertyGroup
     * @return Property[]
     */
    public function getProperties(PropertyGroup $propertyGroup): array;
}