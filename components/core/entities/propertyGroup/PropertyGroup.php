<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 09.02.2018
 * Time: 18:42
 */

namespace commonprj\components\core\entities\propertyGroup;

use commonprj\extendedStdComponents\BaseCrudModel;
use Yii;

/**
 * Class PropertyGroup
 * @property PropertyGroupDBRepository $repository
 * @package commonprj\components\core\entities\propertyGroup
 */
class PropertyGroup extends BaseCrudModel
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var int
     */
    public $sortOrder = 1000;

    /**
     * PropertyGroup constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->repository = Yii::$app->propertyGroupRepository;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['id', 'integer', 'skipOnEmpty' => true],
            [['sortOrder'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
        ];
    }

    /**
     * @return array|\commonprj\components\core\entities\property\Property[]
     * @throws \yii\web\HttpException
     */
    public function getProperties()
    {
        return $this->repository->getProperties($this);
    }
}