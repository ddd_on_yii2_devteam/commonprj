<?php

namespace commonprj\components\core\entities\propertyValueGeolocation;

use Yii;
use commonprj\components\core\entities\abstractPropertyValue\AbstractPropertyValue;
use yii\db\ActiveRecord;

/**
 * Class GeolocationProperty
 * @package commonprj\components\core\entities\propertyValueGeolocation
 */
class PropertyValueGeolocation extends AbstractPropertyValue
{
    public function __construct(array $config = [], ActiveRecord $activeRecord = null)
    {
        parent::__construct($config);
        $this->repository = Yii::$app->propertyValueGeolocationRepository;
    }

    public function init()
    {
        parent::init();
        if (!empty($this->value)) {
            $this->value = '(' . implode(',', $this->value) . ')';
        }
    }

    /**
     * @return bool
     */
    public function save(): bool
    {
        if (!empty($this->value) && is_array($this->value)) {
            $this->value = '(' . implode(',', $this->value) . ')';
        }
        return parent::save();
    }
}