<?php

namespace commonprj\components\core\entities\propertyValueGeolocation;

use commonprj\components\core\entities\abstractPropertyValue\AbstractPropertyValueDBRepository;
use commonprj\components\core\models\PropertyValueGeolocationRecord;

/**
 * Class GeolocationPropertyDBRepository
 * @package commonprj\components\core\entities\propertyValueGeolocation
 */
class PropertyValueGeolocationDBRepository extends AbstractPropertyValueDBRepository
{
    public $activeRecordClass = PropertyValueGeolocationRecord::class;
}