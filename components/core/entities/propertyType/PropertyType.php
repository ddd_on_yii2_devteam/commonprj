<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 14.02.2018
 * Time: 12:39
 */

namespace commonprj\components\core\entities\propertyType;

use commonprj\extendedStdComponents\BaseCrudModel;
use Yii;

/**
 * Class PropertyType
 * @property PropertyTypeDBRepository $repository
 * @package commonprj\components\core\entities\propertyType
 */
class PropertyType extends BaseCrudModel
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var int
     */
    public $parentId;

    /**
     * @var int
     */
    public $elementClassId;

    /**
     * PropertyType constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->repository = Yii::$app->propertyTypeRepository;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['parentId', 'elementClassId'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['name'], 'unique'],
        ];
    }

    /**
     * @return array
     * @throws \HttpException
     */
    public function getProperties()
    {
        return $this->repository->getProperties($this);
    }

}