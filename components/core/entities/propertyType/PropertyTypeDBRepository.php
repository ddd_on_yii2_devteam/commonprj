<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 14.02.2018
 * Time: 12:49
 */

namespace commonprj\components\core\entities\propertyType;

use commonprj\components\core\entities\property\Property;
use commonprj\components\core\models\PropertyTypeRecord;
use commonprj\extendedStdComponents\BaseDBRepository;
use commonprj\extendedStdComponents\IBaseCrudModel;
use HttpException;

class PropertyTypeDBRepository extends BaseDBRepository implements IPropertyTypeRepository
{
    /**
     * @var PropertyTypeRecord::class
     */
    public $activeRecordClass = PropertyTypeRecord::class;

    /**
     * @param PropertyType $propertyType
     * @return bool|mixed|\yii\db\ActiveRecord
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function save(IBaseCrudModel $propertyType, $runValidation = true, $attributes = null): bool
    {
        if (!$propertyTypeRecord = $this->activeRecordClass::findOne($propertyType->id)) {
            $propertyTypeRecord = new $this->activeRecordClass();
        }
        $propertyTypeRecord->setAttributes(self::arrayKeysCamelCase2Underscore($propertyType->attributes));

        $result = $propertyTypeRecord->save();

        if ($result) {
            $propertyType->setAttributes(self::arrayKeysUnderscore2CamelCase($propertyTypeRecord->attributes), false);
        }

        return $result;
    }

    /**
     * @param PropertyType $propertyType
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function delete(IBaseCrudModel $propertyType): bool
    {
        $propertyUnitRecord = $this->activeRecordClass::findOne($propertyType->id);

        if (!$propertyUnitRecord) {
            return true;
        }

        return $propertyUnitRecord->delete();
    }

    /**
     * @return string[]
     */
    public static function primaryKey()
    {
        return PropertyTypeRecord::primaryKey();
    }

    /**
     * @param PropertyType $propertyType
     * @return array
     * @throws HttpException
     */
    public function getProperties(PropertyType $propertyType): array
    {
        /**
         * @var PropertyTypeRecord $propertyTypeRecord
         */
        $propertyTypeRecord = $this->activeRecordClass::findOne($propertyType->id);

        if (!$propertyTypeRecord) {
            throw new HttpException(404, basename(__FILE__, '.php') . __LINE__);
        }

        $propertyRecords = $propertyTypeRecord->getProperties()->all();
        $result = [];

        foreach ((array)$propertyRecords as $propertyRecord) {
            $result[] = Property::populate($propertyRecord);
        }

        return $result;
    }

}