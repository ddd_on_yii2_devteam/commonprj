<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 14.02.2018
 * Time: 12:49
 */

namespace commonprj\components\core\entities\propertyType;

use commonprj\extendedStdComponents\IBaseRepository;

/**
 * Interface IPropertyTypeRepository
 * @package commonprj\components\core\entities\propertyType
 */
interface IPropertyTypeRepository extends IBaseRepository
{
    /**
     * @param PropertyType $propertyType
     * @return array
     */
    public function getProperties(PropertyType $propertyType): array;
}