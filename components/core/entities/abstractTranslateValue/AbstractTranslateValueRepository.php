<?php
/**
 * @package         FurniPrice
 * @subpackage      Core
 * @category        DBRepository
 * @created         17.06.2016
 * @author          daSilva.Rodrigues
 * @refactoredBy    Vasiliy Konakov
 * @updated         13.12.2017
 */

namespace commonprj\components\core\entities\abstractTranslateValue;

use commonprj\components\core\entities\abstractPropertyValue\AbstractPropertyValue;
use commonprj\components\core\entities\abstractPropertyValue\AbstractPropertyValueDBRepository;
use commonprj\components\core\models\PropertyValueStringRecord;
use commonprj\extendedStdComponents\IBaseCrudModel;


/**
 * Class AbstractPropertyValue
 * @package commonprj\components\core\entities\abstractPropertyValue
 */
abstract class AbstractTranslateValueRepository extends AbstractPropertyValueDBRepository
{

    /**
     * @param AbstractPropertyValue $propertyValue
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function save(IBaseCrudModel $propertyValue, $runValidation = true, $attributes = null): bool
    {
        $localizationService = $this->getLocalizationService();
        $source = $localizationService->getSource('search', $this->activeRecordClass::tableName(), 'value');

        //получаем ID записи соответствующее этому значению в searchDB
        $propertyValueOldIdInSearchDB = $propertyValue->getOldAttributes('searchdbPropertyValueId') ?? null;

        //Данная запись уже есть в базе переводов
        if ($source && $propertyValueOldIdInSearchDB) {
            $translations = $localizationService->getLocalizationVariants($source, $propertyValue->value, $propertyValueOldIdInSearchDB, $propertyValue->propertyId);
        } else {
            //получаем переводы данного значения с учетом свойства
            $translations = $localizationService->generateTranslationList($propertyValue->value, $propertyValue->propertyId);
        }

        // если вернулся массив то в значение передаем английский язык или транслитерацию
        // возможно есть переводы
        if (is_array($translations) && (isset($translations['eng']) || isset($translations['transliterate']))) {
            $baseValue = empty($translations['eng']) ? $translations['transliterate'] : $translations['eng'];

            if ($localizationService->isCurrentLang() || $propertyValue->isNewRecord()) {
                $propertyValue->value = (string)$baseValue;
            } else {
                $propertyValueRecord = $this->activeRecordClass::find()->where(['id' => $propertyValue->id])->one();
                $propertyValue->setOldAttribute('value', $propertyValueRecord->value);
                $propertyValue->value = (string)$baseValue;
            }

        } else {
            $propertyValue->value = is_numeric($translations) ? (string)$translations : $translations;
        }

        //сохраняем значение
        $result = parent::save($propertyValue);

        $propertyValueIdInSearchDB = $propertyValue->searchdbPropertyValueId ?? null;

        if ($propertyValueIdInSearchDB && is_array($translations)) {
            $this->addTranslations('search.' . $this->activeRecordClass::tableName() . '.value',
                $translations,
                $propertyValueOldIdInSearchDB,
                $propertyValueIdInSearchDB,
                $propertyValue->propertyId
            );
        }

        return $result;
    }
}
