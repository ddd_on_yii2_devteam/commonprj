<?php

namespace commonprj\components\core\entities\propertyRangeValue;


use commonprj\extendedStdComponents\BaseCrudModel;
use yii\web\HttpException;

/**
 * Class PropertyRangeServiceRepository
 * @package commonprj\components\core\entities\propertyRangeValue
 */
class PropertyRangeServiceValueRepository implements IPropertyRangeValueRepository
{

    /**
     * @inheritdoc
     */
    public function findOne($condition)
    {
        // TODO: Implement findOne() method.
    }

    /**
     * @return mixed
     */
    function delete()
    {
        // TODO: Implement delete() method.
    }

    /**
     * @return BaseCrudModel
     * @throws HttpException
     */
    public function update()
    {
        // TODO: Implement update() method.
    }

    /**
     * @param bool $condition
     * @return BaseCrudModel
     */
    public function find($condition = false)
    {
        // TODO: Implement find() method.
    }

    /**
     * @return BaseCrudModel
     */
    public function save()
    {
        // TODO: Implement save() method.
    }
}