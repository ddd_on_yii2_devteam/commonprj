<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 05.04.2018
 * Time: 13:45
 */

namespace commonprj\components\core\entities\template;

use commonprj\extendedStdComponents\BaseServiceRepository;
use commonprj\extendedStdComponents\IBaseRepository;
use Yii;
use yii\httpclient\Client;

/**
 * Class TemplateServiceRepository
 * @package commonprj\components\core\entities\template
 */
class TemplateServiceRepository extends BaseServiceRepository implements IBaseRepository
{
    /**
     * @var Client
     */
    protected $restServer;

    /**
     * @var string
     */
    protected $baseUri = 'core/template/';

    /**
     * PropertyVariantServiceRepository constructor.
     */
    public function __construct()
    {
        $this->restServer = Yii::$app->catalogManager;
    }
}