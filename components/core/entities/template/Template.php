<?php

namespace commonprj\components\core\entities\template;

use commonprj\extendedStdComponents\BaseCrudModel;
use Yii;

/**
 * Class Template
 * @property Template $repository
 * @package commonprj\components\core\entities\template
 */
class Template extends BaseCrudModel
{

    /**
     * @var
     */
    public $id;
    /**
     * @var
     */
    public $propertyIds;
    /**
     * @var
     */
    public $propertyVariantId;
    /**
     * @var
     */
    public $elementClassId;
    /**
     * @var
     */
    public $relatedProperties;

    /**
     * Присвоение свойству доменного слоя $repository - соответствующего компонента
     * @inheritdoc
     */
    public function __construct($config = [])
    {
        parent::__construct($config);
        $this->repository = Yii::$app->templateRepository;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['id', 'integer', 'skipOnEmpty' => true],
        ];
    }

    /**
     * @return mixed
     */
    public function populateProperties()
    {
        return $this->repository->populateProperties($this);
    }

}