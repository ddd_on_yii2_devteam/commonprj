<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 10.04.2018
 * Time: 17:27
 */

namespace commonprj\components\core\entities\PropertyTreeItem;

/**
 * Interface IPropertyTreeItemRepository
 * @package commonprj\components\core\entities\PropertyTreeItem
 */
interface IPropertyTreeItemRepository
{
    /**
     * @param PropertyTreeItem $entity
     * @return mixed
     */
    public function getParent(PropertyTreeItem $entity): ?PropertyTreeItem;

    /**
     * @param PropertyTreeItem $entity
     * @return array
     */
    public function getParents(PropertyTreeItem $entity): array ;
}