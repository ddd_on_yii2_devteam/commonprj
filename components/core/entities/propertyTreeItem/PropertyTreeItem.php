<?php
/**
 * Created by daSilva.Rodrigues
 * Date: 17.06.2016
 */

namespace commonprj\components\core\entities\PropertyTreeItem;

use commonprj\components\core\entities\propertyListItem\PropertyListItem;
use Yii;

/**
 * Class PropertyListItem
 * @package commonprj\components\core\entities\PropertyTreeItem
 * @property IPropertyTreeItemRepository $repository
 * @property integer $context_id
 * @property string $name
 */
class PropertyTreeItem extends PropertyListItem
{
    public $parentId;

    public $children;

    /**
     * Присвоение свойству доменного слоя $repository - соответствующего компонента
     * @inheritdoc
     */
    public function __construct($config = [])
    {
        parent::__construct($config);
        $this->repository = Yii::$app->propertyTreeItemRepository;
    }

    /**
     * @return PropertyTreeItem|mixed|null
     */
    public function getParent()
    {
        return $this->repository->getParent($this);
    }

    /**
     * @return PropertyTreeItem[]
     */
    public function getParents(): array
    {
        return $this->repository->getParents($this);
    }
}
