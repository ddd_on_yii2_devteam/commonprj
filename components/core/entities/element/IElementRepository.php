<?php

namespace commonprj\components\core\entities\element;

use commonprj\components\core\entities\abstractPropertyValue\AbstractPropertyValue;
use commonprj\extendedStdComponents\IBaseRepository;
use yii\web\HttpException;
use commonprj\components\core\entities\elementClass\ElementClass;
use commonprj\components\core\entities\property\Property;

/**
 * Class ElementRepository
 * @package commonprj\components\core\entities\element
 */
interface IElementRepository extends IBaseRepository
{
    /**
     * @param Element $element
     * @return ElementClass[]
     */
    public function getElementClasses(Element $element): array;

    /**
     * Возвращает свойства, связанные с id запрошенного элемента.
     * @param Element $element - Элемент id, чьи свойства надо вернуть.
     * @return Property[]
     * @throws HttpException
     */
    public function getProperties(Element $element): array;

    /**
     * @param Element $element
     * @param int $propertyId
     */
    public function deletePropertyValue(Element $element, int $propertyId): void;

    /**
     * @param Element $element
     * @param int $propertyId
     * @return AbstractPropertyValue
     */
    public function getPropertyValue(Element $element, int $propertyId): AbstractPropertyValue;

    /**
     * Find parent element related to basic element identified by parent-type relation property
     * @param Element Element
     * @param int $propertyId PropertyRecord id
     * @return null|Element (domain layer object)
     */
    public function getParent(Element $element, int $propertyId): ?Element;

    /**
     * Find all children elements related to basic element by child-type relation property
     * @param Element $element
     * @param $propertyId integer PropertyRecord id
     * @return array Element[] (domain layer objects)
     */
    public function getChildren(Element $element, int $propertyId): array;

    /**
     * Find parent element related to this element by property type "association"
     * @param Element $element this Element
     * @param int $propertyId PropertyRecord id
     * @return Element|null (domain layer object)
     * @throws HttpException
     */
    public function getAssociatedEntity(Element $element, int $propertyId): ?Element;

    /**
     * @param Element $element
     * @param int $propertyId
     * @param int $elementId
     * @return bool
     */
    public function bindChild(Element $element, int $propertyId, int $elementId): bool;

    /**
     * Find if there is s child element related to basic element by relation property
     * @param Element $element
     * @param int $propertyId PropertyRecord id
     * @return bool SUCCESS (has a parent) OR FAIL (no parents)
     */
    public function getIsParent(Element $element, int $propertyId): bool;


    /**
     * @param Element $element
     * @param int $propertyId
     * @param array $elementIds [int]
     * @return bool
     */
    public function bindChildren(Element $element, int $propertyId, array $elementIds): bool;

    /**
     * @param Element $element
     * @param int $propertyId
     * @param int $elementId
     * @return bool
     */
    public function bindToParent(Element $element, int $propertyId, int $elementId): bool;

    /**
     * @param Element $element
     * @param int $propertyId
     * @param int $elementId
     * @return bool
     */
    public function unbindChild(Element $element, int $propertyId, int $elementId): bool;

    /**
     * @param Element $element
     * @param int $propertyId
     * @return bool
     */
    public function unbindAllChildren(Element $element, int $propertyId): bool;

    /**
     * @param Element $element
     * @param int $propertyId
     * @param int $associatedEntityId
     * @return bool
     */
    public function bindAssociatedEntity(Element $element, int $propertyId, int $associatedEntityId): bool;

    /**
     * @param Element $element
     * @param int $propertyId
     * @param array $associatedEntityIds
     * @return bool
     */
    public function bindAssociatedEntities(Element $element, int $propertyId, array $associatedEntityIds): bool;

    /**
     * @param Element $element
     * @param int $propertyId
     * @return array
     */
    public function getAssociatedEntities(Element $element, int $propertyId = null): array;

    /**
     * @param Element $element
     * @param int $propertyId
     * @param int $associatedEntityIds
     * @return bool
     */
    public function unbindAssociatedEntity(Element $element, int $propertyId, int $associatedEntityIds): bool;

    /**
     * @param Element $element
     * @param int $propertyId
     * @return bool
     */
    public function unbindAllAssociatedEntities(Element $element, int $propertyId): bool;

    /**
     * @param Element $element
     * @param array $propertyValues
     * @return mixed
     */
    public function saveProperties(Element $element, array $propertyValues);

    /**
     * @param Element $element
     * @param int $propertyId
     * @param $propertyValue
     * @return mixed
     */
    public function setPropertyValue(Element $element, int $propertyId, $propertyValue);

    /**
     * @param Element $element
     * @param int $propertyId
     * @param bool $isReturnFullObjects
     * @param bool $isReturnLinearArray
     * @return Element[]
     */
    public function getDescendants(Element $element, int $propertyId, $isReturnFullObjects = false, $isReturnLinearArray = false);

}
