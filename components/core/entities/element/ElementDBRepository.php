<?php

/**
 * Created by daSilva.Rodrigues
 * Date: 17.06.2016
 */

namespace commonprj\components\core\entities\element;

use commonprj\components\core\entities\abstractPropertyValue\AbstractPropertyValue;
use commonprj\components\core\entities\elementClass\ElementClass;
use commonprj\components\core\entities\property\Property;
use commonprj\components\core\entities\aggregationRelationValue\AggregationRelationValue;
use commonprj\components\core\entities\associationRelationValue\AssociationRelationValue;
use commonprj\components\core\entities\hierarchyRelationValue\HierarchyRelationValue;
use commonprj\components\core\helpers\ClassAndContextHelper;
use commonprj\components\core\models\Element2elementClassRecord;
use commonprj\components\core\models\PropertyRecord;
use commonprj\components\core\models\PropertyValueBigintRecord;
use commonprj\extendedStdComponents\BaseDBRepository;
use commonprj\extendedStdComponents\IBaseCrudModel;
use commonprj\extendedStdComponents\BaseCrudModel;
use commonprj\services\LocalizationService;
use Yii;
use commonprj\components\core\entities\property\PropertyDBRepository;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Exception;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;
use commonprj\components\core\models;
use commonprj\components\core\entities;
use commonprj\components\core\models\ElementRecord;


/**
 * Class ElementRepository
 * @package commonprj\components\core\entities\element
 */
class ElementDBRepository extends BaseDBRepository
{
    const SORT_PARAMS = [
        'id'       => [
            'asc'     => ['id' => SORT_ASC],
            'desc'    => ['id' => SORT_DESC],
            'default' => SORT_ASC,
            'label'   => 'Id',
        ],
        'name'     => [
            'asc'     => ['name' => SORT_ASC],
            'desc'    => ['name' => SORT_DESC],
            'default' => SORT_ASC,
            'label'   => 'Name',
        ],
        'isActive' => [
            'asc'     => ['is_active' => SORT_ASC],
            'desc'    => ['is_active' => SORT_DESC],
            'default' => SORT_ASC,
            'label'   => 'Is active',
        ],
    ];

    public $activeRecordClass = ElementRecord::class;

    /**
     * Массив хранящий знание о том какие свойства кем являются (значение, массив или диппазон)
     * @var array
     */
    protected $propertyMultiplicitySysname = [];
    protected $propertyMultiplicity = [];

    /**
     * @param Element $element
     * @return array
     * @throws Exception
     * @throws HttpException
     */
    public function getProperties(Element $element): array
    {
        $elementId = $element->id;

        if (!$elementRecord = ElementRecord::findOne($elementId)) {
            throw new HttpException(404, basename(__FILE__, '.php') . __LINE__);
        }

        $propertyValues = $this->getPropertiesValuesData($elementId);
        $result = [];
        $propertyRecords = $this->getElementProperties($elementRecord, Property::PROPERTY_TYPE_PROPERTY_ID);

        foreach ($propertyValues as $propertyValue) {
            $propertyId = $propertyValue['property_id'];

            if (!isset($result[$propertyId])) {

                if (!isset($propertyRecords[$propertyId])) {
                    continue;
                }

                $property = Property::findOne($propertyId);

            } else {
                $property = $result[$propertyId];
            }

            if ($propertyValue['is_array']) {
                $property->value[] = $propertyValue['value'];
            } elseif ($propertyValue['is_range']) {

                if ($propertyValue['is_from']) {
                    $property->value['from'] = $propertyValue['value'];
                } else {
                    $property->value['to'] = $propertyValue['value'];
                }
            } else {
                /** @var ActiveRecord $propertyValueRecord */
                $propertyValueRecord = new $propertyValue['prop_class'];
                $propertyValueRecord->setAttributes([
                    'id'          => $propertyValue['id'],
                    'value'       => $propertyValue['value'],
                    'property_id' => $propertyValue['property_id'],
                    'element_id'  => $elementId,
                    'is_array'    => $propertyValue['is_array'],
                ], false);

                if ($propertyValueRecord instanceof models\PropertyValueMoneyRecord) {
                    list($value, $currency) = explode(',', $propertyValueRecord->value);

                    $propertyValueRecord->value = [
                        "value"    => (double)($value / 100),
                        "currency" => $currency,
                    ];
                }

                if ($propertyValue['searchdb_property_value_id']) {
                    $property->setSearchdbPropertyValueId($propertyValue['searchdb_property_value_id']);
                    $propertyValueRecord->setAttribute('searchdb_property_value_id', $propertyValue['searchdb_property_value_id']);
                }

                $property->value = $propertyValueRecord->value;

                if ($propertyValueRecord instanceof models\PropertyValueJsonRecord) {
                    $property->value = json_decode($property->value);
                }
            }

            $property->setOldAttribute('value', $property->value);
            $result[$propertyId] = $property;
        }

        return array_values($result);
    }

    /**
     * @param Element $element
     * @return ElementClass[]
     * @throws HttpException
     */
    public function getElementClasses(Element $element): array
    {
        $elementClassesQuery = $this->getElementClassesQuery($element->id);

        $elementClassRecords = $elementClassesQuery->all();
        $result = [];

        foreach ($elementClassRecords as $elementClassRecord) {
            $result[$elementClassRecord->id] = self::instantiateByARAndClassName($elementClassRecord,
                entities\elementClass\ElementClass::class
            );
        }

        return $result;
    }

    private function getParentClassesByClass($elementClassRecord)
    {
        $result = [];
        if ($elementClassRecord->parent_id == 0) {
            $result[] = $elementClassRecord;
        } else {
            $parentElementClassRecord = models\ElementClassRecord::findOne(['id' => $elementClassRecord->parent_id]);

            $result = array_merge([$elementClassRecord], $this->getParentClassesByClass($parentElementClassRecord));
        }

        return $result;
    }

    /**
     * Сохранение переданного объекта Element через ActiveRecord. Запись идет транзакцией в 2 таблицы:
     * element и element2element_class
     * @param Element $element
     * @return bool
     * @throws HttpException
     * @throws \yii\db\Exception
     */
    public function save(IBaseCrudModel $element, $runValidation = true, $attributes = null): bool
    {
        $transaction = Yii::$app->getDb()->beginTransaction();

        $newRecord = $element->isNewRecord();

        $result = parent::save($element);

        if (!$result) {
            $transaction->rollBack();
            return false;
        }

        if ($newRecord) {
            // Если запись в таблицу element успешна, переходим к записи в таблицу element2class
            $element2elementClassRecord = new Element2elementClassRecord();
            $element2elementClassRecord->setAttributes([
                'element_id'       => $element->id,
                'element_class_id' => ClassAndContextHelper::getClassId(get_class($element)),
            ]);

            if (!$element2elementClassRecord->save()) {
                $element->addErrors($element2elementClassRecord->getErrors());
                $transaction->rollBack();

                return false;

            }
        }

        // Если записи в предыдущие таблицы успешны, делаем комит транзакции
        $transaction->commit();

        if ($element->properties) {
            $this->saveProperties($element, $element->properties);
        }

        return true;
    }


    public function update(IBaseCrudModel $element, $runValidation = true, $attributes = null): bool
    {
        $result = parent::update($element, $runValidation, $attributes);


        $needUpdateProperties = $attributes === null || isset($attributes['properties']);

        if ($result && $needUpdateProperties) {
            $this->saveProperties($element, $element->properties);
        }

        return true;
    }


    /**
     * @param Element $element
     * @throws HttpException
     */
    public function delete(IBaseCrudModel $element): bool
    {
        $elementRecord = ElementRecord::findOne($element->id);

        if (is_null($elementRecord)) {
            throw new HttpException(404, basename(__FILE__, '.php') . __LINE__);
        }

        Yii::$app->getDb()->beginTransaction();

        try {
            $this->deletePropertyRelations($element);
            $this->deletePropertyValues($elementRecord);
            $this->deleteFromElement2elementClass($elementRecord);
            $this->deleteFromElement($elementRecord);
        } catch (\Exception $e) {
            Yii::$app->getDb()->getTransaction()->rollBack();

            throw new HttpException(500, 'Failed to delete the object for unknown reason. ' . basename(__FILE__,
                    '.php') . __LINE__);
        }

        Yii::$app->getDb()->getTransaction()->commit();
        return true;
    }

    /**
     * @param $elementId
     * @return array
     * @throws \yii\db\Exception
     */
    private function getPropertiesValuesData($elementId)
    {
        $queryUnion = $this->getPropertiesValuesQuery($elementId);

        $query = (new Query())
            ->select([
                'tunion.id',
                'tunion.value',
                'tunion.property_id',
                'tunion.prop_class',
                'tunion.searchdb_property_value_id',
                'is_array' => new Expression('property_array.id IS NOT NULL'),
                'is_range' => new Expression('property_range.id IS NOT NULL'),
                'is_from'  => new Expression('property_range.from_value_id = tunion.id'),
                'is_to'    => new Expression('property_range.to_value_id = tunion.id'),
            ])
            ->from(['tunion' => $queryUnion])
            ->leftJoin(['property_array'],
                'tunion.property_id = property_array.property_id AND property_array.element_id = ' . $elementId)
            ->leftJoin(['property_range'],
                'tunion.property_id = property_range.property_id AND property_array.element_id = ' . $elementId);

        $result = $query->all();
        return $result;
    }

    /**
     * @param $elementId
     * @return null|Query;
     */
    private function getPropertiesValuesQuery($elementId)
    {
        $elementRecord = ElementRecord::findOne($elementId);

        $properties = $this->getElementProperties($elementRecord, Property::PROPERTY_TYPE_PROPERTY_ID);
        foreach ($properties as $property) {
            $propertyClass = PropertyDBRepository::getPropertyValueClassByTypeId($property->property_type_id);
            $existingProps[] = $propertyClass;
        }
        $existingProps = array_unique($existingProps);

//        $existingProps = [
//            entities\propertyValueBoolean\PropertyValueBoolean::class,
//            entities\propertyValueString\PropertyValueString::class,
//            entities\propertyValueText\PropertyValueText::class,
//            entities\propertyValueInt\PropertyValueInt::class,
//            entities\propertyValueBigint\PropertyValueBigint::class,
//            entities\propertyValueFloat\PropertyValueFloat::class,
//            entities\propertyValueListItem\PropertyValueListItem::class,
//            entities\propertyValueDate\PropertyValueDate::class,
//            entities\propertyValueTimeStamp\PropertyValueTimeStamp::class,
//            entities\propertyValueGeolocation\PropertyValueGeolocation::class,
//            entities\propertyValueJson\PropertyValueJson::class,
//            entities\propertyValueMoney\PropertyValueMoney::class,
//        ];


        /** @var Query $queryUnion */
        $queryUnion = null;

        foreach ($existingProps as $existingProp) {
            $query = (new $existingProp)->repository->getValueQueryBuilder($elementId);
            $queryUnion = $queryUnion ? $queryUnion->union($query) : $query;
        }

        return $queryUnion;
    }

    /**
     * Удаляет все свойства-значения элемента
     * @param ElementRecord $elementRecord
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    protected function deletePropertyValues(ElementRecord $elementRecord)
    {
        $queryUnion = $this->getPropertiesValuesQuery($elementRecord->id);
        $propertyValuesArray = $queryUnion->all();

        foreach ($propertyValuesArray as $propertyValueArray) {
            $propClass = $propertyValueArray['prop_class'];

            /** @var ActiveRecord $propertyValue */
            $propertyValue = $propClass::findOne($propertyValueArray['id']);
            $propertyValue->delete();
        }

    }

    /**
     * Удаляет все свойства-связи элемента
     * @param ElementRecord $elementRecord
     */
    private function deletePropertyRelations(Element $element)
    {
        $elementId = $element->id;
        $propertyRecords = PropertyRecord::find()
            ->joinWith(['elementClasses' => function (ActiveQuery $query) use ($elementId) {
                $query->joinWith('elements', false)
                    ->where(['element.id' => $elementId]);
            }], false)
            ->joinWith('propertyType', false)
            ->where(['property_type.parent_id' => Property::PROPERTY_TYPE_RELATION_ID])
            ->indexBy('id')
            ->all();

        foreach ($propertyRecords as $propertyRecord) {
            $parent = $this->getParent($element, $propertyRecord->id);

            if ($parent) {
                $this->unbind($parent->id, $propertyRecord->id, $element->id);
            }

            $this->unbindAllAssociatedEntities($element, $propertyRecord->id);
        }

    }

    /**
     * Внутренний метод для удаления по id элемента связей с классами.
     * @param ElementRecord $elementRecord - id элемента, чьи связи надо удалить.
     * @throws HttpException - В случае ошибки при удалении вернет ServerErrorHttpException
     */
    private function deleteFromElement2elementClass(ElementRecord $elementRecord)
    {
        $rows = $elementRecord->getElement2elementClasses()->all();
        foreach ($rows as $row) {
            $row->delete();
        }
    }

    /**
     * @param ElementRecord $elementRecord
     * @return false|int
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    private function deleteFromElement(ElementRecord $elementRecord)
    {
        return $elementRecord->delete();
    }

    /**
     * @param int $propertyId
     * @return array
     * @throws HttpException
     */
    public function getPropertyTypeById(int $propertyId)
    {
        if (!$propertyRecord = PropertyRecord::findOne($propertyId)) {
            throw new HttpException(404, basename(__FILE__, '.php') . __LINE__);
        }
        /** @var Property $property */
        $property = self::instantiateByARAndClassName($propertyRecord);
        $type = $property->getTypeById();

        return $type;
    }


    /**
     * @param string $propertyValueString
     * @param        $propertyId
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyValueRecord(string $propertyValueString, int $propertyId)
    {
        /** @var ActiveRecord $model */
        $model = new $propertyValueString();

        return $model::find()->where(['property_id' => $propertyId]);
    }

    /**
     * @param int $classId
     * @return array|\yii\db\ActiveRecord[]
     */
    public function idsByClassId(int $classId)
    {
        return Element2elementClassRecord::find()->select('element_id')->where(['element_class_id' => $classId])->asArray()->all();
    }

    /**
     * @return string[]
     */
    public function primaryKey()
    {
        return ElementRecord::primaryKey();
    }

    #-------------------------------------------------------------------------------------------------------------------
    # /** BEGIN this part of code created by Vasiliy Konakov 2018.01.15 as a part of feature/FP-62 // @bklv */
    #-------------------------------------------------------------------------------------------------------------------


    /**
     * Bind a child to an element
     * @param Element $element child Element
     * @param int $propertyId PropertyRecord id
     * @param int $childElementId parent ElementRecord id
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|\yii\db\Exception
     */
    public function bindChild(Element $element, int $propertyId, int $childElementId): bool
    {
        // Проверяем - может ли данный тип связи устанавливать подчиненную связь
        if (!Yii::$app->propertyRepository->isPropertyTypeAggregationOrHierarchy($propertyId)) {
            throw new HttpException(404, basename(__FILE__, '.php') . __LINE__);
        }

        // Создаем привязку
        return $this->bind($element->id, $propertyId, $childElementId, true);
    }

    /**
     * Binding children to parent element
     * @param Element $element
     * @param int $propertyId
     * @param array $childElementIds
     * @return bool|null SUCCESS OR FAIL
     * @throws Exception|HttpException
     */
    public function bindChildren(Element $element, int $propertyId, array $childElementIds): bool
    {
        Yii::$app->getDb()->beginTransaction();

        foreach ($childElementIds as $childElementId) {
            $result = $this->bindChild($element, $propertyId, $childElementId);

            if (!$result) {
                Yii::$app->getDb()->getTransaction()->rollBack();
                return false;
            }
        }

        Yii::$app->getDb()->getTransaction()->commit();

        return true;
    }

    /**
     * Bind an element to parent
     * @param Element $element child Element
     * @param int $propertyId PropertyRecord id
     * @param int $parentElementId parent ElementRecord id
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|\yii\db\Exception
     */
    public function bindToParent(Element $element, int $propertyId, int $parentElementId): bool
    {
        // Проверяем - может ли данный тип связи устанавливать подчиненную связь
        if (!Yii::$app->propertyRepository->isPropertyTypeAggregationOrHierarchy($propertyId)) {
            throw new HttpException(404, basename(__FILE__, '.php') . __LINE__);
        }

        // Создаем привязку
        return $this->bind($element->id, $propertyId, $parentElementId, false);
    }

    /**
     * Bind an associated element
     * @param Element $element this Element
     * @param int $propertyId PropertyRecord id
     * @param int $associatedElementId associated ElementRecord id
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|\yii\db\Exception
     */
    public function bindAssociatedEntity(Element $element, int $propertyId, int $associatedElementId): bool
    {
        // Проверяем - может ли данный тип связи устанавливать подчиненную связь
        if (!Yii::$app->propertyRepository->isPropertyTypeAssociation($propertyId)) {
            throw new HttpException(404, basename(__FILE__, '.php') . __LINE__);
        }

        // Создаем привязку
        return $this->bind($associatedElementId, $propertyId, $element->id, false);
    }

    /**
     * Bind associated elements
     * @param Element $element
     * @param int $propertyId
     * @param array $associatedElementIds
     * @return bool|null SUCCESS OR FAIL
     * @throws Exception|HttpException
     */
    public function bindAssociatedEntities(Element $element, int $propertyId, array $associatedElementIds): bool
    {
        Yii::$app->getDb()->beginTransaction();

        foreach ($associatedElementIds as $associatedElementId) {
            $result = $this->bindAssociatedEntity($element, $propertyId, $associatedElementId);

            if (!$result) {
                Yii::$app->getDb()->getTransaction()->rollBack();
                return false;
            }
        }

        Yii::$app->getDb()->getTransaction()->commit();

        return true;
    }

    /**
     * Delete relation between this and related CHILD element
     * @param Element $element parent Element
     * @param int $propertyId PropertyRecord id
     * @param int $childElementId child ElementRecord id
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|\yii\db\Exception
     */
    public function unbindChild(Element $element, int $propertyId, int $childElementId): bool
    {
        // Удаляем привязку
        return $this->unbind($element->id, $propertyId, $childElementId);
    }

    /**
     * Unbind all children elements
     * @param Element $element
     * @param int $propertyId
     * @return bool|null SUCCESS OR FAIL
     * @throws HttpException|Exception
     */
    public function unbindAllChildren(Element $element, int $propertyId): bool
    {
        // Ищем дочерние элементы и если не находим то вызываем Exception
        $childElements = $this->getChildren($element, $propertyId);

        if (!$childElements) {
            throw new HttpException(404, basename(__FILE__, '.php') . __LINE__);
        }

        // Осуществляем отвязку
        Yii::$app->getDb()->beginTransaction();

        foreach ($childElements as $childElement) {
            $result = $this->unbindChild($element, $propertyId, $childElement->id);

            if (!$result) {
                Yii::$app->getDb()->getTransaction()->rollBack();
                return false;
            }
        }

        Yii::$app->getDb()->getTransaction()->commit();

        return true;
    }

    /**
     * Delete relation between associated elements
     * @param Element $element this Element
     * @param int $propertyId PropertyRecord id
     * @param int $associatedElementId associated ElementRecord id
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|\yii\db\Exception
     */
    public function unbindAssociatedEntity(Element $element, int $propertyId, int $associatedElementId): bool
    {
        // Удаляем привязку
        return $this->unbind($element->id, $propertyId, $associatedElementId);
    }

    /**
     * Unbind all associated elements
     * @param Element $element
     * @param int $propertyId
     * @return bool|null SUCCESS OR FAIL
     * @throws Exception|HttpException
     */
    public function unbindAllAssociatedEntities(Element $element, int $propertyId): bool
    {
        // Ищем связанные элементы и если не находим то вызываем Exception
        $associatedElements = $this->getAssociatedEntities($element, $propertyId);
        if (!$associatedElements) {
            return true;
        }

        // Осуществляем отвязку

        Yii::$app->getDb()->beginTransaction();

        foreach ($associatedElements as $associatedElement) {
            $result = $this->unbindAssociatedEntity($element, $propertyId, $associatedElement->id);

            if (!$result) {
                Yii::$app->getDb()->getTransaction()->rollBack();
                return false;
            }
        }

        Yii::$app->getDb()->getTransaction()->commit();

        return true;
    }

    /**
     * @param Element $element
     * @param array $propertyValues
     * @return bool|mixed
     * @throws HttpException
     */
    public function saveProperties(Element $element, array $properties)
    {
        $elementRecord = ElementRecord::findOne(['id' => $element->id]);

        if (!$elementRecord) {
            throw new HttpException(404, basename(__FILE__, '.php') . __LINE__);
        }

        $elementPropertiesSysnames = $element->propertiesAttributes();

        $propertiesRecord = PropertyRecord::find()
            ->where(['sysname' => $elementPropertiesSysnames])
            ->indexBy('sysname')
            ->all();

        $propertyValues = [];

        foreach ($properties as $key => $property) {
            if ($property instanceof Property) {
                $propertyValues[$property->id] = $property->value;
            } else {
                $propertyValues[$key] = $property;
            }
        }

        $errors = [];
        foreach ($elementPropertiesSysnames as $propertyId => $propertySysname) {
            $propertyRecord = $propertiesRecord[$propertySysname] ?? null;

            if (!$propertyRecord) {
                throw new HttpException(404, basename(__FILE__, '.php') . __LINE__);
            }
            $propertyId = $propertyRecord->id;

            $newValue = $propertyValues[$propertyId] ?? $propertyValues[$propertyRecord->sysname] ?? null;
            $oldProperty = $element->getOldProperties($propertySysname);
            $oldValue = $oldProperty ? ($oldProperty->getOldAttributes('value') ?? null) : null;

            if ($newValue && $newValue != $oldValue) {
                $result = $this->setPropertyValue($element, $propertyId, $newValue);

                if ($result !== true) {
                    $errors[$propertyId] = $result;
                }
            }
        }

        return empty($errors) ? true : $errors;
    }

    /**
     * @param Element $element
     * @param int $propertyId
     * @param $propertyValue
     * @param array|null $propertiesRecord
     * @return bool|mixed
     * @throws HttpException
     */
    public function setPropertyValue(Element $element, int $propertyId, $propertyValue, array $propertiesRecord = null)
    {

        if ($propertiesRecord) {
            $propertyRecord = $propertiesRecord[$propertyId];
        } else {
            $propertyRecord = PropertyRecord::findOne($propertyId);
        }

        if (!$propertyRecord) {
            throw new HttpException(404, basename(__FILE__, '.php') . __LINE__);
        }

        $property = Property::findOne(['id' => $propertyId]);

        $propertyValueStruct = [
            'elementId' => $element->id,
        ];

        switch ($this->getPropertyMultiplicity($element, $propertyId, $propertyRecord->sysname)) {
            case AbstractPropertyValue::MULTIPLICITY_ARRAY:
                $propertyValueStruct['name'] = $propertyValue['name'] ?? uniqid();
                $propertyValueStruct['multiplicityId'] = AbstractPropertyValue::MULTIPLICITY_ARRAY;
                if (isset($propertyValue['name'])) {
                    unset($propertyValue['name']);
                }
                if (isset($propertyValue['multiplicityId'])) {
                    unset($propertyValue['multiplicityId']);
                }
                $propertyValueStruct['values'] = (array)$propertyValue;
                break;
            case AbstractPropertyValue::MULTIPLICITY_RANGE:
                $propertyValueStruct['name'] = $propertyValue['name'] ?? uniqid();
                $propertyValueStruct['fromValue'] = $propertyValue['from'];
                $propertyValueStruct['toValue'] = $propertyValue['to'];
                $propertyValueStruct['multiplicityId'] = AbstractPropertyValue::MULTIPLICITY_RANGE;
                break;
            default:
                $propertyValueStruct['value'] = $propertyValue;
                break;
        }

        $saveResult = $property->saveElementProperty($propertyValueStruct);
        //Если результат равен false или не прошли валидацию
        if ($saveResult !== true) {
            return $saveResult;
        }

        return true;
    }

    protected function getPropertyMultiplicity(Element $element, $propertyId, $propertySysname)
    {
        if (Yii::$app->propertyRepository->isPropertyTypeList($propertyId)) {
            return AbstractPropertyValue::MULTIPLICITY_ARRAY;
        }

        return ArrayHelper::getValue($element->getPropertyMultiplicityData(), $propertyId,
            ArrayHelper::getValue($element->getPropertyMultiplicityData(), $propertySysname,
                AbstractPropertyValue::MULTIPLICITY_SCALAR));
    }

    /**
     * @param int $elementId - элемент инициатор
     * @param int $propertyId
     * @param int $relatedElementId - элемент, который привязывают
     * @param bool $isParent
     * @return bool
     * @throws Exception
     * @throws HttpException
     */
    private function bind(int $elementId, int $propertyId, int $relatedElementId, $isParent = false)
    {
        $propertyRepository = Yii::$app->propertyRepository;

        $elementRecord = ElementRecord::findOne(['id' => $elementId]);
        $relatedElementRecord = ElementRecord::findOne(['id' => $relatedElementId]);
        $propertyRecord = PropertyRecord::findOne(['id' => $propertyId]);

        if (!$elementRecord || !$relatedElementRecord || !$propertyRecord) {
            throw new HttpException(404, basename(__FILE__, '.php') . __LINE__);
        }

        if (!$propertyRepository->isPropertyTypeRelation($propertyRecord->id)) {
            throw new HttpException(404, basename(__FILE__, '.php') . __LINE__);
        }

        if (
            !$this->elementHasProperty($elementRecord, $propertyRecord)
            ||
            !$this->elementHasProperty($relatedElementRecord, $propertyRecord)
        ) {
            throw new HttpException(404, basename(__FILE__, '.php') . __LINE__);
        }

        $newProperty = [
            'elementId'  => $elementId,
            'propertyId' => $propertyId,
            'value'      => $relatedElementId,
            'isParent'   => $isParent,
        ];

        // Сохраняем связь с проверкой на тип связи
        if ($propertyRepository->isPropertyTypeAssociation($propertyRecord->id)) {
            return (new AssociationRelationValue($newProperty))->save();
        } elseif ($propertyRepository->isPropertyTypeAggregation($propertyRecord->id)) {
            return (new AggregationRelationValue($newProperty))->save();
        } elseif ($propertyRepository->isPropertyTypeHierarchy($propertyRecord->id)) {
            return (new HierarchyRelationValue($newProperty))->save();
        }

        // В противном случае вызываем HttpException
        throw new HttpException(404, basename(__FILE__, '.php') . __LINE__);
    }

    /**
     * Unbinding base method
     * @param int $elementId Parent ElementRecord id
     * @param PropertyRecord $property PropertyRecord
     * @param int $value child ElementRecord id
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|\yii\db\Exception
     */
    private function unbind(int $elementId, int $propertyId, int $relatedElementId): bool
    {
        $propertyRepository = Yii::$app->propertyRepository;

        // Проверяем наличие записи для первичного и вторичного элементов
        $elementRecord = ElementRecord::findOne(['id' => $elementId]);
        $relatedElementRecord = ElementRecord::findOne(['id' => $relatedElementId]);
        $propertyRecord = PropertyRecord::findOne(['id' => $propertyId]);

        if (!$elementRecord || !$relatedElementRecord || !$propertyRecord) {
            throw new HttpException(404, basename(__FILE__, '.php') . __LINE__);
        }

        // Проверяем на отсутствие записи для связи
        $propertyToDeleteAttributes = [
            'elementId'  => $elementId,
            'propertyId' => $propertyId,
            'value'      => $relatedElementId,
        ];

        // Удаляем связь с проверкой на тип связи
        if ($propertyRepository->isPropertyTypeAssociation($propertyRecord->id)) {
            return (new AssociationRelationValue($propertyToDeleteAttributes))->delete();
        } elseif ($propertyRepository->isPropertyTypeAggregation($propertyRecord->id)) {
            return (new AggregationRelationValue($propertyToDeleteAttributes))->delete();
        } elseif ($propertyRepository->isPropertyTypeHierarchy($propertyRecord->id)) {
            return (new HierarchyRelationValue($propertyToDeleteAttributes))->delete();
        }

        // В противном случае вызываем HttpException
        throw new HttpException(404, basename(__FILE__, '.php') . __LINE__);

    }

    /**
     * @param ElementRecord $elementRecord
     * @param PropertyRecord $propertyRecord
     * @return bool
     */
    private function elementHasProperty(ElementRecord $elementRecord, PropertyRecord $propertyRecord)
    {
        $properties = $this->getElementProperties($elementRecord);

        return isset($properties[$propertyRecord->id]);
    }

    /**
     * @param ElementRecord $elementRecord
     * @return array
     */
    private function getElementProperties(ElementRecord $elementRecord, $propertyType = null)
    {
        $elementClassesQuery = $this->getElementClassesQuery($elementRecord->id);

        $elementClassesQuery = $elementClassesQuery
            ->with('properties')
            ->with('properties.propertyType');
        $elementClassRecords = $elementClassesQuery->all();

        $properties = [];

        foreach ($elementClassRecords as $elementClassRecord) {
            foreach ($elementClassRecord->properties as $property) {

                if (!$propertyType || $property->propertyType->parent_id == $propertyType) {
                    unset($property->propertyType);
                    $properties[$property->id] = $property;
                }
            }
        }

        return $properties;
    }

    /**
     * @param int $elementId
     * @return ActiveQuery
     */
    private function getElementClassesQuery(int $elementId)
    {
        // Данный запрос рекурсивно вытягивает все дерево классов текущего элемента
        $query = sprintf("WITH RECURSIVE r AS(
        select ec.* from element_class as ec
            left join element2element_class as e2ec on (ec.id = e2ec.element_class_id) where e2ec.element_id = %s

          UNION

          SELECT element_class.*
          FROM element_class
            JOIN r
              ON element_class.id = r.parent_id
        )

        SELECT * from r;", $elementId);

        return models\ElementClassRecord::findBySql($query);
    }

    /**
     * Find all associated elements related to basic element by property type "association"
     * @param Element $element
     * @param int|null $propertyId PropertyRecord id
     * @return array Element[]|Element[][] - описание
     * если $propertyId указан - возвращает массив в виде [element_id => Element]
     * если $propertyId указан - возвращает массив в виде [property_id => [element_id => Element]]
     * @throws HttpException
     */
    public function getAssociatedEntities(Element $element, int $propertyId = null): array
    {
        $result = $this->getRelatedElements($element->id, $propertyId, false);

        $propertyRepository = Yii::$app->propertyRepository;

        $associatedEntities = [];
        foreach ($result as $propertyId => $relation) {

            if (!$propertyRepository->isPropertyTypeAssociation($propertyId)) {
                unset($result[$propertyId]);
            } else {

                foreach ($relation->value as $element) {
                    $elementModelClass = $element->getElementClasses()->one();
                    $elementModelClassName = $elementModelClass->name;
                    $elementClassName = ClassAndContextHelper::getFullClassNameByModelClass($elementModelClassName);
                    $model = $elementClassName::instantiate();
                    $elementClassName::populateRecord($model, $element->id);
                    $associatedEntities[$propertyId][] = $model;
                }
            }
        }
        if (!empty($propertyId)) {
            return $associatedEntities[$propertyId] ?? [];
        }

        return $associatedEntities ?? [];
    }

    /**
     * Возвращает массив, ключи - $propertyId, значения - массив Associated Entities Ids
     * @param Element $element
     * @param array $propertyIds
     * @return array
     */
    public function getAssociatedEntitiesIds(Element $element, array $propertyIds = [])
    {
        $relationRecords = PropertyValueBigintRecord::find()
            ->where(['element_id' => $element->id])
            ->andWhere(['property_id' => $propertyIds])
            ->all();


        $result = [];
        foreach ($propertyIds as $propertyId) {
            $result[$propertyId] = [];
        }


        foreach ($relationRecords as $relationRecord) {
            $result[$relationRecord->property_id][] = $relationRecord->value;
        }

        return $result;
    }

    /**
     * Find all children elements related to basic element by property type "aggregation|hierarchy"
     * @param Element $element
     * @param int|null $propertyId PropertyRecord id
     * @return array Element[]|Element[][] - описание
     * если $propertyId указан - возвращает массив в виде [element_id => Element[] ]
     * если $propertyId указан - возвращает массив в виде [property_id => [element_id => Element[] ]]
     * @throws HttpException
     */
    public function getChildren(Element $element, int $propertyId = null): array
    {
        $result = $this->getRelatedElements($element->id, $propertyId, true);

        $propertyRepository = Yii::$app->propertyRepository;

        foreach ($result as $propertyId => $relation) {

            if (!$propertyRepository->isPropertyTypeAggregationOrHierarchy($propertyId)) {
                unset($result[$propertyId]);
            } else {

                foreach ($relation->value as &$element) {
                    $elementModelClass = $element->getElementClasses()->one();
                    $elementModelClassName = $elementModelClass->name;
                    $elementClassName = ClassAndContextHelper::getFullClassNameByModelClass($elementModelClassName);
                    $element = self::instantiateByARAndClassName($element, $elementClassName);
                }
            }
        }

        if (isset($result[$propertyId])) {
            $prop = $result[$propertyId];
            $children = [];
            foreach ((array)$prop->value as $childElement) {
                $childElement->properties = $this->getProperties($childElement);
                $children[] = $childElement;
            }
            return $children;
        }

        return $result;
    }

    /**
     * Find element related to this element by property type "aggregation"
     * @param Element $element this Element
     * @param int $propertyId PropertyRecord id
     * @return Element|null (domain layer object)
     * @throws HttpException
     */
    public function getAssociatedEntity(Element $element, int $propertyId): ?Element
    {
        $result = $this->getAssociatedEntities($element, $propertyId);

        return $result ? reset($result) : null;
    }

    /**
     * Find parent element related to this element by property type "aggregation|hierarchy"
     * @param Element $element this Element
     * @param int $propertyId PropertyRecord id
     * @return Element|null (domain layer object)
     * @throws HttpException
     */
    public function getParent(Element $element, int $propertyId): ?Element
    {
        $result = $this->getParents($element, $propertyId);
        $result = reset($result);

        return isset($result->value[0]) ? $result->value[0] : null;
    }

    /**
     * Find all parent elements related to basic element by property type "aggregation"
     * @param Element $element
     * @param int|null $propertyId PropertyRecord id
     * @return array Element[]|Element[][] - описание
     * если $propertyId указан - возвращает массив в виде [element_id => Element]
     * если $propertyId указан - возвращает массив в виде [property_id => [element_id => Element]]
     * @throws HttpException
     */
    public function getParents(Element $element, int $propertyId = null): array
    {
        $result = $this->getRelatedElements($element->id, $propertyId, false);

        $propertyRepository = Yii::$app->propertyRepository;

        foreach ($result as $propertyId => $relation) {

            if (!$propertyRepository->isPropertyTypeAggregationOrHierarchy($propertyId)) {
                unset($result[$propertyId]);
            } else {

                foreach ($relation->value as &$element) {
                    $elementModelClass = $element->getElementClasses()->one();
                    $elementModelClassName = $elementModelClass->name;
                    $elementClassName = ClassAndContextHelper::getFullClassNameByModelClass($elementModelClassName);
                    $element = self::instantiateByARAndClassName($element, $elementClassName);
                }
            }
        }

        return $result;
    }

    /**
     * @param int $elementId
     * @param int|null $propertyId
     * @param bool $isParent
     * @return array
     * @throws HttpException
     */
    private function getRelatedElements(int $elementId, int $propertyId = null, $isParent = false)
    {
        /** @var PropertyDBRepository $repository */
        $propertyRepository = Yii::$app->propertyRepository;

        $elementRecord = ElementRecord::findOne(['id' => $elementId]);

        //находим все свойтва данного элемента по его классам
        $elementPropertiesRecord = $this->getElementProperties($elementRecord);

        $elementAssociatedPropertiesRecord = [];
        //выбираем связи
        foreach ($elementPropertiesRecord as $key => $elementPropertyRecord) {

            if ($propertyRepository->isPropertyTypeRelation($elementPropertyRecord->id)) {
                $elementAssociatedPropertiesRecord[$key] = $elementPropertyRecord;
            }
        }

        if ($propertyId && !isset($elementAssociatedPropertiesRecord[$propertyId])) {
            throw new HttpException(404, basename(__FILE__, '.php') . __LINE__);
        }

        $properties = $propertyId ? [$propertyId] : array_keys($elementAssociatedPropertiesRecord);

        // достаем все связи по элементу
        $propertyValuesBigInt = PropertyValueBigintRecord::find()
            ->where([
                'element_id'  => $elementRecord->id,
                'property_id' => $properties,
                'is_parent'   => $isParent,
            ])
            ->all();
        // достаем все элементы
        $associatedElementRecords = ElementRecord::find([
            'id' => ArrayHelper::getColumn($propertyValuesBigInt, 'value'),
        ])
            ->indexBy('id')
            ->all();

        $result = [];

        foreach ($propertyValuesBigInt as $propertyValueBigInt) {
            $propertyBigIntId = $propertyValueBigInt->property_id;

            if (!isset($result[$propertyBigIntId])) {

                if (!isset($elementAssociatedPropertiesRecord[$propertyBigIntId])) {
                    continue;
                }
                $property = self::instantiateByARAndClassName(
                    $elementAssociatedPropertiesRecord[$propertyBigIntId],
                    Property::class
                );
                $property['value'] = [];
            } else {
                $property = $result[$propertyBigIntId];
            }

            $element = $associatedElementRecords[$propertyValueBigInt['value']] ?? null;

            if ($element) {
                $property->value[] = $element;
                $result[$propertyBigIntId] = $property;
            }
        }

        return $result;
    }

    /**
     * Find if there is s child element related to basic element by property type "aggregation|hierarchy"
     * @param Element $element
     * @param int $propertyId PropertyRecord id
     * @return boolean SUCCESS (has a parent) OR FAIL (no parents)
     * @throws HttpException
     */
    public function getIsParent(Element $element, int $propertyId): bool
    {

        // Ищем relation и проверяем его тип. Если не подходит - HttpException 404
        /** @var PropertyDBRepository $repository */
        $repository = Yii::$app->propertyRepository;

        // Проверяем на наличие записи текущего элемента
        if (!ElementRecord::findOne([
            'id' => $element->id,
        ])) {
            throw new HttpException(404, basename(__FILE__, '.php') . __LINE__);
        }

        if (!$repository->isPropertyTypeAggregationOrHierarchy($propertyId)) {
            throw new HttpException(404, basename(__FILE__, '.php') . __LINE__);
        }

        // Проверяем на наличие записи связи
        $propertyRecord = PropertyValueBigintRecord::findOne([
            'property_id' => $propertyId,
            'value'       => $element->id,
            'is_parent'   => true,
        ]);

        if (!$propertyRecord) {
            return false;
        }
        return true;
    }

    /**
     * @param Element $element
     * @param int $propertyId
     * @param bool $isReturnFullObjects
     * @param bool $isReturnLinearArray
     * @return Element[]
     * @throws Exception
     * @throws HttpException
     */
    public function getDescendants(Element $element, int $propertyId, $isReturnFullObjects = false, $isReturnLinearArray = false, $field = 'children', $allRoots = false)
    {
        $elementId = $allRoots ? null : $element->id;
        $propertyRecord = PropertyRecord::findOne(['id' => $propertyId]);

        if (!$propertyRecord) {
            throw new HttpException(404, basename(__FILE__, '.php') . __LINE__);
        }

        $classId = ClassAndContextHelper::getClassId(get_class($element));

        $descendants = $this->getDescendantsElements($classId, $propertyId, $elementId);

        $elementsIds = ArrayHelper::getColumn($descendants, function ($item) {
            return $item['element'];
        });

        $elementQuery = $element::find()
            ->where(['id' => $elementsIds])
            ->indexBy('id');

        $elementQuery = $elementQuery->withProperties($isReturnFullObjects);

        $elements = $elementQuery
            ->all();

        if ($isReturnLinearArray) {
            return $elements;
        }

        $descendantsTree = $this->buildTreeFromArray($descendants, $elements, $field);

        return $allRoots ? array_values($descendantsTree) : $descendantsTree[$element->id];
    }

    protected function buildTreeFromArray($table, $elements, $field = 'children')
    {
        $roots = [];
        foreach ($table as $item) {
            $element = $elements[$item['element']];

            if ($item['parent'] == 0) {
                $roots[$element->id] = $element;
            } else {
                $parent = $elements[$item['parent']];
                $parent->{$field} = array_merge($parent->{$field} ?? [], [$element]);
            }
        }

        return $roots;
    }

    /**
     * выбирает елементы лежащие ниже текущего, если не передан элемент, то выбираем от корней
     * @param int $classId
     * @param int $propertyId
     * @param int $elementId
     * @param int $level
     * @return array
     * @throws Exception
     */
    private function getDescendantsElements(int $classId, int $propertyId, int $elementId = null, $level = 10)
    {
        if ($elementId) {
            $elementIdString = $elementId;
        } else {
            $rootQuery = (new Query())
                ->select('value')
                ->from('property_value_bigint')
                ->where(['property_id' => $propertyId])
                ->groupBy('value')
                ->having("sum(is_parent::int) = 0");

            $rootElements = $rootQuery->column(Yii::$app->getDb());
            $elementIdString = implode(',', $rootElements);
        }

        $query = sprintf(
            'WITH RECURSIVE r AS(
                        SELECT CAST(element_id as BIGINT) as value, CAST(0 as BIGINT) as element_id, 0 as level
                            FROM property_value_bigint as pvb
                            WHERE element_id in (%s)
                        UNION
                        SELECT property_value_bigint.value, property_value_bigint.element_id, r.level+1 as level
                            FROM property_value_bigint
                            JOIN r ON property_value_bigint.element_id = r.value
                                    AND property_value_bigint.is_parent
                                    AND property_id=%s AND r.level < %s
                    )

            SELECT value as element, element_id as parent, level from r;'
            , $elementIdString, $propertyId, $level);

        $descendantsElements = Yii::$app->getDb()->createCommand($query)->queryAll();

        // добавляем элементы без привязок как корневые
        $elementsWithoutRelationsQuery = (new Query())
            ->select([
                'element' => 'element2element_class.element_id',
                'parent'  => new Expression(0),
                'level'   => new Expression(0),
            ])
            ->from('element2element_class')
            ->leftJoin('property_value_bigint', 'property_value_bigint.element_id = element2element_class.element_id')
            ->where(['element2element_class.element_class_id' => $classId])
            ->andWhere(['property_value_bigint.property_id' => null]);

        if ($elementId) {
            $elementsWithoutRelationsQuery = $elementsWithoutRelationsQuery
                ->andWhere(['element2element_class.element_id' => $elementId]);
        }

        $elementsWithoutRelations = $elementsWithoutRelationsQuery->all();

        return array_merge($descendantsElements, $elementsWithoutRelations);
    }

    #-------------------------------------------------------------------------------------------------------------------
    # /** END this part of code created by Vasiliy Konakov 2018.01.15 as a part of feature/FP-62 // @bklv */
    #-------------------------------------------------------------------------------------------------------------------

    /**
     * @param Element $element
     * @param int $propertyId
     */
    public function deletePropertyValue(Element $element, int $propertyId): void
    {
        // TODO: Implement deletePropertyValue() method.
    }

    /**
     * @param Element $element
     * @param int $propertyId
     * @return AbstractPropertyValue
     */
    public function getPropertyValue(Element $element, int $propertyId): AbstractPropertyValue
    {
        /**
         * @var Property $property
         */
        $property = (new Property())->findOne($propertyId);

        return $property->getValueByElementId($element->id);
    }

    /**
     * @param Element $element
     * @param string $propertyName
     * @return int|null
     * @throws HttpException
     */
    protected function getPropertyIdByName(Element $element, string $propertyName): ?int
    {
        foreach ($element->getElementClasses() as $elementClass) {
            $properties = $elementClass->getProperties();
            $property = $properties[$propertyName] ?? null;

            if ($property) {
                $propertyId = $property->id;
                return $propertyId;
            }
        }

        return null;
    }

    /**
     * @param BaseCrudModel $entity
     * @param int $entityId
     * @throws NotFoundHttpException
     */
    public function populateRecord(BaseCrudModel $entity, $entityId)
    {
        parent::populateRecord($entity, $entityId);
        $this->populateElementProperties($entity);
    }


    public function populateElementProperties(Element $entity)
    {
        $properties = $this->getProperties($entity);

        foreach ($properties as $key => $property) {
            $entity->setOldProperties($property->sysname, $property);
        }

        $entity->properties = $properties;
    }

    public static function translateRecord(BaseCrudModel $entity)
    {
        /** @var LocalizationService $localizationService */
        $localizationService = Yii::$app->localization;;
        $localizationService->translateEntityProperties($entity);
    }

    public function setAttributes($entity, $values, $safeOnly = true)
    {
        $attributes = $values;
        unset($attributes['properties']);

        parent::setAttributes($entity, $attributes, $safeOnly = true);

        $properties = $values['properties'] ?? [];
        $properties = array_filter($properties);

        foreach ($properties as $key => &$propertyValues) {
            if (is_array($propertyValues)) {
                $propertyValues = array_filter($propertyValues);
            }
        }

        $properties = array_filter($properties);

        if ($properties) {

            if ($entity->isNewRecord()) {
                $entity->properties = $properties;
            } else {
                foreach ($entity->propertiesAttributes() as $propertyId => $propertySysname) {
                    $newValue = $properties[$propertyId] ?? $properties[$propertySysname] ?? null;

                    if ($newValue) {
                        $oldProperty = $entity->getOldProperties($propertySysname);
                        if ($oldProperty) {
                            $oldProperty->setAttributes(['value' => $newValue], $safeOnly);
                        } else {
                            $entity->properties[$propertyId] = $newValue;
                        }
                    }
                }
            }

        }

    }

    public function attributes()
    {
        $values = parent::attributes();

        return array_merge($values, [
            'properties',
            'children',
        ]);

    }

    /**
     * @param BaseCrudModel $element
     * @return array|null
     * @throws HttpException
     */
    public function getTranslatableFields(BaseCrudModel $element)
    {
        $localizationService = $this->getLocalizationService();
        $elementClasses = $this->getElementClasses($element);

        $translatableProperties = [];

        foreach ($elementClasses as $elementClass) {
            $properties = $elementClass->getProperties();
            //Если свойсво переводимое и является строкой или текстом
            foreach ($properties as $property) {
                if ($localizationService->isTranslatableProperty($property->sysname)
                    && in_array($property->propertyTypeId, [Property::PROPERTY_TYPE_STRING, Property::PROPERTY_TYPE_TEXT])
                ) {
                    $translatableProperties[$property->sysname] = $property;
                }
            }

        }

        return $translatableProperties;
    }

    /**
     * @param null $data
     * @return mixed|null
     */
    public function getTranslatablePropertyId($data = null)
    {
        return $data->id;
    }

    /**
     * Возвращает source в зависимотси от переданный параметров
     * @param null $translatableProperty
     * @return null
     */
    public function getTranslatableSource($translatableProperty = null)
    {
        $localizationService = $this->getLocalizationService();
        $source = $localizationService->getPropertySourceByProperty($translatableProperty);

        return $source;
    }

    /**
     * @param BaseCrudModel $element
     * @param null $translatableProperty
     * @return mixed|null
     */
    public function getTranslatableRecordSetId(BaseCrudModel $element, $translatableProperty = null)
    {
        try {
            $value = $translatableProperty->getValueByElementId($element->id);
        } catch (\Exception $e) {
            $value = null;
        }

        if ($value) {
            $recordSetId = $value->searchdbPropertyValueId;
        }

        return $recordSetId ?? null;
    }
}
