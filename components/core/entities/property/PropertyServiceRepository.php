<?php

namespace commonprj\components\core\entities\property;

use commonprj\components\core\entities\propertyValidationRule\PropertyValidationRule;
use commonprj\components\core\entities\propertyVariant\PropertyVariant;
use commonprj\components\core\entities\template\Template;
use commonprj\extendedStdComponents\BaseServiceRepository;
use Yii;
use yii\httpclient\Client;
use yii\web\HttpException;

/**
 * Class PropertyServiceRepository
 * @package commonprj\components\core\entities\property
 */
class PropertyServiceRepository extends BaseServiceRepository implements IPropertyRepository
{
    /**
     * @var Client
     */
    protected $restServer;

    protected $baseUri = 'core/property/';

    protected $forbiddenMethods = ['delete'];

    /**
     * PropertyServiceRepository constructor.
     */
    public function __construct()
    {
        $this->restServer = Yii::$app->catalogManager;
    }

    /**
     * @param int $propertyId
     * @param null $multiplicityId
     * @return array
     * @throws \yii\web\HttpException
     */
    public function getValues(int $propertyId, $multiplicityId = null)
    {
        $this->requestUri = $this->getBaseUri($propertyId . '/values');
        $arModel = $this->getApiData(['multiplicityId' => $multiplicityId]);

        return $arModel;
    }

    /**
     * @param Property $property
     * @return array
     * @throws \yii\web\HttpException
     */
    public function getElementClasses(Property $property): array
    {
        $this->requestUri = $this->getBaseUri($property->id . '/element-classes');
        $arModel = $this->getApiData();

        return $arModel;
    }

    /**
     * @param Property $property
     * @return array
     * @throws \yii\web\HttpException
     */
    public function getPropertyVariants(Property $property): array
    {
        $this->requestUri = $this->getBaseUri($property->id . '/property-variants');
        $arModel = $this->getApiData();

        return $arModel;
    }

    /**
     * @param Property $property
     * @return array
     * @throws \yii\web\HttpException
     */
    public function getListItems(Property $property)
    {
        $this->requestUri = $this->getBaseUri($property->id . '/list-items');
        $arModel = $this->getApiData();

        return $arModel;
    }

    /**
     * @param Property $property
     * @param array $listItems
     * @return array|mixed
     */
    public function setListItems(Property $property, array $listItems)
    {
        $requestParams = [
            'items' => $listItems,
        ];

        $response = $this->restServer->post($this->getBaseUri($property->id . '/list-items'), $requestParams)->send();

        if (!$response->getIsOk()) {
            return false;
        }

        return $response->getData();
    }

    /**
     * @param Property $property
     * @param array $listItems
     * @return null
     * @throws HttpException
     * @throws \yii\httpclient\Exception
     */
    public function saveWithListItems(Property $property, array $listItems)
    {
        // Сохранение Property
        $this->saveProperty($property);

        // Сохранение ListItems
        $requestParams = [
            'items' => $listItems,
        ];
        $createListItemsResponse = $this->setListItems($property, $listItems);

        // Удаление Property если не прошел запрос на сохранение ListItems
        if ($createListItemsResponse === false) {

            if ($this->isNew($property)) {
                $this->restServer->delete($this->getBaseUri($property->id), $requestParams)->send();
            }

            throw new HttpException(500, 'Не удалось сохранить Property listItems');
        }

        $createdPropertyData['items'] = $createListItemsResponse;

        return $createdPropertyData;
    }

    /**
     * @param Property $property
     * @return bool
     * @throws HttpException
     * @throws \yii\httpclient\Exception
     */
    private function saveProperty(Property $property): bool
    {
        foreach ($property->getAttributes() as $key) {
            if (!in_array($key, ['items', 'repository', 'entity'])) {
                $requestParams[$key] = $property->{$key};
            }
        }

        if ($this->isNew($property)) {
            $savePropertyResponse = $this->restServer->post($this->getBaseUri(), $requestParams)->send();
        } else {
            $savePropertyResponse = $this->restServer->put($this->getBaseUri($property->id), $requestParams)->send();
        }

        if (!$savePropertyResponse->getIsOk()) {
            $content = $savePropertyResponse->getData();
            $message = (!empty($content)) ? $content['message'] ?? '' : '';
            throw new HttpException($savePropertyResponse->getStatusCode(), $message);
        }

        if (!isset($savePropertyResponse->headers['id'])) {
            throw new HttpException(500, 'Не удалось сохранить Property');
        }

        if ($this->isNew($property)) {
            $property->id = $savePropertyResponse->headers['id'];
        }

        return true;
    }

    /**
     * @param Property $property
     * @return PropertyVariant|null
     */
    public function getPropertyVariant(Property $property, int $propertyValueId, int $multiplicityId): ?PropertyVariant
    {
        // TODO: Implement getPropertyVariant() method.
    }

    /**
     * @param Property $property
     * @return PropertyValidationRule[]
     */
    public function getPropertyValidationRules(Property $property): array
    {
        // TODO: Implement getPropertyValidationRules() method.
    }

    /**
     * @param Property $property
     * @param int $elementClassId
     * @param int $propertyValueId
     * @param int|null $multiplicityId
     * @param bool $isInHierarchy
     * @return Template|null
     * @throws HttpException
     * @throws \yii\httpclient\Exception
     */
    public function getTemplate(Property $property, int $elementClassId, int $propertyValueId, int $multiplicityId = null, bool $isInHierarchy = false): ?Template
    {
        $this->requestUri = $this->getBaseUri($property->id . '/template');

        $queryParams = [
            'elementClassId' => $elementClassId,
            'propertyValueId' => $propertyValueId,
            'multiplicityId' => $multiplicityId,
            'isInHierarchy' => $isInHierarchy,
        ];

        $content = $this->getApiData($queryParams);

        if (!empty($content)) {
            $result = $this->getOneModel($content, Template::className());
        }

        return $result ?? null;
    }


}
