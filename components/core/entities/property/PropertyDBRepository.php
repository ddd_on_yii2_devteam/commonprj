<?php
/**
 * Created by daSilva.Rodrigues
 * Date: 17.06.2016
 */

namespace commonprj\components\core\entities\property;

use commonprj\components\core\entities\abstractPropertyValue\AbstractPropertyValue;
use commonprj\components\core\entities\elementClass\ElementClass;
use commonprj\components\core\entities\propertyArrayValue\PropertyArrayValue;
use commonprj\components\core\entities\propertyGroup\PropertyGroup;
use commonprj\components\core\entities\propertyListItem\PropertyListItem;
use commonprj\components\core\entities\propertyRangeValue\PropertyRangeValue;
use commonprj\components\core\entities\PropertyTreeItem\PropertyTreeItem;
use commonprj\components\core\entities\propertyUnit\PropertyUnit;
use commonprj\components\core\entities\propertyValueListItem\PropertyValueListItem;
use commonprj\components\core\entities\propertyValueTreeItem\PropertyValueTreeItem;
use commonprj\components\core\entities\propertyVariant\PropertyVariant;
use commonprj\components\core\entities\template\Template;
use commonprj\components\core\helpers\ClassAndContextHelper;
use commonprj\components\core\models\Property2elementClassRecord;
use commonprj\components\core\models\PropertyListItemRecord;
use commonprj\components\core\models\PropertyRangeRecord;
use commonprj\components\core\models\PropertyRecord;
use commonprj\components\core\models\PropertyTreeItemRecord;
use commonprj\components\core\models\PropertyTypeRecord;
use commonprj\components\core\models\PropertyValueTreeItemRecord;
use commonprj\components\core\models\search as Search;
use commonprj\extendedStdComponents\BaseCrudModel;
use commonprj\extendedStdComponents\BaseDBRepository;
use commonprj\extendedStdComponents\IBaseCrudModel;
use Throwable;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Exception;
use yii\web\HttpException;
use yii\web\ServerErrorHttpException;

/**
 * Class PropertyRepository
 * @package commonprj\components\core\entities\property
 */
Class PropertyDBRepository extends BaseDBRepository implements IPropertyRepository
{
    const SORT_PARAMS = [
        'id'             => [
            'asc'     => ['id' => SORT_ASC],
            'desc'    => ['id' => SORT_DESC],
            'default' => SORT_ASC,
            'label'   => 'Id',
        ],
        'name'           => [
            'asc'     => ['name' => SORT_ASC],
            'desc'    => ['name' => SORT_DESC],
            'default' => SORT_ASC,
            'label'   => 'Name',
        ],
        'description'    => [
            'asc'     => ['description' => SORT_ASC],
            'desc'    => ['description' => SORT_DESC],
            'default' => SORT_ASC,
            'label'   => 'Description',
        ],
        'propertyUnitId' => [
            'asc'     => ['property_unit_id' => SORT_ASC],
            'desc'    => ['property_unit_id' => SORT_DESC],
            'default' => SORT_ASC,
            'label'   => 'Property unit id',
        ],
        'isSpecific'     => [
            'asc'     => ['is_specific' => SORT_ASC],
            'desc'    => ['is_specific' => SORT_DESC],
            'default' => SORT_ASC,
            'label'   => 'Is specific',
        ],
        'propertyTypeId' => [
            'asc'     => ['property_type_id' => SORT_ASC],
            'desc'    => ['property_type_id' => SORT_DESC],
            'default' => SORT_ASC,
            'label'   => 'Property type id',
        ],
        'sysname'        => [
            'asc'     => ['sysname' => SORT_ASC],
            'desc'    => ['sysname' => SORT_DESC],
            'default' => SORT_ASC,
            'label'   => 'Sysname',
        ],
    ];

    /**
     * @var string
     */
    public $activeRecordClass = PropertyRecord::class;

    /**
     * @var array
     */
    private static $valueTypes;

    /**
     * @param int $typeId
     * @return string|false
     */
    public static function getPropertyValueClassByTypeId(int $typeId)
    {
        if (!is_array(self::$valueTypes)) {
            $propertyTypeRecords = PropertyTypeRecord::find()
                ->where(['parent_id' => Property::PROPERTY_TYPE_PROPERTY_ID])->all();

            foreach ($propertyTypeRecords as $type) {
                $elementClass = $type->getElementClass()->one();

                if ($elementClass) {
                    $modelClass = $elementClass->getAttribute('name');
                    self::$valueTypes[$type->id] = ClassAndContextHelper::getFullClassNameByModelClass($modelClass);
                } else {
                    self::$valueTypes[$type->id] = false;
                }
            }
        }

        return self::$valueTypes[$typeId] ?? false;
    }

    /**
     * @param $attributes
     * @return bool
     * @throws HttpException
     */
    public static function validateProperty($attributes)
    {
        $attributes = BaseDBRepository::arrayKeysCamelCase2Underscore($attributes);
        $property = self::instantiateByARAndClassName($attributes,
            'commonprj\components\core\entities\property\Property');

        if (!$property->validate()) {
            $firstErrors = $property->getFirstErrors();
            $firstKey = array_keys($firstErrors)[0];
            $errorMessage = reset($firstErrors);
            throw new HttpException(400,
                "attribute: {$firstKey}. {$errorMessage} " . basename(__FILE__, '.php') . __LINE__);
        }

        /** @var ActiveRecord $validPropertyId */
        $validPropertyId = PropertyRecord::find()->where([
            'id' => $property['id'],
        ])->scalar();

        if ($validPropertyId === false) {
            throw new HttpException(400, 'Wrong property id given. ' . basename(__FILE__, '.php') . __LINE__);
        }

        return true;
    }

    /**
     * Сохранение переданного объекта посредством active record
     * @param IBaseCrudModel $property
     * @param bool $runValidation
     * @param array|null $attributes
     * @return bool
     * @throws \yii\web\NotFoundHttpException
     */
    public function save(IBaseCrudModel $property, $runValidation = true, $attributes = null): bool
    {
        /** @var Property $property */
        $descriptionTranslations = $this->generateTranslationList($property->description);
        $nameTranslations = $this->generateTranslationList($property->name);

        $property->description = $descriptionTranslations['eng'] ?? $descriptionTranslations['transliterate'] ?? $property->label;
        $property->name = $nameTranslations['eng'] ?? $nameTranslations['transliterate'] ?? $property->name;

        $result = parent::save($property);
        //получаем ID записи соответствующее этому значению в searchDB
        $idInSearchDB = $property->id;

        // Если была запись и у нас есть переводы, то добавляем их в базу переводов
        if (is_array($descriptionTranslations) && $idInSearchDB) {
            $this->addTranslations('search.' . $this->activeRecordClass::tableName() . '.description',
                $descriptionTranslations,
                null,
                $idInSearchDB
            );
        }

        if (is_array($nameTranslations) && $idInSearchDB) {
            $this->addTranslations('search.' . $this->activeRecordClass::tableName() . '.name',
                $nameTranslations,
                null,
                $idInSearchDB
            );
        }

        return $result === true;
    }

    /**
     * @param int $id
     * @return array
     * @throws HttpException
     */
    public function getPropertyClassesById(int $id)
    {
        $elementRecord = PropertyRecord::findOne($id);
        $elementClassRecords = $elementRecord->getElementClasses()->all();
        $result = [];

        foreach ($elementClassRecords as $elementClassRecord) {
            $result[$elementClassRecord['id']] = self::instantiateByARAndClassName($elementClassRecord,
                'commonprj\components\core\entities\property\Property');
        }

        return $result;
    }

    /**
     * @param int $propertyId
     * @param null $multiplicityId
     * @return array
     * @throws HttpException
     */
    public function getValues(int $propertyId, $multiplicityId = null)
    {
        $propertyRecord = PropertyRecord::findOne($propertyId);
        $propertyTypeId = $propertyRecord->getPropertyType()->one()->id;
        $propertyValueClass = self::getPropertyValueClassByTypeId($propertyTypeId);

        if (!$propertyValueClass) {
            throw new HttpException('500', 'Нельзя вызвать метод для получения значений этого типа Property');
        }

        $result = [];

        switch ($multiplicityId) {
            case AbstractPropertyValue::MULTIPLICITY_RANGE:
                $propertyRangeValue = new PropertyRangeValue();

                foreach ($propertyRangeValue->getValues($propertyRecord, $multiplicityId) as $value) {
                    $result['propertyValuesByMultiplicityId'][AbstractPropertyValue::MULTIPLICITY_RANGE][$value->id] = $value;
                }
                break;
            case AbstractPropertyValue::MULTIPLICITY_ARRAY:
                $propertyArrayValue = new PropertyArrayValue();

                foreach ($propertyArrayValue->getValues($propertyRecord) as $value) {
                    $result['propertyValuesByMultiplicityId'][AbstractPropertyValue::MULTIPLICITY_ARRAY][$value->id] = $value;
                }
                break;
            default:
                $propertyValue = new $propertyValueClass;

                foreach ($propertyValue->getValues($propertyRecord, $multiplicityId) as $value) {
                    $result['propertyValuesByMultiplicityId'][AbstractPropertyValue::MULTIPLICITY_SCALAR][$value->id] = $value;
                }
                break;
        }

        return $result;
    }

    /**
     * @param int $propertyId
     * @param int $elementId
     * @return bool|BaseCrudModel|ActiveRecord
     * @throws HttpException
     */
    public function getValueByElementId(int $propertyId, int $elementId)
    {
        $propertyTypeId = PropertyRecord::findOne($propertyId)->getAttribute('property_type_id');
        $propertyValueClass = self::getPropertyValueClassByTypeId($propertyTypeId);

        if (!$propertyValueClass) {
            return false;
        }

        $condition = [
            'property_id' => $propertyId,
            'element_id'  => $elementId,
        ];

        $values = $propertyValueClass::find()
            ->where([
                'property_id' => $propertyId,
                'element_id'  => $elementId,
            ])
            ->all(Yii::$app->getDb());

        $valuesCount = count($values);

        if ($valuesCount == 0) {
            throw new HttpException(404, basename(__FILE__, '.php') . __LINE__);
        }

        if ($valuesCount <= 2) {
            // range property
            if ($propertyValue = $this->getRangePropertyValue($condition, $values)) {
                return $propertyValue;
            }
        }

        if ($valuesCount > 0 || in_array($propertyTypeId, [Property::PROPERTY_TYPE_LIST, Property::PROPERTY_TYPE_TREE])) {
            // array property
            if ($propertyValue = $this->getArrayPropertyValue($condition, $values)) {
                return $propertyValue;
            }
        }

        if ($valuesCount == 1) {
            // scalar property
            return $values[0];
//            return self::instantiateByARAndClassName($values[0], $propertyValueClass);
        }

        throw new HttpException(404, basename(__FILE__, '.php') . __LINE__);
    }

    /**
     * @param array $condition
     * @param array $values
     * @return bool|BaseCrudModel|ActiveRecord
     * @throws HttpException
     */
    private function getArrayPropertyValue(array $condition, array $values)
    {
        $propertyArrayValue = PropertyArrayValue::find()
            ->where($condition)
            ->one(Yii::$app->getDb());

        if (!$propertyArrayValue) {
            return false;
        }
        $items = [];

        foreach ($values as $propertyValue) {
            if ($propertyValue instanceof PropertyValueListItem) {
                $itemValue = $propertyValue->repository->getListItemByParams(['id' => $propertyValue->value]);
                $value = $itemValue->item ?? null;
            } else {
                $value = $propertyValue->value;
            }
            $items[] = $value;
        }

        $propertyArrayValue->values = $items;
        $propertyArrayValue->setOldAttribute('values', $items);

        return $propertyArrayValue;
    }

    /**
     * @param array $condition
     * @param array $values
     * @return bool|BaseCrudModel|ActiveRecord
     * @throws HttpException
     */
    private function getRangePropertyValue(array $condition, array $values)
    {
        if ($rangeValueRecord = PropertyRangeRecord::find()->where($condition)->one()) {
            $propertyValue = self::instantiateByARAndClassName($rangeValueRecord, PropertyRangeValue::class);

            foreach ($values as $value) {
                if ($propertyValue->fromValueId == $value['id']) {
                    $propertyValue->fromValue = $value['value'];
                }
                if ($propertyValue->toValueId == $value['id']) {
                    $propertyValue->toValue = $value['value'];
                }
            }

            return $propertyValue;
        } else {
            return false;
        }
    }

    /**
     * @param $propertyUnitId
     * @return array|ActiveRecord
     */
    public function getPropertyUnitById($propertyUnitId)
    {
        $propertyRecord = PropertyRecord::findOne($propertyUnitId);
        $propertyUnitRecord = $propertyRecord->getPropertyUnit()->one();

        if (is_null($propertyUnitRecord)) {
            return [];
        } else {
            return $propertyUnitRecord;
        }
    }

    /**
     * @param int $elementClassId
     * @param int $id
     * @return bool
     * @throws HttpException
     * @throws Exception
     */
    public function deletePropertyClassByClassId(int $elementClassId, int $id)
    {
        $propertyRecord = PropertyRecord::findOne($id);

        $deletionRow = $propertyRecord->getProperty2elementClasses()->where(['element_class_id' => $elementClassId])->all();

        if (!$deletionRow) {
            throw new HttpException(404, basename(__FILE__, '.php') . __LINE__);
        }

        Yii::$app->getDb()->beginTransaction();

        try {
            BaseCrudModel::deleteRows($deletionRow);
        } catch (ServerErrorHttpException $e) {
            Yii::$app->getDb()->getTransaction()->rollBack();
            throw new HttpException(500, 'Failed to delete the object for unknown reason. ' . basename(__FILE__,
                    '.php') . __LINE__);
        }

        Yii::$app->getDb()->getTransaction()->commit();
        return true;
    }

    /**
     * @param IBaseCrudModel $baseCrudModel
     * @return bool
     * @throws Exception
     * @throws HttpException
     * @throws Throwable
     * @throws \Exception
     */
    public function delete(IBaseCrudModel $baseCrudModel): bool
    {
        $propertyRecord = PropertyRecord::findOne($baseCrudModel->id);

        if (is_null($propertyRecord)) {
            throw new HttpException(404, basename(__FILE__, '.php') . __LINE__);
        }

        $elementsCount = Search\PropertyValue2elementRecord::find()->where(['property_id' => $baseCrudModel->id])->count();

        if ($elementsCount != 0) {
            throw new \Exception('Can`t delete property while it is used by elements');
        }

        $transaction = Yii::$app->getDb()->beginTransaction();

        // Delete related listItems and treeItems
        $listItems = PropertyListItemRecord::find()
            ->where(['property_id' => $baseCrudModel->id])->all();

        $treeItems = PropertyTreeItemRecord::find()
            ->where(['property_id' => $baseCrudModel->id])->all();

        foreach (array_merge($listItems, $treeItems) as $listAndTreeItem) {
            if ($listAndTreeItem instanceof ActiveRecord) {
                $result = $listAndTreeItem->delete();

                if ($result === false) {
                    $transaction->rollBack();
                    throw new \Exception(sprintf("Не удалось удаление listItem или treeItem"));
                }
            }
        }

        $result = $propertyRecord->delete();

        if ($result === false) {
            $transaction->rollBack();
            throw new \Exception(sprintf("Не удалось удаление property"));
        }

        $transaction->commit();
        return true;
    }

    /**
     * @return string[]
     */
    public function primaryKey()
    {
        return PropertyRecord::primaryKey();
    }

    /**
     * Устанавливает значение свойства для сущности
     * @param array $attributes
     * @return bool
     * @throws HttpException
     */
    public function saveElementProperty(Property $property, array $attributes): bool
    {

        $propertyRecord = PropertyRecord::findOne($property->id);

        if (!$propertyRecord) {
            throw new HttpException(404, basename(__FILE__, '.php') . __LINE__);
        }

        $concretePropertyClass = self::getPropertyValueClassByTypeId($propertyRecord->getAttribute('property_type_id'));

        if (!isset($attributes['multiplicityId'])) {
            $attributes['multiplicityId'] = AbstractPropertyValue::MULTIPLICITY_SCALAR;
        }
        $attributes['propertyId'] = $property->id;

        switch ($attributes['multiplicityId']) {
            case AbstractPropertyValue::MULTIPLICITY_ARRAY:
                try {
                    $propertyValue = $property->getValueByElementId($attributes['elementId']);
                    $propertyValue->values = $attributes['values'];
                } catch (\Exception $e) {
                    $propertyValue = new PropertyArrayValue($attributes);
                }
                break;
            case AbstractPropertyValue::MULTIPLICITY_RANGE:
                $propertyValue = new PropertyRangeValue($attributes);
                break;
            default:
                try {
                    $propertyValue = $property->getValueByElementId($attributes['elementId']);
                    $propertyValue->value = $attributes['value'];
                } catch (\Exception $e) {
                    $propertyValue = new $concretePropertyClass($attributes);
                }
                break;
        }

        return $propertyValue->save() === true ?: false;
    }

    /**
     * Метод определяет название типа характеристики по property id
     * @param int $id
     * @return mixed
     */
    public static function getPropertyTypeNameByPropertyId(int $id)
    {
        $propertyTypeId = self::getPropertyTypeIdByPropertyId($id);

        return PropertyTypeRecord::find()->select(['name'])->where(['id' => $propertyTypeId])->scalar();
    }

    /**
     * Метод определяет название типа характеристики по property id
     * @param int $id
     * @return mixed
     */
    public static function getPropertyTypeIdByPropertyId(int $id)
    {
        return PropertyRecord::find()->select('property_type_id')->where(['id' => $id])->scalar();
    }

    /**
     * Удаляет значения свойства
     * @param $propertyValueId
     * @param $propertyId
     * @return bool
     * @throws HttpException
     * @throws Exception
     */
    public function deletePropertyValueByValueId($propertyValueId, $propertyId)
    {
        $propertyRecord = PropertyRecord::findOne($propertyId);
        $propertyTypeName = $propertyRecord->getPropertyType()->select('name')->one()['name'];
        $getValueByTypeName = "get{$propertyTypeName}PropertyValues";
        $deletionRow = $propertyRecord->$getValueByTypeName()->where(['id' => $propertyValueId])->all();

        if (!$deletionRow) {
            throw new HttpException(404, basename(__FILE__, '.php') . __LINE__);
        }

        Yii::$app->getDb()->beginTransaction();

        try {
            BaseCrudModel::deleteRows($deletionRow);
        } catch (\Exception $e) {
            Yii::$app->getDb()->getTransaction()->rollBack();
            return false;
        }

        Yii::$app->getDb()->getTransaction()->commit();
        return true;
    }

    /**
     * Check property - type any relation
     * @param int $propertyId
     * @return boolean SUCCESS OR FAIL
     */
    public function isPropertyTypeRelation(int $propertyId): bool
    {
        $propertyType = PropertyRecord::findOne(['id' => $propertyId])->getPropertyType()->one();
        return $propertyType->parent_id === Property::PROPERTY_TYPE_RELATION_ID;
    }

    /**
     * Check if property type is relation association
     * @param int $propertyId
     * @return boolean SUCCESS OR FAIL
     */
    public function isPropertyTypeAssociation(int $propertyId): bool
    {
        return $this->isPropertyTypeInArray($propertyId, [Property::PROPERTY_TYPE_RELATION_ASSOCIATION_ID]);
    }

    /**
     * Check if property type is relation aggregation
     * @param int $propertyId
     * @return boolean SUCCESS OR FAIL
     */
    public function isPropertyTypeAggregation(int $propertyId): bool
    {
        return $this->isPropertyTypeInArray($propertyId, [Property::PROPERTY_TYPE_RELATION_AGGREGATION_ID]);
    }

    /**
     * Check if property type is relation hierarchy
     * @param int $propertyId
     * @return boolean SUCCESS OR FAIL
     */
    public function isPropertyTypeHierarchy(int $propertyId): bool
    {
        return $this->isPropertyTypeInArray($propertyId, [Property::PROPERTY_TYPE_RELATION_HIERARCHY_ID]);
    }

    /**
     * Check property - type relation `Aggregation Or Hierarchy`
     * @param int $propertyId
     * @return bool
     */
    public function isPropertyTypeAggregationOrHierarchy(int $propertyId): bool
    {
        return $this->isPropertyTypeInArray($propertyId,
            [Property::PROPERTY_TYPE_RELATION_AGGREGATION_ID, Property::PROPERTY_TYPE_RELATION_HIERARCHY_ID]);
    }

    /**
     * Check if property type in the given array
     * @param int $propertyId
     * @param int[] $propertyTypeIds
     * @return boolean SUCCESS OR FAIL
     */
    public function isPropertyTypeInArray(int $propertyId, array $propertyTypeIds): bool
    {
        $propertyRecord = PropertyRecord::findOne(['id' => $propertyId]);

        return in_array($propertyRecord->property_type_id, $propertyTypeIds);
    }

    /**
     * @param int $propertyId
     * @return bool
     */
    public function isPropertyTypeList(int $propertyId): bool
    {
        return $this->isPropertyTypeInArray($propertyId, [Property::PROPERTY_TYPE_LIST, Property::PROPERTY_TYPE_TREE]);
    }

    /**
     * @param Property $property
     * @return PropertyListItem[]
     * @throws HttpException
     */
    public function getListItems(Property $property)
    {
        $propertyRecord = PropertyRecord::findOne($property->id);

        if (!$propertyRecord || !$this->isPropertyTypeList($property->id)) {
            throw new HttpException(404, basename(__FILE__, '.php') . __LINE__);
        }

        $listClass = $propertyRecord['property_type_id'] == Property::PROPERTY_TYPE_TREE
            ? PropertyTreeItem::class : PropertyListItem::class;
        $model = new $listClass();

        return $model->getTree($property->id);
    }

    /**
     * @param Property $property
     * @param array $listItems
     * @return array
     * @throws HttpException
     */
    public function setListItems(Property $property, array $listItems)
    {
        $propertyRecord = PropertyRecord::findOne($property->id);

        if (!$propertyRecord || !$this->isPropertyTypeList($property->id)) {
            throw new HttpException(404, basename(__FILE__, '.php') . __LINE__);
        }

        $listClass = $propertyRecord['property_type_id'] == Property::PROPERTY_TYPE_TREE
            ? PropertyTreeItem::class : PropertyListItem::class;

        $existingItems = $listClass::find()
            ->where(['propertyId' => $property->id])
            ->indexBy('item')
            ->all();

        $result = [];

        foreach ((array)$listItems as $key => $listItem) {
            $params = [
                'propertyId' => $property->id,
            ];

            if (is_array($listItem) && array_key_exists('item', $listItem)) {
                $item = $listItem['item'];
                $label = $listItem['label'] ?? null;
            } else {
                $item = $key;
                $label = $listItem;
            }

            $params['item'] = (string)$item;

            $propertyListItem = $existingItems[$item] ?? new $listClass($params);

            if ($label) {
                $propertyListItem->label = $label;
            }

            if ($propertyListItem->hasAttribute('parentId')) {
                $propertyListItem->parentId = 0;
            }

            $result = $propertyListItem->save();

            if ($result !== true) {
                throw new HttpException(404, basename(__FILE__, '.php') . __LINE__);
            }

            $children = $listItem['children'] ?? null;

            if ($children && $propertyListItem instanceof PropertyTreeItem) {
                $this->setListItemsChildren($property->id, $propertyListItem->id, $children);
            }
        }

        return $result;
    }

    private function setListItemsChildren($propertyId, $parentId, $items)
    {
        $existingItems = PropertyTreeItem::find()
            ->where(['propertyId' => $propertyId])
            ->indexBy('item')
            ->all();

        foreach ($items as $child) {

            $item = $child['item'];
            $params = [
                'propertyId' => $propertyId,
                'parentId'   => $parentId,
                'item'       => (string)$item,
                'label'      => $child['label'],
            ];

            $propertyTreeItem = $existingItems[$item] ?? new PropertyTreeItem($params);

            $result = $propertyTreeItem->save();

            if ($result !== true) {
                throw new HttpException(404, basename(__FILE__, '.php') . __LINE__);
            }

            $children = $child['children'] ?? null;

            if ($children) {
                $this->setListItemsChildren($propertyId, $propertyTreeItem->id, $children);
            }
        }
    }

    /**
     * @param Property $property
     * @return array|ElementClass[]
     * @throws HttpException
     */
    public function getElementClasses(Property $property): array
    {
        /**
         * @var PropertyRecord $propertyRecord
         */
        $propertyRecord = PropertyRecord::findOne($property->id);

        if (!$propertyRecord) {
            throw new HttpException(404, basename(__FILE__, '.php') . __LINE__);
        }

        $result = [];

        foreach ($propertyRecord->getElementClasses()->all() as $elementClassRecord) {
            $result[] = self::instantiateByARAndClassName($elementClassRecord, ElementClass::class);
        }

        return $result;
    }

    /**
     * Возвращает PropertyTypeRecord приведенный к массиву
     * @param Property $property
     * @return array
     * @throws HttpException
     */
    public function getType(Property $property): array
    {
        /**
         * @var PropertyRecord $propertyRecord
         */
        $propertyRecord = PropertyRecord::findOne($property->id);

        if (!$propertyRecord) {
            throw new HttpException(404, basename(__FILE__, '.php') . __LINE__);
        }

        return $propertyRecord->getPropertyType()->one()->toArray();
    }

    /**
     * @param Property $property
     * @return array
     * @throws HttpException
     */
    public function getPropertyVariants(Property $property): array
    {
        /**
         * @var PropertyRecord $propertyRecord
         */
        $propertyRecord = Search\PropertyRecord::findOne($property->id);

        if (!$propertyRecord) {
            throw new HttpException(404, basename(__FILE__, '.php') . __LINE__);
        }

        return $propertyRecord->getPropertyVariants()->all();
    }

    /**
     * @param Property $property
     * @param int $propertyValueId
     * @return PropertyVariant|null
     * @throws HttpException
     */

    public function getPropertyVariant(Property $property, int $propertyValueId, int $multiplicityId): ?PropertyVariant
    {
        /**
         * @var PropertyRecord $propertyRecord
         */
        $propertyRecord = Search\PropertyRecord::findOne($property->id);

        if (!$propertyRecord) {
            throw new HttpException(404, basename(__FILE__, '.php') . __LINE__);
        }

        $result = $propertyRecord->getPropertyVariants()
            ->where([
                'property_value_id'        => $propertyValueId,
                'property_multiplicity_id' => $multiplicityId,
            ])
            ->one();

        $model = PropertyVariant::instantiate();
        PropertyVariant::populateRecord($model, $result);
        return $model;
    }


    public function getProperty2elementClass()
    {
        $properties = Property2elementClassRecord::find()
            ->select(['element_class_id', 'property_id', 'propertySysname' => 'property.sysname'])
            ->joinWith('property', false)
            ->asArray()
            ->all();

        return $properties;
    }

    /**
     * Заполняет сущность данными
     * @param BaseCrudModel $entity
     * @param $entityId
     */
    public function populateRecord(BaseCrudModel $entity, $entityId)
    {
        parent::populateRecord($entity, $entityId);

        $propertyUnit = PropertyUnit::findOne($entity->propertyUnitId);
        $propertyGroup = PropertyGroup::findOne($entity->propertyGroup);

        $entity->propertyUnit = $propertyUnit;
        $entity->propertyGroup = $propertyGroup;
    }

    public function attributes()
    {
        $values = parent::attributes();
        return array_merge($values, ['value']);
    }

    /**
     * @param Property $property
     * @return array
     * @throws HttpException
     */
    public function getPropertyValidationRules(Property $property): array
    {
        $propertyVariants = $this->getPropertyVariants($property);
        foreach ($propertyVariants as &$propertyVariant) {
            $propertyVariant->getPropertyValidationRule();
        }
        return $propertyVariants ?? [];
    }

    /**
     * Возвращает source в зависимотси от переданный параметров
     * @param null $data
     * @return null
     */
    public function getTranslatableSource($data = null)
    {
        $localizationService = $this->getLocalizationService();
        $sourceString = sprintf("search.%s.%s",
            $this->activeRecordClass::tableName(),
            $data
        );

        $source = $localizationService->getSourceBySourceString($sourceString);

        return $source;
    }

    /**
     * Возвращает список переводимых полей в формате $field => $data
     * @param BaseCrudModel $entity
     * @return null
     */
    public function getTranslatableFields(BaseCrudModel $entity)
    {
        return [
            'name'        => 'name',
            'description' => 'description',
        ];
    }

    /**
     * @param Property $property
     * @param int $elementClassId
     * @param int $propertyValueId
     * @param int|null $multiplicityId
     * @param bool $isInHierarchy
     * @return Template|null
     */
    public function getTemplate(Property $property, int $elementClassId, int $propertyValueId, int $multiplicityId = null, bool $isInHierarchy = false): ?Template
    {
        $multiplicityId = $multiplicityId ?? $multiplicityId = AbstractPropertyValue::MULTIPLICITY_SCALAR;

        if ($isInHierarchy && $property->propertyTypeId == Property::PROPERTY_TYPE_TREE) {
            return $this->getPropertyTreeTemplate($property, $elementClassId, $propertyValueId, $multiplicityId);
        }

        return $this->getSimpleTemplate($property, $elementClassId, $propertyValueId, $multiplicityId);
    }

    /**
     * @param Property $property
     * @param int $elementClassId
     * @param int $propertyValueId
     * @param int|null $multiplicityId
     * @return Template
     */
    private function getPropertyTreeTemplate(Property $property, int $elementClassId, int $propertyValueId, int $multiplicityId = null)
    {
        $propertyValueRecord = PropertyValueTreeItemRecord::findOne($propertyValueId);
        $propertyTreeItem = PropertyTreeItem::findOne($propertyValueRecord->value);

        // propertyIds собственного шаблона
        $currentTemplate = $this->getSimpleTemplate($property, $elementClassId, $propertyValueId, $multiplicityId);
        $templateProperties = ($currentTemplate) ? $currentTemplate->propertyIds : [];

        // propertyIds родительских шаблонов
        foreach ($propertyTreeItem->getParents() as $propertyTreeItem) {
            $parenTemplate = $this->getSimpleTemplate($property, $elementClassId, $propertyTreeItem->id, $multiplicityId);
            if ($parenTemplate) {
                $templateProperties = array_merge($templateProperties, $parenTemplate->propertyIds);
            }
        }
        $templateProperties = array_unique($templateProperties);

        $template = new Template();
        $template->load([
            'propertyIds' => $templateProperties
        ], '');
        $template->populateProperties();

        return $template;
    }

    /**
     * @param Property $property
     * @param int $elementClassId
     * @param int $propertyValueId
     * @param int|null $multiplicityId
     * @return array|BaseCrudModel|null
     */
    private function getSimpleTemplate(Property $property, int $elementClassId, int $propertyValueId, int $multiplicityId = null)
    {
        /**
         * @var Search\PropertyVariantRecord $propertyVariantRecord
         */
        $propertyVariantRecord = Search\PropertyVariantRecord::findOne(
            [
                'property_id'              => $property->id,
                'property_value_id'        => $propertyValueId,
                'property_multiplicity_id' => $multiplicityId,
            ]
        );

        if (is_null($propertyVariantRecord)) {
            return null;
        }

        $template = Template::find()->where([
            'elementClassId'   => $elementClassId,
            'propertyVariantId' => $propertyVariantRecord->id,
        ])->one();

        return $template;
    }

}
