<?php
/**
 * Created by daSilva.Rodrigues
 * Date: 23.09.2016
 */

namespace commonprj\components\core\entities\property;

use commonprj\components\core\entities\elementClass\ElementClass;
use commonprj\components\core\entities\propertyVariant\PropertyVariant;
use commonprj\components\core\entities\template\Template;
use commonprj\extendedStdComponents\IBaseRepository;
use yii\db\ActiveRecord;

/**
 * Class PropertyRepository
 * @package commonprj\components\core\entities\property
 */
interface IPropertyRepository extends IBaseRepository
{
    /**
     * @param int $id
     * @return array
     */
//    public function getPropertyClassesById(int $id);

    /**
     * @param int $propertyId
     * @param null $multiplicityId
     * @return array
     */
    public function getValues(int $propertyId, $multiplicityId = null);

    /**
     * @param $propertyUnitId
     * @return array|ActiveRecord
     */
//    public function getPropertyUnitById($propertyUnitId);

    /**
     * @param Property $property
     * @param array $attributes
     * @return bool
     */
//    public function saveElementProperty(Property $property, array $attributes): bool;

    /**
     * @param Property $property
     * @return ElementClass[]
     */
    public function getElementClasses(Property $property): array;

    /**
     * @param Property $property
     * @return PropertyVariant[]
     */
    public function getPropertyVariants(Property $property): array;


    /**
     * @param Property $property
     * @param int $propertyValueId
     * @param int $multiplicityId
     * @return PropertyVariant|null
     */
    public function getPropertyVariant(Property $property, int $propertyValueId, int $multiplicityId): ?PropertyVariant;

    /**
     * @param Property $property
     * @return PropertyValidationRule[]
     */
    public function getPropertyValidationRules(Property $property): array;

    /**
     * @param Property $property
     * @param int $elementClassId
     * @param int $propertyValueId
     * @param int|null $multiplicityId
     * @param bool $isInHierarchy
     * @return Template|null
     */
    public function getTemplate(Property $property, int $elementClassId, int $propertyValueId, int $multiplicityId = null, bool $isInHierarchy = false): ?Template;
}
