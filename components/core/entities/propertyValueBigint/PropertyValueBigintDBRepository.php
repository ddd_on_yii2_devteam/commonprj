<?php
/**
 * @package         FurniPrice
 * @subpackage      Core
 * @category        DBRepository
 * @created         17.06.2016
 * @author          daSilva.Rodrigues
 * @refactoredBy    Vasiliy Konakov
 * @updated         13.12.2017
 */

namespace commonprj\components\core\entities\propertyValueBigint;

use commonprj\components\core\entities\abstractPropertyValue\AbstractPropertyValueDBRepository;
use commonprj\components\core\models\PropertyValueBigintRecord;

/**
 * Class propertyValueBigintDBRepository
 * @package commonprj\components\core\entities\propertyValueBigint
 */
class PropertyValueBigintDBRepository extends AbstractPropertyValueDBRepository implements IPropertyValueBigintRepository
{
    public $activeRecordClass = PropertyValueBigintRecord::class;

}
