<?php
/**
 * Created by daSilva.Rodrigues
 * Date: 22.08.2016
 */

namespace commonprj\components\core\entities\propertyValueTreeItem;

use commonprj\components\core\entities\propertyValueListItem\PropertyValueListItem;
use yii\db\ActiveRecord;

/**
 * Class ListItemProperty
 * @package commonprj\components\core\entities\listItemProperty
 */
class PropertyValueTreeItem extends PropertyValueListItem
{
    public $label;
    public $value;

    public function __construct(array $config = [], ActiveRecord $activeRecord = null)
    {
        parent::__construct($config);
        $this->repository = \Yii::$app->propertyValueTreeRepository;
    }

}