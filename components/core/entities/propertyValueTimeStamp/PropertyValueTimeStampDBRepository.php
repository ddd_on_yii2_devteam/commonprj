<?php
/**
 * Created by daSilva.Rodrigues
 * Date: 22.08.2016
 */

namespace commonprj\components\core\entities\propertyValueTimeStamp;

use commonprj\components\core\entities\abstractPropertyValue\AbstractPropertyValueDBRepository;
use commonprj\components\core\models\PropertyValueTimestampRecord;

/**
 * Class TimeStampPropertyDBRepository
 * @package commonprj\components\core\entities\timeStampProperty
 */
class PropertyValueTimeStampDBRepository extends AbstractPropertyValueDBRepository
{
    public $activeRecordClass = PropertyValueTimestampRecord::class;
}