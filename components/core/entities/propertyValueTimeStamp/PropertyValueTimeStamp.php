<?php
/**
 * Created by daSilva.Rodrigues
 * Date: 22.08.2016
 */

namespace commonprj\components\core\entities\propertyValueTimeStamp;

use commonprj\components\core\entities\abstractPropertyValue\AbstractPropertyValue;
use yii\db\ActiveRecord;

/**
 * Class TimeStampProperty
 * @package commonprj\components\core\entities\timeStampProperty
 */
class PropertyValueTimeStamp extends AbstractPropertyValue
{
    public function __construct(array $config = [], ActiveRecord $activeRecord = null)
    {
        parent::__construct($config);
        $this->repository = \Yii::$app->propertyValueTimeStampRepository;
    }
}