<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 20.03.2018
 * Time: 16:54
 */

namespace commonprj\components\core\entities\propertyValueJson;

use commonprj\components\core\entities\abstractPropertyValue\AbstractPropertyValue;
use Yii;
use yii\db\ActiveRecord;

class PropertyValueJson extends AbstractPropertyValue
{
    public function __construct(array $config = [], ActiveRecord $activeRecord = null)
    {
        parent::__construct($config);
        $this->repository = Yii::$app->propertyValueJsonRepository;
    }
}