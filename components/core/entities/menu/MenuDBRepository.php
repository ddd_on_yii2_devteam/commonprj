<?php

namespace commonprj\components\core\entities\menu;

use commonprj\components\core\entities\abstractPropertyValue\AbstractPropertyValue;
use commonprj\components\core\entities\elementCategory\ElementCategory;
use commonprj\components\core\entities\elementClass\ElementClass;
use commonprj\components\core\helpers\PostgreSqlArrayHelper;
use commonprj\components\core\models\search\PropertyArrayRecord;
use commonprj\components\core\models\search\MenuRecord;
use commonprj\extendedStdComponents\BaseDBRepository;
use Yii;

/**
 * Class MenuDBRepository
 * @package commonprj\components\core\entities\menu
 */
class MenuDBRepository extends BaseDBRepository implements IMenuRepository
{
    public $activeRecordClass = MenuRecord::class;

    /**
     * Получение класса элемента
     * @param Menu $menu
     * @return ElementClass
     */
    public function getElementClass(Menu $menu)
    {
        return ElementClass::find()->where(['id' => $menu->elementClassId])->one();
    }

    /**
     * Получение дерева категорий по ID меню
     * @param Menu $menu
     * @return array
     */
    public function getElementCategories(Menu $menu)
    {
        $propertyService = Yii::$app->propertyService;

        $elementCategories = ElementCategory::find()
            ->where(['menuId' => $menu->id])
            ->indexBy('id')
            ->all();

        foreach ($elementCategories as $elementCategory) {
            $table[] = [
                'element' => $elementCategory['id'],
                'parent'  => $elementCategory['parentId'] ?? 0,
            ];

            $propertyVariant = $elementCategory->propertyVariant ?? null;

            if ($propertyVariant) {
                //Если хранит ссылку на массив - достаем даные из массива
                if ($propertyVariant && $propertyVariant->propertyMultiplicityId == AbstractPropertyValue::MULTIPLICITY_ARRAY) {
                    $propertyArrayRecord = PropertyArrayRecord::findOne(['id' => $propertyVariant->propertyValueId]);
                    $propertyVariant->propertyValueId = PostgreSqlArrayHelper::pg2array($propertyArrayRecord->value_ids);
                } else {
                    $propertyVariant->propertyValueId = [$propertyVariant->propertyValueId];
                }

                $propertySysname = $propertyService->getSysnameById($propertyVariant->propertyId);

                $propertyVariant->propertySysname = $propertySysname;
            }
        }

        $result = $this->buildTreeFromArray($table, $elementCategories);

        return $result;
    }


}