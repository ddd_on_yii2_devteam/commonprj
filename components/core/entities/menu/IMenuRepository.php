<?php

namespace commonprj\components\core\entities\menu;

use commonprj\components\core\entities\elementClass\ElementClass;
use commonprj\extendedStdComponents\IBaseRepository;

interface IMenuRepository extends IBaseRepository
{
    /**
     * Получение класса элемента
     * @param Menu $menu
     * @return ElementClass
     */
    public function getElementClass(Menu $menu);

    /**
     * Получение дерева категорий по ID меню
     * @param Menu $menu
     * @return array
     */
    public function getElementCategories(Menu $menu);
}
