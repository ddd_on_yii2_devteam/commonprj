<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 4/10/2018
 * Time: 1:05 PM
 */

namespace commonprj\components\core\entities\menu;

use commonprj\components\core\entities\elementClass\ElementClass;
use commonprj\extendedStdComponents\BaseCrudModel;
use Yii;

class Menu extends BaseCrudModel
{
    public $id;
    public $elementClassId;
    public $name;

    /**
     * Присвоение свойству доменного слоя $repository - соответствующего компонента
     * @inheritdoc
     */
    public function __construct($config = [])
    {
        parent::__construct($config);
        $this->repository = Yii::$app->menuRepository;
    }

    /**
     * Получение класса элемента
     * @return ElementClass
     */
    public function getElementClass()
    {
        return $this->repository->getElementClass($this);
    }

    /**
     * Получение дерева категорий по ID меню
     * @return array
     */
    public function getElementCategories()
    {
        return $this->repository->getElementCategories($this);
    }
}