<?php

namespace commonprj\components\core\entities\propertyListItem;

use commonprj\components\core\models\PropertyListItemRecord;
use commonprj\components\core\models\PropertyRecord;
use commonprj\extendedStdComponents\BaseDBRepository;
use commonprj\extendedStdComponents\IBaseCrudModel;
use yii\db\Exception;
use yii\db\Expression;
use yii\web\HttpException;

/**
 * Class PropertyListItemDBRepository
 * @package commonprj\components\core\entities\propertyListItem
 */
class PropertyListItemDBRepository extends BaseDBRepository
{
    public $activeRecordClass = PropertyListItemRecord::class;

    /**
     * Сохранение переданного объекта посредством active record
     * @param IBaseCrudModel $propertyListItem
     * @param bool $runValidation
     * @param null $attributes
     * @return bool
     * @throws HttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function save(IBaseCrudModel $propertyListItem, $runValidation = true, $attributes = null): bool
    {
        // Если не задан item, то ищем максимальный текущий и прибавляем 1..
        // если вернулся null значит item должен быть строковым обязателен
        if (!$propertyListItem->item) {
            $maxItem = $this->getLastItem($propertyListItem->propertyId);

            if ($maxItem === null) {
                throw new HttpException(404, basename(__FILE__, '.php') . __LINE__);
            }

            $propertyListItem->item = (string)($maxItem + 1);
        }

        $translations = $this->generateTranslationList($propertyListItem->label);
        $propertyListItem->label = $translations['eng'] ?? $translations['transliterate'] ?? $propertyListItem->label;

        $result = parent::save($propertyListItem);

        $propertyValueIdInSearchDB = $propertyListItem->searchdbPropertyValueId;
        if (is_array($translations) && $propertyValueIdInSearchDB) {
            $this->addTranslations('search.' . $this->activeRecordClass::tableName() . '.label',
                $translations,
                null,
                $propertyValueIdInSearchDB,
                $propertyListItem->propertyId
            );
        }

        return $result;
    }

    public function getTree($entity, $propertyId)
    {
        $propertyRecord = PropertyRecord::findOne($propertyId);

        if (!$propertyRecord) {
            throw new HttpException(404, basename(__FILE__, '.php') . __LINE__);
        }

        $listItemsQuery = $entity::find()
            ->indexBy('id');

        if ($propertyId) {
            $listItemsQuery->andWhere(['propertyId' => $propertyId]);
        }

        $listItems = $listItemsQuery->all();

        if (!$listItems) {
            return [];
        }

        foreach ($listItems as $listItem) {
            $table[] = [
                'element' => $listItem['id'],
                'parent'  => $listItem['parentId'] ?? 0,
            ];
        }

        $result = $this->buildTreeFromArray($table, $listItems);

        return $result;
    }

    public function getLastItem($propertyId)
    {
        try {
            $maxItem = $this->activeRecordClass::find()
                ->select(['maxItem' => new Expression('max("item"::integer)')])
                ->where(['property_id' => $propertyId])
                ->column();
            $maxItem = reset($maxItem);
        } catch (Exception $e) {
            $maxItem = null;
        }

        return $maxItem;
    }

}
