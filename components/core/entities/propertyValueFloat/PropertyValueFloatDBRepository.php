<?php

namespace commonprj\components\core\entities\propertyValueFloat;

use commonprj\components\core\entities\abstractPropertyValue\AbstractPropertyValueDBRepository;
use commonprj\components\core\models\PropertyValueFloatRecord;

/**
 * Class FloatPropertyDBRepository
 * @package commonprj\components\core\entities\propertyValueFloat
 */
class PropertyValueFloatDBRepository extends AbstractPropertyValueDBRepository
{
    public $activeRecordClass = PropertyValueFloatRecord::class;
}