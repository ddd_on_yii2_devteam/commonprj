<?php
/**
 * Created by daSilva.Rodrigues
 * Date: 17.08.2016
 */

namespace commonprj\components\core\helpers;

use commonprj\components\core\models\ContextRecord;
use commonprj\components\core\models\ElementClassRecord;
use yii\db\ActiveRecord;
use yii\web\HttpException;

/**
 * Class BaseCrudModel
 * @package commonprj\components\core\entities
 */
class ClassAndContextHelper
{
    protected static $pathFormat = 'commonprj\\components\\%s\\entities\\%s\\%s';

    /**
     * Получение id из таблицы element_class по имени класса.
     * @param string $fullClassName - Имя класса включая неймспейс.
     * @return int
     */
    public static function getClassId(string $fullClassName)
    {
        $entityClassName = str_replace('DBRepository', '', $fullClassName);
        $contextAndClassName = self::getContextAndClassName($entityClassName);
        return self::getElementClassIdByClassAndContextName($contextAndClassName['className']);
    }

    /**
     * @param string $fullClassName
     * @return array
     */
    public static function getContextAndClassName(string $fullClassName)
    {
        $list = explode('\\', $fullClassName);
        $count = count($list);
        $className = $list[$count - 1];
        $contextName = $list[$count - 4];

        $fullClassName = "{$contextName}\\{$className}";

        return [
            'contextName' => $contextName,
            'className'   => $fullClassName,
        ];
    }

    public static function getFullClassNameByModelClass(string $modelClassName)
    {
        list($contextName, $className) = explode('\\', $modelClassName);

        return sprintf(self::$pathFormat,
            $contextName,
            lcfirst($className),
            $className
        );
    }

    /**
     * Метод для получения id из таблицы element_class по имени класса и контекста.
     * @param string $className
     * @return int
     * @throws HttpException
     */
    public static function getElementClassIdByClassAndContextName(string $className)
    {
        $elementClassRecord = ElementClassRecord::findOne(['name' => $className]);
        if (!$elementClassRecord) {
            throw new HttpException(404, basename(__FILE__, '.php') . __LINE__);
        }

        return $elementClassRecord['id'];
    }

    /**
     * @param string $modelName
     * @return string
     */
    public static function getActiveRecordByModelName(string $modelName)
    {
        return "commonprj\\components\\core\\models\\{$modelName}Record";
    }

    /**
     * @param string $coreActiveRecordClass
     * @return string
     */
    public static function getSearchRecordClassByCoreRecordClass(string $coreActiveRecordClass): string
    {
        return str_replace('\\models\\', '\\models\\search\\', $coreActiveRecordClass);
    }

    /**
     * @param string $elementClassName
     * @return null|string|ActiveRecord::class
     */
    public static function getActiveRecordClassByElementClassName(string $elementClassName)
    {
        list($_, $name) = explode('\\', $elementClassName);
        $activeRecordClass = "commonprj\\components\\core\\models\\search\\{$name}Record";

        if (class_exists($activeRecordClass)) {
            return $activeRecordClass;
        }

        return null;
    }
}