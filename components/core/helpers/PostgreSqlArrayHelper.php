<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 27.12.2017
 * Time: 15:13
 */

namespace commonprj\components\core\helpers;


class PostgreSqlArrayHelper
{
    /**
     * @param string $valueIds
     * @return array
     */
    public static function pg2array(string $valueIds): array
    {
        return explode(',', trim($valueIds, '{}'));
    }

    /**
     * @param array $array
     * @return string
     */
    public static function array2pg(array $array) : string
    {
        return sprintf('{%s}', implode(',', $array));
    }
}