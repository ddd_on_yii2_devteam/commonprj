<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 1/18/2018
 * Time: 12:51 PM
 */

namespace commonprj\components\core\helpers;


use commonprj\components\core\entities\element\Element;
use commonprj\components\core\models\Property2elementClassRecord;

class PropertyHelper
{
    private static $propertyMap;

    public static function getPropertyIdByElementAndSysname(Element $element, string $propertySysname)
    {
        $elementClasses = $element->getElementClasses();

        if (!self::$propertyMap) {
            $properties = Property2elementClassRecord::find()
                ->select(['element_class_id', 'property_id', 'propertySysname' => 'property.sysname'])
                ->joinWith('property', false)
                ->asArray()
                ->all();

            foreach ($properties as $property) {
                self::$propertyMap[$property['element_class_id']][$property['propertySysname']] = $property['property_id'];
            }
        }

        foreach ($elementClasses as $elementClass) {
            $propertyId = self::$propertyMap[$elementClass->id][$propertySysname] ?? null;

            if ($propertyId) {
                return $propertyId;
            }
        }

        throw new NotFoundHttpException('Property Relation Not Found');
    }
}