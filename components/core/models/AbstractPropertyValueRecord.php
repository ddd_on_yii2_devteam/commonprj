<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 29.12.2017
 * Time: 18:58
 */

namespace commonprj\components\core\models;

use commonprj\components\core\helpers\PostgreSqlArrayHelper;
use commonprj\services\SearchDBSynchService;
use yii\db\ActiveRecord;

abstract class AbstractPropertyValueRecord extends ActiveRecord
{
    /**
     * @return array|bool|false|int|null|ActiveRecord
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     */
    public function delete()
    {
        /**
         * @var ActiveRecord $propertyValueRecordClass
         */
        $propertyValueRecordClass = $this->getActiveRecordName();

        $transaction = \Yii::$app->getDb()->beginTransaction();

        try {

            switch (self::className()) {

                case PropertyArrayRecord::class:
                    $valueIds = PostgreSqlArrayHelper::pg2array($this->getAttribute('value_ids'));
                    $scalarPropertyValues = $propertyValueRecordClass::find()->where(['id' => $valueIds])->all();

                    foreach ($scalarPropertyValues as $scalarPropertyValue) {
                        $scalarPropertyValue->delete();
                    }

                    $searchDbSynch = new SearchDBSynchService($this);
                    $result = $searchDbSynch->deletePropertyValue();
                    break;

                case PropertyRangeRecord::class:

                    foreach (['from_value_id', 'to_value_id'] as $key) {
                        $valueId = $this->getAttribute($key);

                        if ($valueId) {
                            $scalarPropertyValue = $propertyValueRecordClass::findOne($valueId);

                            if ($scalarPropertyValue) {
                                $scalarPropertyValue->delete();
                            }
                        }
                    }

                    break;

                default:
                    break;
            }
            $searchDbSynch = new SearchDBSynchService($this);
            $result = $searchDbSynch->deletePropertyValue();

            $transaction->commit();
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }

        if ($result) {
            return parent::delete();
        } else {
            $transaction->rollBack();
            return $result;
        }
    }

    /**
     * @return string
     */
    private function getActiveRecordName()
    {
        $className = $this->getProperty()->one()->getPropertyType()->one()->getElementClass()->one()->name;

        $search = 'core\\';
        $replace = sprintf('%s\\', __NAMESPACE__);

        return str_replace($search, $replace, $className) . 'Record';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getElement()
    {
        return $this->hasOne(ElementRecord::className(), ['id' => 'element_id']);
    }
}