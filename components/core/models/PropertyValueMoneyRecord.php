<?php

namespace commonprj\components\core\models;

use Yii;

/**
 * This is the model class for table "property_value_money".
 *
 * @property int $id
 * @property int $property_id
 * @property int $value
 * @property int $currency_list_item_id
 *
 * @property PropertyRecord $property
 */
class PropertyValueMoneyRecord extends AbstractPropertyValueRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'property_value_money';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['property_id', 'value', 'element_id', 'currency_list_item_id'], 'required'],
            [['property_id', 'value', 'element_id', 'currency_list_item_id'], 'default', 'value' => null],
            [['property_id', 'value', 'element_id', 'currency_list_item_id'], 'integer'],
            [['property_id'], 'exist', 'skipOnError' => true, 'targetClass' => PropertyRecord::className(), 'targetAttribute' => ['property_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'property_id' => 'Property ID',
            'value' => 'Value',
            'element_id' => 'Element ID',
            'currency_list_item_id' => 'Currency List Item ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(PropertyRecord::className(), ['id' => 'property_id']);
    }
}
