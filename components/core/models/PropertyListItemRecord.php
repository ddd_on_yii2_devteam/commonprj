<?php

namespace commonprj\components\core\models;

use Yii;

/**
 * This is the model class for table "property_list_item".
 *
 * @property int $id
 * @property int $property_id
 * @property string $item
 * @property string $label
 * @property int $searchdb_property_value_id
 *
 * @property PropertyRecord $property
 * @property PropertyValueListItemRecord[] $propertyValueListItems
 */
class PropertyListItemRecord extends AbstractBasicRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'property_list_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['property_id', 'item', 'label'], 'required'],
            [['property_id', 'searchdb_property_value_id', 'sort_order'], 'default', 'value' => null],
            [['property_id', 'searchdb_property_value_id', 'sort_order'], 'integer'],
            [['item', 'label'], 'string', 'max' => 255],
            [['property_id', 'label'], 'unique', 'targetAttribute' => ['property_id', 'label']],
            [['property_id', 'item'], 'unique', 'targetAttribute' => ['property_id', 'item']],
            [['property_id'], 'exist', 'skipOnError' => true, 'targetClass' => PropertyRecord::className(), 'targetAttribute' => ['property_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'property_id' => 'Property ID',
            'item' => 'Item',
            'label' => 'Label',
            'searchdb_property_value_id' => 'Searchdb Property Value ID',
            'sort_order' => 'Sort Order',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(PropertyRecord::className(), ['id' => 'property_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyValueListItems()
    {
        return $this->hasMany(PropertyValueListItemRecord::className(), ['value' => 'id']);
    }
}
