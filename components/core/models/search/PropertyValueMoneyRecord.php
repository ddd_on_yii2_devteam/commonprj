<?php

namespace commonprj\components\core\models\search;

use Yii;

/**
 * This is the model class for table "property_value_money".
 *
 * @property int $id
 * @property int $property_id
 * @property int $value
 * @property int $currency_list_item_id
 * @property int $value_in_base_currency
 *
 * @property PropertyRecord $property
 */
class PropertyValueMoneyRecord extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'property_value_money';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbSearch');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['property_id', 'value', 'currency_list_item_id', 'value_in_base_currency'], 'required'],
            [['property_id', 'value', 'currency_list_item_id', 'value_in_base_currency'], 'default', 'value' => null],
            [['property_id', 'value', 'currency_list_item_id', 'value_in_base_currency'], 'integer'],
            [['property_id'], 'exist', 'skipOnError' => true, 'targetClass' => PropertyRecord::className(), 'targetAttribute' => ['property_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'property_id' => 'Property ID',
            'value' => 'Value',
            'currency_list_item_id' => 'Currency List Item ID',
            'value_in_base_currency' => 'Value In Base Currency',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(PropertyRecord::className(), ['id' => 'property_id']);
    }
}
