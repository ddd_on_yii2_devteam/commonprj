<?php

namespace commonprj\components\core\models\search;

use Yii;

/**
 * This is the model class for table "property_value_int".
 *
 * @property integer $id
 * @property integer $property_id
 * @property integer $value
 *
 * @property PropertyRecord $property
 */
class PropertyValueIntRecord extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'property_value_int';
    }

    /**
     * @return null|object|\yii\db\Connection
     */
    public static function getDb()
    {
        return Yii::$app->get('dbSearch');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['property_id', 'value'], 'required'],
            [['property_id', 'value'], 'integer'],
            [['property_id'], 'exist', 'skipOnError' => true, 'targetClass' => PropertyRecord::className(), 'targetAttribute' => ['property_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'property_id' => 'Property ID',
            'value' => 'Value',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(PropertyRecord::className(), ['id' => 'property_id']);
    }
}
