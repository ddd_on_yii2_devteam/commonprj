<?php

namespace commonprj\components\core\models\search;

use Yii;

/**
 * This is the model class for table "property_value2element".
 *
 * @property integer $id
 * @property integer $property_id
 * @property integer $property_multiplicity_id
 * @property integer $property_value_id
 * @property integer $element_id
 * @property boolean $is_active
 */
class PropertyValue2elementRecord extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'property_value2element';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbSearch');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['property_id', 'property_multiplicity_id', 'property_value_id', 'element_id'], 'integer'],
            [['property_multiplicity_id', 'property_value_id', 'element_id'], 'required'],
            [['is_active'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'property_id' => 'Property ID',
            'property_multiplicity_id' => 'Property Multiplicity ID',
            'property_value_id' => 'Property Value ID',
            'element_id' => 'Element ID',
            'is_active' => 'Is Active',
        ];
    }
}
