<?php

namespace commonprj\components\core\models\search;

use Yii;

/**
 * This is the model class for table "element2element_class".
 *
 * @property integer $element_class_id
 * @property integer $element_id
 *
 * @property ElementClassRecord $elementClass
 */
class Element2elementClassRecord extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'element2element_class';
    }

    /**
     * @return null|object|\yii\db\Connection
     */
    public static function getDb()
    {
        return Yii::$app->get('dbSearch');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['element_class_id', 'element_id'], 'required'],
            [['element_class_id', 'element_id'], 'integer'],
            [['element_class_id'], 'exist', 'skipOnError' => true, 'targetClass' => ElementClassRecord::className(), 'targetAttribute' => ['element_class_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'element_class_id' => 'Element Class ID',
            'element_id' => 'Element ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getElementClass()
    {
        return $this->hasOne(ElementClassRecord::className(), ['id' => 'element_class_id']);
    }
}
