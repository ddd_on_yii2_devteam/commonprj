<?php

namespace commonprj\components\core\models\search;

use Yii;

/**
 * This is the model class for table "menu".
 *
 * @property int $id
 * @property int $element_class_id
 * @property string $name
 *
 * @property ElementCategoryRecord[] $elementCategories
 * @property ElementClassRecord $elementClass
 */
class MenuRecord extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbSearch');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['element_class_id', 'name'], 'required'],
            [['element_class_id'], 'default', 'value' => null],
            [['element_class_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['element_class_id'], 'exist', 'skipOnError' => true, 'targetClass' => ElementClassRecord::className(), 'targetAttribute' => ['element_class_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'element_class_id' => 'Element Class ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getElementCategories()
    {
        return $this->hasMany(ElementCategoryRecord::className(), ['menu_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getElementClass()
    {
        return $this->hasOne(ElementClassRecord::className(), ['id' => 'element_class_id']);
    }
}
