<?php

namespace commonprj\components\core\models\search;

use Yii;

/**
 * This is the model class for table "property_validation_rule".
 *
 * @property int $id
 * @property int $property_variant_id
 * @property string $data
 *
 * @property PropertyVariant $propertyVariant
 */
class PropertyValidationRuleRecord extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'property_validation_rule';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbSearch');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['property_variant_id'], 'default', 'value' => null],
            [['property_variant_id'], 'integer'],
            [['data'], 'string'],
            [['property_variant_id'], 'exist', 'skipOnError' => true, 'targetClass' => PropertyVariantRecord::className(), 'targetAttribute' => ['property_variant_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'property_variant_id' => 'Property Variant ID',
            'data' => 'Data',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyVariant()
    {
        return $this->hasOne(PropertyVariantRecord::className(), ['id' => 'property_variant_id']);
    }
}
