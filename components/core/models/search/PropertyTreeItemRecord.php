<?php

namespace commonprj\components\core\models\search;

use Yii;

/**
 * This is the model class for table "property_tree_item".
 *
 * @property int $id
 * @property int $property_id
 * @property string $item
 * @property string $label
 * @property int $parent_id
 *
 * @property PropertyRecord $property
 */
class PropertyTreeItemRecord extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'property_tree_item';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbSearch');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['property_id', 'item', 'label'], 'required'],
            [['property_id', 'parent_id', 'sort_order'], 'default', 'value' => null],
            [['property_id', 'parent_id', 'sort_order'], 'integer'],
            [['item', 'label'], 'string', 'max' => 255],
            [['property_id', 'item'], 'unique', 'targetAttribute' => ['property_id', 'item']],
            [['property_id'], 'exist', 'skipOnError' => true, 'targetClass' => PropertyRecord::className(), 'targetAttribute' => ['property_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'property_id' => 'Property ID',
            'item' => 'Item',
            'label' => 'Label',
            'parent_id' => 'Parent ID',
            'sort_order' => 'Sort Order',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(Property::className(), ['id' => 'property_id']);
    }
}
