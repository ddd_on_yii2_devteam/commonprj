<?php

namespace commonprj\components\core\models;

use yii\db\ActiveRecord;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "element".
 *
 * @property integer $id
 * @property integer $sort_order
 * @property boolean $is_active
 *
 * @property Element2elementClassRecord[] $element2elementClasses
 * @property ElementClassRecord[] $elementClasses
 */
class ElementRecord extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'element';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sort_order'], 'integer'],
            [['is_active'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sort_order' => 'Sort Order',
            'is_active' => 'Is Active',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getElement2elementClasses()
    {
        return $this->hasMany(Element2elementClassRecord::className(), ['element_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getElementClasses()
    {
        return $this->hasMany(ElementClassRecord::className(), ['id' => 'element_class_id'])->viaTable('element2element_class', ['element_id' => 'id']);
    }
}
