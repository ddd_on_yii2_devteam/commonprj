<?php

namespace commonprj\components\catalog\entities\priceCategory;

use commonprj\components\crm\entities\manufacturer\Manufacturer;
use commonprj\extendedStdComponents\BaseServiceRepository;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * Class PriceCategoryServiceRepository
 * @package commonprj\components\catalog\entities\priceCategory
 */
class PriceCategoryServiceRepository extends BaseServiceRepository
{

    /**
     * @var string
     */
    protected $baseUri = 'price-category/';

    /**
     * PropertyServiceRepository constructor.
     */
    public function __construct()
    {
        $this->restServer = Yii::$app->catalogManager;
    }

    /**
     * @param PriceCategory $priceCategory
     * @param int $manufacturerId
     * @return bool
     */
    public function bindManufacturer(PriceCategory $priceCategory, int $manufacturerId): bool
    {
        $uri = $this->getBaseUri($priceCategory->id . '/manufacturer/' . $manufacturerId);
        $response = $this->restServer->post($uri)->send();
        return $response->getIsOk();
    }

    /**
     * @param PriceCategory $priceCategory
     * @param int $manufacturerId
     * @return bool
     */
    public function unbindManufacturer(PriceCategory $priceCategory, int $manufacturerId): bool
    {
        $uri = $this->getBaseUri($priceCategory->id . '/manufacturer/' . $manufacturerId);
        $response = $this->restServer->delete($uri)->send();
        return $response->getIsOk();
    }

    /**
     * @param PriceCategory $priceCategory
     * @return \commonprj\extendedStdComponents\BaseCrudModel|null
     * @throws \yii\web\HttpException
     */
    public function getManufacturer(PriceCategory $priceCategory): ?Manufacturer
    {
        $this->requestUri = $this->getBaseUri($priceCategory->id . '/manufacturer');
        $arModel = $this->getApiData();
        $result = $this->getOneModel($arModel, Manufacturer::className());

        if (empty($result)) {
            throw new NotFoundHttpException('У материала не найдено производителя', 404);
        }

        return $result;
    }

}
