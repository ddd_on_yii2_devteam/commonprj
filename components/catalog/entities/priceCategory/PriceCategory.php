<?php

namespace commonprj\components\catalog\entities\priceCategory;

use commonprj\components\core\entities\element\Element;
use commonprj\components\crm\entities\manufacturer\Manufacturer;
use Yii;

/**
 * Class PriceCategory
 * @package commonprj\components\catalog\entities\priceCategory
 */
class PriceCategory extends Element
{
    public $materialCollections;
    public $productMaterials;

    /**
     * Присвоение свойству доменного слоя $repository - соответствующего компонента
     * @inheritdoc
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->repository = Yii::$app->priceCategoryRepository;
    }

    /**
     * @return boolean
     */
    public function bindManufacturer(int $manufacturerId): bool
    {
        return $this->repository->bindManufacturer($this, $manufacturerId);
    }

    /**
     * @return boolean
     */
    public function unbindManufacturer(int $manufacturerId): bool
    {
        return $this->repository->unbindManufacturer($this, $manufacturerId);
    }

    /**
     * @return Manufacturer
     */
    public function getManufacturer(): ?Manufacturer
    {
        return $this->repository->getManufacturer($this);
    }

}
