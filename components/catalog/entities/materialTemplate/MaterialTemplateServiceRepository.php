<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 28.02.2018
 * Time: 17:28
 */

namespace commonprj\components\catalog\entities\materialTemplate;

use commonprj\components\core\entities\element\ElementServiceRepository;
use commonprj\components\core\entities\property\Property;
use commonprj\extendedStdComponents\BaseCrudModel;
use Yii;
use yii\httpclient\Client;

/**
 * Class MaterialTemplateServiceRepository
 * @package commonprj\components\catalog\entities\materialTemplate
 */
class MaterialTemplateServiceRepository extends ElementServiceRepository
{
    /**
     * @var Client
     */
    protected $restServer;

    /**
     * @var string
     */
    protected $baseUri = 'material-template/';

    /**
     * PropertyServiceRepository constructor.
     */
    public function __construct()
    {
        $this->restServer = Yii::$app->catalogManager;
    }

    /**
     * @param MaterialTemplate $materialTemplate
     * @return array
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    public function getMaterials(MaterialTemplate $materialTemplate): array
    {
        $this->requestUri = $this->getBaseUri($materialTemplate->id . '/materials');
        $content = $this->getApiData();

        if (!empty($content)) {
            $result = $this->getArrayOfModels($content, MaterialTemplate::className());
        }

        return $result ?? [];
    }

    /**
     * @param BaseCrudModel $entity
     * @param $data
     */
    public function populateRecord(BaseCrudModel $entity, $data)
    {
        parent::populateRecord($entity, $data);
        $this->populateRelatedProperties($entity, $data);
    }

    /**
     * @param $entity
     * @param $data
     */
    protected function populateRelatedProperties($entity, $data){

        $propertyBySysname = $entity->getPropertyFromPropertiesBySysname(Property::PROPERTY_SYSNAME__PROPERTIES);

        if ($propertyBySysname) {
            $propertiesIds = $propertyBySysname->value;
            $populatedProperties = Property::findAll(['id__in' => implode(',', $propertiesIds)]);
            $entity->relatedProperties = $populatedProperties['items'] ?? [];
        }
    }

}