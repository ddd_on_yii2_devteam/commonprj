<?php

namespace commonprj\components\catalog\entities\component;

use commonprj\components\catalog\entities\productMaterial\ProductMaterial;
use commonprj\components\core\entities\element\Element;
use commonprj\components\crm\entities\manufacturer\Manufacturer;
use Yii;

/**
 * Class Component
 * @property IComponentRepository $repository
 * @package commonprj\components\catalog\entities\component
 */
class Component extends Element
{

    /**
     * Присвоение свойству доменного слоя $repository - соответствующего компонента
     * @inheritdoc
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->repository = Yii::$app->componentRepository;
    }

    /**
     *
     * @return array
     */
    public function getMaterials(): array
    {
        return $this->repository->getMaterials($this);
    }

    /**
     *
     * @param int $materialId
     * @return bool
     */
    public function bindMaterial(int $materialId): bool
    {
        return $this->repository->bindMaterial($this, $materialId);
    }

    /**
     *
     * @param int $materialId
     * @return bool
     */
    public function unbindMaterial(int $materialId): bool
    {
        return $this->repository->unbindMaterial($this, $materialId);
    }

    /**
     * @return Manufacturer
     */
    public function getManufacturer()
    {
        return $this->repository->getManufacturer($this);
    }

    /**
     * @param int $manufacturerId
     * @return bool
     */
    public function bindManufacturer(int $manufacturerId): bool
    {
        return $this->repository->bindManufacturer($this, $manufacturerId);
    }

    /**
     * @param int $manufacturerId
     * @return bool
     */
    public function unbindManufacturer(int $manufacturerId): bool
    {
        return $this->repository->unbindManufacturer($this, $manufacturerId);
    }

    /**
     * @return ProductMaterial[]
     */
    public function getProductMaterials()
    {
        return $this->repository->getProductMaterials($this);
    }

    /**
     * @param int $manufacturerId
     * @return bool
     */
    public function bindProductMaterial(int $manufacturerId): bool
    {
        return $this->repository->bindProductMaterial($this, $manufacturerId);
    }

    /**
     * @param int $manufacturerId
     * @return bool
     */
    public function unbindProductMaterial(int $manufacturerId): bool
    {
        return $this->repository->unbindProductMaterial($this, $manufacturerId);
    }
}
