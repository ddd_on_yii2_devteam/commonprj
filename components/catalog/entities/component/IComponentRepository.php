<?php

namespace commonprj\components\catalog\entities\component;

use commonprj\components\catalog\entities\material\Material;
use commonprj\components\crm\entities\manufacturer\Manufacturer;

interface IComponentRepository
{

    /**
     * @param Component $component
     * @return Material[]
     */
    public function getMaterials(Component $component): array;

    /**
     * @param Component $component
     * @param int $materialId
     * @return bool
     */
    public function bindMaterial(Component $component, int $materialId): bool;

    /**
     * @param Component $component
     * @param int $materialId
     * @return bool
     */
    public function unbindMaterial(Component $component, int $materialId): bool;

    /**
     * @param Component $component
     * @return Manufacturer
     */
    public function getManufacturer(Component $component);

    /**
     * @param Component $component
     * @param int $manufacturerId
     * @return bool
     */
    public function bindManufacturer(Component $component, int $manufacturerId): bool;

    /**
     * @param Component $component
     * @param int $manufacturerId
     * @return bool
     */
    public function unbindManufacturer(Component $component, int $manufacturerId): bool;

    /**
     * @param Component $component
     * @return ProductMaterial
     */
    public function getProductMaterials(Component $component);

    /**
     * @param Component $component
     * @param int $productMaterialId
     * @return bool
     */
    public function bindProductMaterial(Component $component, int $productMaterialId): bool;

    /**
     * @param Component $component
     * @param int $productMaterialId
     * @return bool
     */
    public function unbindProductMaterial(Component $component, int $productMaterialId): bool;



}