<?php

namespace commonprj\components\catalog\entities\component;

use commonprj\components\core\entities\element\ElementDBRepository;
use commonprj\components\core\entities\property\Property;
use commonprj\components\crm\entities\manufacturer\Manufacturer;
use Yii;
use yii\base\Component;
use yii\web\HttpException;

/**
 * Class ComponentDBRepository
 * @package commonprj\components\catalog\entities\component
 */
class ComponentDBRepository extends ElementDBRepository implements IComponentRepository
{
    /**
     * @param Component $component
     * @return array
     * @throws HttpException
     */
    public function getMaterials(Component $component): array
    {
        $propertyComponent2MaterialId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($component, Property::PROPERTY_SYSNAME__COMPONENT2MATERIAL);

        return $component->getChildren($propertyComponent2MaterialId);
    }

    /**
     * @param Component $component
     * @param int $materialId
     * @return bool
     * @throws HttpException
     */
    public function bindMaterial(Component $component, int $materialId): bool
    {
        $propertyComponent2MaterialId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($component, Property::PROPERTY_SYSNAME__COMPONENT2MATERIAL);

        return $component->bindChild($propertyComponent2MaterialId, $materialId);
    }

    /**
     * @param Component $component
     * @param int $materialId
     * @return bool
     * @throws HttpException
     */
    public function unbindMaterial(Component $component, int $materialId): bool
    {
        $propertyComponent2MaterialId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($component, Property::PROPERTY_SYSNAME__COMPONENT2MATERIAL);

        return $component->unbindChild($propertyComponent2MaterialId, $materialId);
    }

    /**
     * @param Component $component
     * @return \commonprj\components\core\entities\element\Element|\commonprj\components\core\entities\element\IElement|Manufacturer|null
     * @throws HttpException
     */
    public function getManufacturer(Component $component)
    {
        $relationId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($component, Property::PROPERTY_SYSNAME__COMPONENT2MANUFACTURER);
        return $this->getParent($this, $relationId);
    }

    /**
     * @param Component $component
     * @param int $manufacturerId
     * @return bool
     * @throws \commonprj\components\core\entities\element\Exception
     * @throws HttpException
     */
    public function bindManufacturer(Component $component, int $manufacturerId): bool
    {
        $relationId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($component, Property::PROPERTY_SYSNAME__COMPONENT2MANUFACTURER);
        return $component->bindToParent($relationId, $manufacturerId);
    }

    /**
     * @param Component $component
     * @param int $manufacturerId
     * @return bool
     * @throws HttpException
     */
    public function unbindManufacturer(Component $component, int $manufacturerId): bool
    {
        $relationId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($component, Property::PROPERTY_SYSNAME__COMPONENT2MANUFACTURER);
        return $component->unbindAllAssociatedEntities($relationId);
    }

    /**
     * @param Component $component
     * @return \commonprj\components\core\entities\element\Element|\commonprj\components\core\entities\element\IElement|Manufacturer|null
     * @throws HttpException
     */
    public function getProductMaterials(Component $component)
    {
        $relationId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($component, Property::PROPERTY_SYSNAME__PRODUCT_MATERIAL2COMPONENT);
        return $this->getChildren($this, $relationId);
    }

    /**
     * @param Component $component
     * @param int $productMaterialId
     * @return bool
     * @throws \commonprj\components\core\entities\element\Exception
     * @throws HttpException
     */
    public function bindProductMaterial(Component $component, int $productMaterialId): bool
    {
        $relationId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($component, Property::PROPERTY_SYSNAME__PRODUCT_MATERIAL2COMPONENT);
        return $component->bindChildren($relationId, $productMaterialId);
    }

    /**
     * @param Component $component
     * @param int $productMaterialId
     * @return bool
     * @throws HttpException
     */
    public function unbindProductMaterial(Component $component, int $productMaterialId): bool
    {
        $relationId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($component, Property::PROPERTY_SYSNAME__PRODUCT_MATERIAL2COMPONENT);
        return $component->unbindAllAssociatedEntities($relationId);
    }

}
