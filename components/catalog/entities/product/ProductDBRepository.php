<?php

namespace commonprj\components\catalog\entities\product;

use commonprj\components\catalog\entities\product\IProductRepository;
use commonprj\components\catalog\entities\product\Product;
use commonprj\components\catalog\entities\productMaterial\ProductMaterial;
use commonprj\components\catalog\entities\productModel\ProductModel;
use commonprj\components\core\entities\element\ElementDBRepository;
use commonprj\components\core\entities\property\Property;
use commonprj\components\crm\entities\manufacturer\Manufacturer;
use Yii;

class ProductDBRepository extends ElementDBRepository implements IProductRepository
{

    /**
     *
     * @param Product $product
     * @return ProductMaterial
     */
    public function getProductMaterials(Product $product): array
    {
        $propertyProduct2ProductMaterialId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($product, Property::PROPERTY_SYSNAME__PRODUCT2PRODUCT_MATERIAL);

        return $product->getChildren($propertyProduct2ProductMaterialId);
    }

    /**
     *
     * @param Product $product
     * @param int $productMaterialId
     * @return bool
     */
    public function bindProductMaterial(Product $product, int $productMaterialId): bool
    {
        $propertyProduct2ProductMaterialId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($product, Property::PROPERTY_SYSNAME__PRODUCT2PRODUCT_MATERIAL);

        return $product->bindAssociatedEntity($propertyProduct2ProductMaterialId, $productMaterialId);
    }

    /**
     *
     * @param Product $product
     * @param int $productMaterialId
     * @return bool
     */
    public function unbindProductMaterial(Product $product, int $productMaterialId): bool
    {
        $propertyProduct2ProductMaterialId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($product, Property::PROPERTY_SYSNAME__PRODUCT2PRODUCT_MATERIAL);

        return $product->unbindAssociatedEntity($propertyProduct2ProductMaterialId, $productMaterialId);
    }

    /**
     *
     * @param Product $product
     * @return ProductModel
     */
    public function getProductModel(Product $product): ProductModel
    {
        $propertyProduct2ProductModelId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($product, Property::PROPERTY_SYSNAME__PRODUCT2PRODUCT_MODEL);

        return $product->getAssociatedEntity($propertyProduct2ProductModelId);
    }

    /**
     *
     * @param Product $product
     * @param int $productModelId
     * @return bool
     */
    public function bindProductModel(Product $product, int $productModelId): bool
    {
        $propertyProduct2ProductModelId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($product, Property::PROPERTY_SYSNAME__PRODUCT2PRODUCT_MODEL);

        return $product->bindAssociatedEntity($propertyProduct2ProductModelId, $productModelId);
    }

    /**
     *
     * @param Product $product
     * @param int $productModelId
     * @return bool
     */
    public function unbindProductModel(Product $product, int $productModelId): bool
    {
        $propertyProduct2ProductModelId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($product, Property::PROPERTY_SYSNAME__PRODUCT2PRODUCT_MODEL);

        return $product->unbindAssociatedEntity($propertyProduct2ProductModelId, $productModelId);
    }
}