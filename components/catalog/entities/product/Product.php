<?php

namespace commonprj\components\catalog\entities\product;

use commonprj\components\catalog\entities\productMaterial\ProductMaterial;
use commonprj\components\catalog\entities\productModel\ProductModel;
use commonprj\components\catalog\entities\variation\Variant;
use commonprj\components\core\entities\element\Element;
use Yii;

class Product extends Element
{

    /**
     * Присвоение свойству доменного слоя $repository - соответствующего компонента
     * @inheritdoc
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->repository = Yii::$app->productRepository;
    }

    /**
     *
     * @return ProductMaterial
     */
    public function getProductMaterials(): array
    {
        return $this->repository->getProductMaterials($this);
    }

    /**
     *
     * @param int $productMaterialId
     * @return bool
     */
    public function bindProductMaterial(int $productMaterialId): bool
    {
        return $this->repository->bindProductMaterial($this, $productMaterialId);
    }

    /**
     *
     * @param int $productMaterialId
     * @return bool
     */
    public function unbindProductMaterial(int $productMaterialId): bool
    {
        return $this->repository->unbindProductMaterial($this, $productMaterialId);
    }

    /**
     *
     * @return ProductModel
     */
    public function getProductModel(): ProductModel
    {
        return $this->repository->getProductModel($this);
    }

    /**
     *
     * @param int $productModelId
     * @return bool
     */
    public function bindProductModel(int $productModelId): bool
    {
        return $this->repository->bindProductModel($this, $productModelId);
    }

    /**
     *
     * @param int $productModelId
     * @return bool
     */
    public function unbindProductModel(int $productModelId): bool
    {
        return $this->repository->unbindProductModel($this, $productModelId);
    }
}
