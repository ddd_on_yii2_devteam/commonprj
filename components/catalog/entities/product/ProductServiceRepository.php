<?php

namespace commonprj\components\catalog\entities\product;

use commonprj\components\catalog\entities\productModel\ProductModel;
use commonprj\components\crm\entities\manufacturer\Manufacturer;

class ProductServiceRepository implements IProductRepository
{

    /**
     *
     * @param Product $product
     * @param int $manufacturerId
     */
    public function bindManufacturer(Product $product, int $manufacturerId): bool
    {
        // TODO: Implement bindManufacturer() method.
    }

    /**
     *
     * @param Product $product
     * @param int $manufacturerId
     */
    public function unbindManufacturer(Product $product, int $manufacturerId): bool
    {
        // TODO: Implement unbindManufacturer() method.
    }

    /**
     *
     * @param Product $product
     */
    public function getManufacturer(Product $product): Manufacturer
    {
        // TODO: Implement getManufacturer() method.
    }

    /**
     *
     * @param Product $product
     * @param int $productMaterialId
     */
    public function bindProductMaterial(Product $product, int $productMaterialId): bool
    {
        // TODO: Implement bindProductMaterial() method.
    }

    /**
     *
     * @param Product $product
     * @param int $productMaterialId
     */
    public function unbindProductMaterial(Product $product, int $productMaterialId): bool
    {
        // TODO: Implement unbindProductMaterial() method.
    }

    /**
     *
     * @param Product $product
     */
    public function getProductMaterials(Product $product): array
    {
        // TODO: Implement getProductMaterials() method.
    }

    /**
     *
     * @param Product $product
     * @param int $productModelId
     */
    public function bindProductModel(Product $product, int $productModelId): bool
    {
        // TODO: Implement bindProductModel() method.
    }

    /**
     *
     * @param Product $product
     * @param int $productModelId
     */
    public function unbindProductModel(Product $product, int $productModelId): bool
    {
        // TODO: Implement unbindProductModel() method.
    }

    /**
     *
     * @param Product $product
     */
    public function getProductModel(Product $product): ProductModel
    {
        // TODO: Implement getProductModel() method.
    }

    /**
     *
     * @param Product $product
     * @param int $materialId
     */
    public function bindMaterial(Product $product, int $materialId): bool
    {
        // TODO: Implement bindMaterial() method.
    }

    /**
     *
     * @param Product $product
     * @param int $materialId
     */
    public function unbindMaterial(Product $product, int $materialId): bool
    {
        // TODO: Implement unbindMaterial() method.
    }

    /**
     *
     * @param Product $product
     */
    public function getMaterials(Product $product): array
    {
        // TODO: Implement getMaterials() method.
    }

    /**
     *
     * @param Product $product
     * @param int $constructionElementId
     */
    public function bindConstructionElement(Product $product, int $constructionElementId): bool
    {
        // TODO: Implement bindConstructionElement() method.
    }

    /**
     *
     * @param Product $product
     * @param int $constructionElementId
     */
    public function unbindConstructionElement(Product $product, int $constructionElementId): bool
    {
        // TODO: Implement unbindConstructionElement() method.
    }

    /**
     *
     * @param Product $product
     */
    public function getConstructionElements(Product $product): array
    {
        // TODO: Implement getConstructionElements() method.
    }

    /**
     *
     * @param Product $product
     * @param int $modularComponentId
     */
    public function bindModularComponent(Product $product, int $modularComponentId): bool
    {
        // TODO: Implement bindModularComponent() method.
    }

    /**
     *
     * @param Product $product
     * @param int $modularComponentId
     */
    public function unbindModularComponent(Product $product, int $modularComponentId): bool
    {
        // TODO: Implement unbindModularComponent() method.
    }

    /**
     *
     * @param Product $product
     */
    public function getModularComponents(Product $product): array
    {
        // TODO: Implement getModularComponents() method.
    }

}
