<?php

namespace commonprj\components\catalog\entities\product;

use commonprj\components\catalog\entities\component\Component;
use commonprj\components\catalog\entities\material\Material;
use commonprj\components\catalog\entities\productMaterial\ProductMaterial;
use commonprj\components\catalog\entities\productModel\ProductModel;
use commonprj\components\crm\entities\manufacturer\Manufacturer;

interface IProductRepository
{

    /**
     * @param Product $product
     * @return ProductMaterial[]
     */
    public function getProductMaterials(Product $product): array;

    /**
     * @param Product $product
     * @param int $productMaterialId
     * @return boolean
     */
    public function bindProductMaterial(Product $product, int $productMaterialId): bool;

    /**
     * @param Product $product
     * @param int $productMaterialId
     * @return boolean
     */
    public function unbindProductMaterial(Product $product, int $productMaterialId): bool;

    /**
     * @param Product $product
     * @return ProductModel
     */
    public function getProductModel(Product $product): ProductModel;

    /**
     * @param Product $product
     * @param int $productModelId
     * @return boolean
     */
    public function bindProductModel(Product $product, int $productModelId): bool;

    /**
     * @param Product $product
     * @param int $productModelId
     * @return boolean
     */
    public function unbindProductModel(Product $product, int $productModelId): bool;
}
