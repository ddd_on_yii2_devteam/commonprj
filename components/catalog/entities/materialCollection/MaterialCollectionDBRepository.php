<?php

namespace commonprj\components\catalog\entities\materialCollection;

use commonprj\components\core\entities\element\ElementDBRepository;
use commonprj\components\core\entities\property\Property;
use commonprj\components\crm\entities\manufacturer\Manufacturer;
use Yii;

class MaterialCollectionDBRepository extends ElementDBRepository implements IMaterialCollectionRepository
{

    /**
     *
     * @param MaterialCollection $materialCollection
     * @param int $manufacturerId
     * @return bool
     */
    public function bindManufacturer(MaterialCollection $materialCollection, int $manufacturerId): bool
    {
        $propertyMaterialCollection2ManufacturerId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($materialCollection, Property::PROPERTY_SYSNAME__MATERIAL_COLLECTION2MANUFACTURER);

        return $materialCollection->bindAssociatedEntity($propertyMaterialCollection2ManufacturerId, $manufacturerId);
    }

    /**
     *
     * @param MaterialCollection $materialCollection
     * @param int $manufacturerId
     * @return bool
     */
    public function unbindManufacturer(MaterialCollection $materialCollection, int $manufacturerId): bool
    {
        $propertyMaterialCollection2ManufacturerId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($materialCollection, Property::PROPERTY_SYSNAME__MATERIAL_COLLECTION2MANUFACTURER);

        return $materialCollection->unbindAssociatedEntity($propertyMaterialCollection2ManufacturerId, $manufacturerId);
    }

    /**
     *
     * @param MaterialCollection $materialCollection
     * @return Manufacturer
     */
    public function getManufacturer(MaterialCollection $materialCollection): Manufacturer
    {
        $propertyMaterialCollection2ManufacturerId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($materialCollection, Property::PROPERTY_SYSNAME__MATERIAL_COLLECTION2MANUFACTURER);

        $manufacturer = $materialCollection->getAssociatedEntity($propertyMaterialCollection2ManufacturerId);
        $manufacturer->properties = $manufacturer->getProperties();

        return $manufacturer;
    }
}
