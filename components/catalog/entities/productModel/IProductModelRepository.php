<?php
/**
 * @package         FurniPrice
 * @subpackage      Catalog
 * @category        Entity
 * @created         2018.01.22
 * @author          Vasiliy Konakov
 * @updated         2018.01.23
 * @updatedBy       Vasiliy Konakov
 */

namespace commonprj\components\catalog\entities\productModel;

use commonprj\components\catalog\entities\component\Component;
use commonprj\components\catalog\entities\material\Material;
use commonprj\components\catalog\entities\product\Product;
use commonprj\components\catalog\entities\productMaterial\ProductMaterial;
use commonprj\components\catalog\entities\variation\Variant;
use commonprj\components\crm\entities\manufacturer\Manufacturer;
use commonprj\components\crm\entities\seller\Seller;

/**
 * Class IProductModelRepository
 * @package commonprj\components\catalog\entities\productModel
 */
interface IProductModelRepository
{

    /**
     * bindManufacturer
     * @param ProductModel $element
     * @param int $manufacturerId
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|Exception
     */
    public function bindManufacturer(ProductModel $element, int $manufacturerId): bool;

    /**
     * unbindManufacturer
     * @param ProductModel $element
     * @param int $manufacturerId
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|Exception
     */
    public function unbindManufacturer(ProductModel $element, int $manufacturerId): bool;

    /**
     * getManufacturer
     * @param ProductModel $element
     * @return null|Manufacturer (domain layer object)
     * @throws HttpException|Exception
     */
    public function getManufacturer(ProductModel $element): ?Manufacturer;

    /**
     * getSellers
     * @param ProductModel $element
     * @return Seller[]
     * @throws HttpException|Exception
     */
    public function getSellers(ProductModel $element): array;

    /**
     * bindProductMaterial
     * @param ProductModel $element
     * @param int $productMaterialId
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|Exception
     */
    public function bindProductMaterial(ProductModel $element, int $productMaterialId): bool;

    /**
     * unbindProductMaterial
     * @param ProductModel $element
     * @param int $$productMaterialId
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|Exception
     */
    public function unbindProductMaterial(ProductModel $element, int $productMaterialId): bool;

    /**
     * getProductMaterials
     * @param ProductModel $element
     * @return ProductMaterial[]
     * @throws HttpException|Exception
     */
    public function getProductMaterials(ProductModel $element): array;

    /**
     * bindMaterial
     * @param ProductModel $element
     * @param int $materialId
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|Exception
     */
    public function bindMaterial(ProductModel $element, int $materialId): bool;

    /**
     * unbindMaterial
     * @param ProductModel $element
     * @param int $materialId
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|Exception
     */
    public function unbindMaterial(ProductModel $element, int $materialId): bool;

    /**
     * getMaterials
     * @param ProductModel $element
     * @return Material[]
     * @throws HttpException|Exception
     */
    public function getMaterials(ProductModel $element): array;

    /**
     * bindConstructionElement
     * @param ProductModel $element
     * @param int $constructionElementId
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|Exception
     */
    public function bindConstructionElement(ProductModel $element, int $constructionElementId): bool;

    /**
     * unbindConstructionElement
     * @param ProductModel $element
     * @param int $constructionElementId
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|Exception
     */
    public function unbindConstructionElement(ProductModel $element, int $constructionElementId): bool;

    /**
     * getConstructionElements
     * @param ProductModel $element
     * @return Component[]
     * @throws HttpException|Exception
     */
    public function getConstructionElements(ProductModel $element): array;

    /**
     * bindModularComponent
     * @param ProductModel $element
     * @param int $modularComponentId
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|Exception
     */
    public function bindModularComponent(ProductModel $element, int $modularComponentId): bool;

    /**
     * unbindModularComponent
     * @param ProductModel $element
     * @param int $modularComponentId
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|Exception
     */
    public function unbindModularComponent(ProductModel $element, int $modularComponentId): bool;

    /**
     * getModularComponents
     * @param ProductModel $element
     * @return Component[]
     * @throws HttpException|Exception
     */
    public function getModularComponents(ProductModel $element): array;

    /**
     * getProducts
     * @param ProductModel $element
     * @return Product[]
     * @throws HttpException|Exception
     */
    public function getProducts(ProductModel $element): array;
}
