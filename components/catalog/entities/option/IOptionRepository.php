<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 22.03.2018
 * Time: 13:03
 */

namespace commonprj\components\catalog\entities\option;

use commonprj\components\catalog\entities\variant\Variant;
use commonprj\components\core\entities\element\IElementRepository;
use commonprj\components\core\entities\property\Property;

/**
 * Interface IOptionRepository
 * @package commonprj\components\catalog\entities\option
 */
interface IOptionRepository extends IElementRepository
{
    /**
     * @param Option $element
     * @return Property
     */
    public function getProperty(Option $element): ?Property;

    /**
     * @param Option $element
     * @return Variant[]
     */
    public function getVariants(Option $element): array;

}