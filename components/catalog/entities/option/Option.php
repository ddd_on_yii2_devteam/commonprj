<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 22.03.2018
 * Time: 12:55
 */

namespace commonprj\components\catalog\entities\option;

use commonprj\components\catalog\entities\materialGroup\MaterialGroup;
use commonprj\components\core\entities\element\Element;
use commonprj\components\core\entities\property\Property;
use Yii;

/**
 * Class Option
 * @property OptionDBRepository $repository
 * @package commonprj\components\catalog\entities\option
 */
class Option extends Element
{
    /**
     * @var int
     */
    public $propertyId;

    /**
     * @var MaterialGroup
     */
    public $materialGroup;

    /**
     * @var Property
     */
    public $property;

    /**
     * @var array
     */
    public $typeOfOption;

    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->repository = Yii::$app->optionRepository;
    }

    /**
     * @return Property
     * @throws \yii\web\NotFoundHttpException
     */
    public function getProperty(): ?Property
    {
        return $this->repository->getProperty($this);
    }

    /**
     * @return array
     * @throws \yii\web\HttpException
     */
    public function getVariants(): array
    {
        return $this->repository->getVariants($this);
    }

    /**
     * @param int $propertyId
     * @return bool
     * @throws \commonprj\components\core\entities\element\Exception
     * @throws \yii\web\HttpException
     */
    public function bindProperty(int $propertyId): bool
    {
        return $this->repository->bindProperty($this, $propertyId);
    }

    /**
     * @param int $propertyId
     * @return bool
     * @throws \commonprj\components\core\entities\element\Exception
     * @throws \yii\web\HttpException
     */
    public function unbindProperty(int $propertyId): bool
    {
        return $this->repository->unbindProperty($this, $propertyId);
    }
}