<?php

namespace commonprj\components\catalog\entities\Option;

use commonprj\components\catalog\entities\variant\Variant;
use commonprj\components\core\entities\element\ElementServiceRepository;
use commonprj\components\core\entities\property\Property;
use Yii;
use yii\httpclient\Client;

/**
 * Class OptionServiceRepository
 * @package commonprj\components\catalog\entities\Option
 */
class OptionServiceRepository extends ElementServiceRepository
{
    /**
     * @var Client
     */
    protected $restServer;

    /**
     * @var string
     */
    protected $baseUri = 'option/';

    /**
     * OptionServiceRepository constructor.
     */
    public function __construct()
    {
        $this->restServer = Yii::$app->catalogManager;
    }

    /**
     * @param Option $option
     * @return \commonprj\extendedStdComponents\BaseCrudModel|null
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    public function getProperty(Option $option)
    {
        $this->requestUri = $this->getBaseUri($option->id . '/property');
        $this->requestParams = [];
        $content = $this->getApiData();

        if (!empty($content)) {
            $result = $this->getOneModel($content, Property::className());
        }

        return $result ?? null;
    }

    /**
     * @param Option $option
     * @return array|\commonprj\extendedStdComponents\BaseCrudModel[]|null
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    public function getVariants(Option $option)
    {
        $this->requestUri = $this->getBaseUri($option->id . '/variants');
        $this->requestParams = [];
        $content = $this->getApiData();

        if (!empty($content)) {
            $result = $this->getArrayOfModels($content, Variant::className());
        }

        return $result ?? null;
    }

    /**
     * @param $entity
     * @return array
     * @throws \ReflectionException
     */
    public function attributes($entity)
    {
        return array_merge(parent::attributes($entity), ['property', 'typeOfOption', 'propertyId', 'materialGroupId']);
    }
}