<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 28.02.2018
 * Time: 14:10
 */

namespace commonprj\components\catalog\entities\materialGroup;

use commonprj\components\catalog\entities\material\Material;
use commonprj\extendedStdComponents\BaseServiceRepository;
use Yii;
use yii\httpclient\Client;

/**
 * Class MaterialGroupServiceRepository
 * @package commonprj\components\catalog\entities\materialGroup
 */
class MaterialGroupServiceRepository extends BaseServiceRepository
{
    /**
     * @var Client
     */
    protected $restServer;

    /**
     * @var string
     */
    protected $baseUri = 'material-group/';

    /**
     * PropertyServiceRepository constructor.
     */
    public function __construct()
    {
        $this->restServer = Yii::$app->catalogManager;
    }

    /**
     * @param MaterialGroup $materialGroup
     * @return array
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    public function getMaterials(MaterialGroup $materialGroup): array
    {
        $this->requestUri = $this->getBaseUri($materialGroup->id . '/materials');
        $result = $this->getApiData();
        return $result ?? [];
    }
}