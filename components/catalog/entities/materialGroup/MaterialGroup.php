<?php
/**
 * @package         FurniPrice
 * @subpackage      Catalog
 * @category        Entity
 * @created         2018.01.22
 * @author          Vasiliy Konakov
 * @updated         2018.01.23
 * @updatedBy       Vasiliy Konakov
 */

namespace commonprj\components\catalog\entities\materialGroup;

use Yii;
use yii\db\Exception;
use yii\web\HttpException;
use commonprj\components\core\entities\element\Element;
use commonprj\components\catalog\entities\material\Material;

/**
 * Class MaterialGroup
 * @package commonprj\components\catalog\entities\materialGroup
 */
class MaterialGroup extends Element
{
    public $materials = [];
    /**
     * Присвоение свойству доменного слоя $repository - соответствующего компонента
     * @inheritdoc
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->repository = Yii::$app->materialGroupRepository;
    }

    /**
     * getMaterials
     * @return Material[]
     * @throws HttpException|Exception
     */
    public function getMaterials(): array
    {
        return $this->repository->getMaterials($this);
    }

}
