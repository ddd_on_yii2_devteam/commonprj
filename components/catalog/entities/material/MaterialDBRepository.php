<?php
/**
 * @package         FurniPrice
 * @subpackage      Catalog
 * @category        Entity
 * @created         2018.01.22
 * @author          Vasiliy Konakov
 * @updated         2018.01.23
 * @updatedBy       Vasiliy Konakov
 */

namespace commonprj\components\catalog\entities\material;

use Yii;
use yii\db\Exception;
use yii\web\HttpException;
use commonprj\components\core\models\ElementRecord;
use commonprj\components\core\entities\property\Property;
use commonprj\components\core\entities\element\Element;
use commonprj\components\core\entities\element\ElementDBRepository;
use commonprj\components\catalog\entities\component\Component;
use commonprj\components\catalog\entities\materialCollection\MaterialCollection;
use commonprj\components\catalog\entities\materialGroup\MaterialGroup;
use commonprj\components\catalog\entities\materialTemplate\MaterialTemplate;
use commonprj\components\catalog\entities\productModel\ProductModel;

class MaterialDBRepository extends ElementDBRepository implements IMaterialRepository
{

    /**
     * bindMaterialGroup
     * @param Material $element
     * @param int $materialGroupId
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|Exception
     */
    public function bindMaterialGroup(Material $element, int $materialGroupId): bool
    {
        return $this->bindAssociatedEntity($element, Yii::$app->propertyService->getPropertyIdByElementAndSysname($element,
            Property::PROPERTY_SYSNAME__MATERIAL2MATERIAL_GROUP), $materialGroupId);
    }

    /**
     * unbindMaterialGroup
     * @param Material $element
     * @param int $materialGroupId
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|Exception
     */
    public function unbindMaterialGroup(Material $element, int $materialGroupId): bool
    {
        return $this->unbindAssociatedEntity($element, Yii::$app->propertyService->getPropertyIdByElementAndSysname($element,
            Property::PROPERTY_SYSNAME__MATERIAL2MATERIAL_GROUP), $materialGroupId);
    }

    /**
     * getMaterialGroups
     * @param Material $element
     * @return array (domain layer object)
     * @throws HttpException|Exception
     */
    public function getMaterialGroups(Material $element): array
    {
        $result = [];
        foreach ($this->getAssociatedEntities($element,
            Yii::$app->propertyService->getPropertyIdByElementAndSysname($element,
                Property::PROPERTY_SYSNAME__MATERIAL2MATERIAL_GROUP)) as $item) {
            $result[$item->id] = MaterialGroup::findOne($item->id);
        }
        return $result;
    }

    /**
     * bindMaterialTemplate
     * @param Material $element
     * @param int $materialTemplateId
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|Exception
     */
    public function bindMaterialTemplate(Material $element, int $materialTemplateId): bool
    {
        return $this->bindAssociatedEntity($element, Yii::$app->propertyService->getPropertyIdByElementAndSysname($element,
            Property::PROPERTY_SYSNAME__MATERIAL2MATERIAL_TEMPLATE), $materialTemplateId);
    }

    /**
     * unbindMaterialTemplate
     * @param Material $element
     * @param int $materialTemplateId
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|Exception
     */
    public function unbindMaterialTemplate(Material $element, int $materialTemplateId): bool
    {
        return $this->unbindAssociatedEntity($element, Yii::$app->propertyService->getPropertyIdByElementAndSysname($element,
            Property::PROPERTY_SYSNAME__MATERIAL2MATERIAL_TEMPLATE), $materialTemplateId);
    }

    /**
     * getMaterialTemplate
     * @param Material $element
     * @return null|Element (domain layer object)
     * @throws HttpException|Exception
     */
    public function getMaterialTemplate(Material $element): ?MaterialTemplate
    {
        $item = $this->getAssociatedEntity($element, Yii::$app->propertyService->getPropertyIdByElementAndSysname($element,
            Property::PROPERTY_SYSNAME__MATERIAL2MATERIAL_TEMPLATE));
        if ($item) {
            /** @var MaterialTemplate $result */
            $result = MaterialTemplate::findOne($item->id);
            return $result;
        }
        return null;
    }

    /**
     * getComponents
     * @param Material $element
     * @return Component[]
     * @throws HttpException|Exception
     */
    public function getComponents(Material $element): array
    {
        $result = [];
        foreach ($this->getParents($element, Yii::$app->propertyService->getPropertyIdByElementAndSysname($element,
            Property::PROPERTY_SYSNAME__MATERIAL2COMPONENT)) as $item) {
            $result[$item->id] = Component::findOne($item->id);
        }
        return $result;
    }

    /**
     * getProductModels
     * @param Material $element
     * @return ProductModel[]
     * @throws HttpException|Exception
     */
    public function getProductModels(Material $element): array
    {
        $result = [];
        foreach ($this->getParents($element, Yii::$app->propertyService->getPropertyIdByElementAndSysname($element,
            Property::PROPERTY_SYSNAME__MATERIAL2PRODUCT_MODEL)) as $item) {
            $result[$item->id] = ProductModel::findOne($item->id);
        }
        return $result;
    }

    /**
     * bindCompositionChild
     * @param Material $element
     * @param int $compositionMaterialId
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|Exception
     */
    public function bindCompositionChild(Material $element, $compositionChildId): bool
    {
        return $this->bindToParent($element, Yii::$app->propertyService->getPropertyIdByElementAndSysname($element,
            Property::PROPERTY_SYSNAME__MATERIAL2COMPOSITION), $compositionChildId);
    }

    /**
     * unbindCompositionChild
     * @param Material $element
     * @param int $compositionMaterialId
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|Exception
     */
    public function unbindCompositionChild(Material $element, $compositionChildId): bool
    {
        return $this->unbindChild($element, Yii::$app->propertyService->getPropertyIdByElementAndSysname($element,
            Property::PROPERTY_SYSNAME__MATERIAL2COMPOSITION), $compositionChildId);
    }

    /**
     * getCompositionMaterials
     * @param Material $element
     * @return Component[]
     * @throws HttpException|Exception
     */
    public function getCompositionChildren(Material $element): array
    {
        $result = [];
        foreach ($this->getChildren($element, Yii::$app->propertyService->getPropertyIdByElementAndSysname($element,
            Property::PROPERTY_SYSNAME__MATERIAL2COMPOSITION)) as $item) {
            $result[$item->id] = self::instantiateByARAndClassName(
                ElementRecord::findOne(['id' => $item->id]), Material::class);
        }
        return $result;
    }

    /**
     * @param Material $element
     * @return Material[]
     */
    public function getCompositionParents(Material $element): array
    {
        $propertyId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($element,
            Property::PROPERTY_SYSNAME__MATERIAL_COMPOSITION);

        return $this->getParents($element, $propertyId);
    }

    /**
     * @param Material $element
     * @param int $childMaterialId
     * @return bool
     */
    public function bindHierarchyChild(Material $element, int $childMaterialId): bool
    {
        $propertyId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($element,
            Property::PROPERTY_SYSNAME__MATERIAL_HIERARCHY);

        return $this->bindChild($element, $propertyId, $childMaterialId);
    }

    /**
     * @param Material $element
     * @param int $childMaterialId
     * @return bool
     */
    public function unbindHierarchyChild(Material $element, int $childMaterialId): bool
    {
        $propertyId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($element,
            Property::PROPERTY_SYSNAME__MATERIAL_HIERARCHY);

        return $this->unbindChild($element, $propertyId, $childMaterialId);
    }

    /**
     * @param Material $element
     * @return Material[]
     */
    public function getHierarchyChildren(Material $element): array
    {
        $propertyId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($element,
            Property::PROPERTY_SYSNAME__MATERIAL_HIERARCHY);

        return $this->getChildren($element, $propertyId);
    }

    /**
     * @param Material $element
     * @return null|Material
     * @throws HttpException
     */
    public function getHierarchyParent(Material $element): ?Material
    {
        $propertyId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($element,
            Property::PROPERTY_SYSNAME__MATERIAL_HIERARCHY);

        return $this->getParent($element, $propertyId);
    }

    /**
     * @param Material $element
     * @param bool $isReturnFullObjects
     * @param bool $isReturnLinearArray
     * @return array
     * @throws Exception
     * @throws HttpException
     */
    public function getHierarchyDescendants(Material $element, $isReturnFullObjects = false, $isReturnLinearArray = false)
    {
        $propertyId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($element,
            Property::PROPERTY_SYSNAME__MATERIAL_HIERARCHY);

        return $this->getDescendants($element, $propertyId, $isReturnFullObjects, $isReturnLinearArray, 'hierarchyChildren');
    }

    /**
     * @param Material $element
     * @param bool $isReturnFullObjects
     * @param bool $isReturnLinearArray
     * @return mixed
     * @throws Exception
     * @throws HttpException
     */
    public function getFullListInHierarchy(Material $element, $isReturnFullObjects = true, $isReturnLinearArray = false)
    {
        $propertyId = Yii::$app->propertyService->getIdBySysname(Property::PROPERTY_SYSNAME__MATERIAL_HIERARCHY);

        return $this->getDescendants($element, $propertyId, $isReturnFullObjects, $isReturnLinearArray, 'hierarchyChildren', true);
    }


    public function getMaterialTemplateInHierarchy(Material $element)
    {
        $parents = $this->getAllHierarchyParents($element);

        $parents = array_merge($parents, [$element]);
        $properties = [];
        foreach ($parents as $parent) {
            /**
             * @var MaterialTemplate $materialTemplate
             */
            $materialTemplate = $parent->getMaterialTemplate();
            if ($materialTemplate) {
                $materialTemplate->repository->populateElementProperties($materialTemplate);

                if (!is_null($parent->getMaterialTemplate())) {

                    foreach ($materialTemplate->properties as $property) {
                        $properties[$property->id] = $property;
                    }
                }

            }
        }

        $hierarchyTemplate = new MaterialTemplate();
        $hierarchyTemplate->load(['properties' => array_values($properties)], '');
        $hierarchyTemplate->repository->populateElementProperties($hierarchyTemplate);

        return $hierarchyTemplate;
    }

    /**
     * @param Material $element
     * @return Material[]
     * @throws HttpException
     */
    public function getAllHierarchyParents(Material $element)
    {
        $parents = [];

        $continue = true;
        while ($continue) {
            $parent = $this->getHierarchyParent($element);

            if ($parent) {
                $parents[] = $parent;
                $element = $parent;
            } else {
                $continue = false;
            }
        }

        return $parents;
    }

    public function attributes()
    {
        $values = parent::attributes();

        return array_merge($values, [
            'hierarchyChildren',
        ]);
    }

}