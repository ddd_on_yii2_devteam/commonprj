<?php
/**
 * @package         FurniPrice
 * @subpackage      Catalog
 * @category        Entity
 * @created         2018.01.22
 * @author          Vasiliy Konakov
 * @updated         2018.01.23
 * @updatedBy       Vasiliy Konakov
 */

namespace commonprj\components\catalog\entities\material;

use yii\db\Exception;
use yii\web\HttpException;
use commonprj\components\core\entities\element\Element;
use commonprj\components\catalog\entities\component\Component;
use commonprj\components\catalog\entities\materialCollection\MaterialCollection;
use commonprj\components\catalog\entities\materialGroup\MaterialGroup;
use commonprj\components\catalog\entities\materialTemplate\MaterialTemplate;
use commonprj\components\catalog\entities\productModel\ProductModel;

/**
 * Class IMaterialRepository
 * @package commonprj\components\catalog\entities\material
 */
interface IMaterialRepository
{

    /**
     * bindMaterialGroup
     * @param Material $element
     * @param int $materialGroupId
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|Exception
     */
    public function bindMaterialGroup(Material $element, int $materialGroupId): bool;

    /**
     * unbindMaterialGroup
     * @param Material $element
     * @param int $materialGroupId
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|Exception
     */
    public function unbindMaterialGroup(Material $element, int $materialGroupId): bool;

    /**
     * getMaterialGroups
     * @param Material $element
     * @return array (domain layer object)
     * @throws HttpException|Exception
     */
    public function getMaterialGroups(Material $element): array;

    /**
     * bindMaterialTemplate
     * @param Material $element
     * @param int $materialTemplateId
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|Exception
     */
    public function bindMaterialTemplate(Material $element, int $materialTemplateId): bool;

    /**
     * unbindMaterialTemplate
     * @param Material $element
     * @param int $materialTemplateId
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|Exception
     */
    public function unbindMaterialTemplate(Material $element, int $materialTemplateId): bool;

    /**
     * getMaterialTemplate
     * @param Material $element
     * @return null|Element (domain layer object)
     * @throws HttpException|Exception
     */
    public function getMaterialTemplate(Material $element): ?MaterialTemplate;

    /**
     * getComponents
     * @param Material $element
     * @return Component[]
     * @throws HttpException|Exception
     */
    public function getComponents(Material $element): array;

    /**
     * getProductModels
     * @param Material $element
     * @return ProductModel[]
     * @throws HttpException|Exception
     */
    public function getProductModels(Material $element): array;

    /**
     * bindCompositionChild
     * @param Material $element
     * @param int $compositionChildId
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|Exception
     */
    public function bindCompositionChild(Material $element, $compositionChildId): bool;

    /**
     * unbindCompositionChild
     * @param Material $element
     * @param int $compositionChildId
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|Exception
     */
    public function unbindCompositionChild(Material $element, $compositionChildId): bool;

    /**
     * getCompositionChildren
     * @param Material $element
     * @return Component[]
     * @throws HttpException|Exception
     */
    public function getCompositionChildren(Material $element): array;

    /**
     * @param Material $element
     * @return Material[]
     */
    public function getCompositionParents(Material $element): array;

    /**
     * @param Material $element
     * @param int $childChildId
     * @return bool
     */
    public function bindHierarchyChild(Material $element, int $childChildId): bool;

    /**
     * @param Material $element
     * @param int $childChildId
     * @return bool
     */
    public function unbindHierarchyChild(Material $element, int $childChildId): bool;

    /**
     * @param Material $element
     * @return Material[]
     */
    public function getHierarchyChildren(Material $element): array;

    /**
     * @param Material $element
     * @return null|Material
     */
    public function getHierarchyParent(Material $element): ?Material;

    /**
     * @param Material $element
     * @param bool $isReturnFullObjects
     * @param bool $isReturnLinearArray
     * @return Material[]
     */
    public function getHierarchyDescendants(Material $element, $isReturnFullObjects = false, $isReturnLinearArray = false);

    /**
     * @param Material $element
     * @param bool $isReturnFullObjects
     * @param bool $isReturnLinearArray
     * @return mixed
     */
    public function getFullListInHierarchy(Material $element, $isReturnFullObjects = false, $isReturnLinearArray = false);


}