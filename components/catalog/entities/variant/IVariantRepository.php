<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 02.02.2018
 * Time: 13:19
 */

namespace commonprj\components\catalog\entities\variant;

use commonprj\components\catalog\entities\option\Option;
use commonprj\components\core\entities\element\IElementRepository;

/**
 * Interface IVariantRepository
 * @package commonprj\components\catalog\entities\variant
 */
interface IVariantRepository extends IElementRepository
{
    /**
     * @param Variant $element
     * @param int $optionId
     * @return bool
     */
    public function bindOption(Variant $element, int $optionId): bool;

    /**
     * @param Variant $element
     * @param int $optionId
     * @return bool
     */
    public function unbindOption(Variant $element, int $optionId): bool;

    /**
     * @param Variant $element
     * @return Option|null
     */
    public function getOption(Variant $element): ?Option;
}