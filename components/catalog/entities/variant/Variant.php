<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 02.02.2018
 * Time: 13:04
 */

namespace commonprj\components\catalog\entities\variant;

use commonprj\components\catalog\entities\material\Material;
use commonprj\components\catalog\entities\option\Option;
use commonprj\components\catalog\entities\priceCategory\PriceCategory;
use commonprj\components\catalog\entities\productMaterial\ProductMaterial;
use commonprj\components\core\entities\element\Element;
use commonprj\components\core\entities\property\Property;
use Yii;

/**
 * Class Variation
 * @property VariantDBRepository $repository
 * @package commonprj\components\core\entities\variation
 */
class Variant extends Element
{
    /**
     * @var PriceCategory|null
     */
    public $priceCategory;

    /**
     * @var ProductMaterial[]
     */
    public $productMaterials;

    /**
     * @var Material[]
     */
    public $materials;

    public $propertyValue;

    /**
     * Variation constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->repository = Yii::$app->variantRepository;
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function __get($name)
    {
        foreach ($this->properties as $property) {
            if (isset($property['sysname']) && $property['sysname'] == $name) {
                return  $property['value'];
            }
        }

        return null;
    }

    /**
     * @param int $optionId
     * @return bool
     * @throws \yii\db\Exception
     * @throws \yii\web\HttpException
     */
    public function bindOption(int $optionId): bool
    {
        return $this->repository->bindOption($this, $optionId);
    }

    /**
     * @param int $optionId
     * @return bool
     * @throws \yii\db\Exception
     * @throws \yii\web\HttpException
     */
    public function unbindOption(int $optionId): bool
    {
        return $this->repository->unbindOption($this, $optionId);
    }

    /**
     * @return Option|null
     * @throws \yii\web\HttpException
     */
    public function getOption(): ?Option
    {
        return $this->repository->getOption($this);
    }
}