<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace commonprj\extendedStdComponents;

use Yii;
use yii\rest\ActiveController;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

/**
 * Class CelecomActiveController
 * Переопределение статичных вызовов к ActiveRecord в ActiveController в динамичные к доменному слою.
 * @package commonprj\extendedStdComponents
 */
class BaseActiveController extends ActiveController
{
    /**
     * @var string|array the configuration for creating the serializer that formats the response data.
     */
    public $serializer = Serializer::class;

    public $serializerParams = [];

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'index'   => [
                'class'       => 'common\extendedStdComponents\IndexAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'view'    => [
                'class'       => 'common\extendedStdComponents\ViewAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'create'  => [
                'class'       => 'common\extendedStdComponents\CreateAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'scenario'    => $this->createScenario,
            ],
            'update'  => [
                'class'       => 'common\extendedStdComponents\UpdateAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'scenario'    => $this->updateScenario,
            ],
            'delete'  => [
                'class'       => 'common\extendedStdComponents\DeleteAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'options' => [
                'class' => 'common\extendedStdComponents\OptionsAction',
            ],

            'getTranslationData'            => [
                'class'       => 'common\extendedStdComponents\GetTranslationDataAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
    }

    /**
     * Checks the privilege of the current user.
     *
     * This method should be overridden to check whether the current user has the privilege
     * to run the specified action against the specified data model.
     * If the user does not have access, a [[ForbiddenHttpException]] should be thrown.
     *
     * @param string $action the ID of the action to be executed
     * @param object $model the model to be accessed. If null, it means no specific model is being accessed.
     * @param array $params additional parameters
     * @throws ForbiddenHttpException if the user does not have access
     */
    public function checkAccess($action, $model = null, $params = [])
    {
    }

    public function verbs()
    {
        return ['index' => ['GET']];
    }

    /**
     * @param array $params
     */
    public function setSerializerParams($params = [])
    {
        $this->serializerParams = $params;
    }

    /**
     * Serializes the specified data.
     * The default implementation will create a serializer based on the configuration given by [[serializer]].
     * It then uses the serializer to serialize the given data.
     * @param mixed $data the data to be serialized
     * @return mixed the serialized data.
     */
    protected function serializeData($data)
    {
        $serializer = Yii::createObject($this->serializer);
        Yii::configure($serializer, $this->serializerParams);

        return $serializer->serialize($data);
    }

    /**
     * Runs an action within this controller with the specified action ID and parameters.
     * If the action ID is empty, the method will use [[defaultAction]].
     * @param string $id the ID of the action to be executed.
     * @param array $params the parameters (name-value pairs) to be passed to the action.
     * @return mixed the result of the action.
     * @throws InvalidRouteException if the requested action ID cannot be resolved into an action successfully.
     * @see createAction()
     */
    public function runAction($id, $params = [])
    {
        $request = Yii::$app->getRequest();
        $result = parent::runAction($id, $params);

        if ($request->getQueryParam('debug')){
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_HTML;
            ob_start();
            dump($result);
            $res = ob_get_clean();
            return $this->renderContent($res);
        } else {
            return $result;
        }

    }


}
