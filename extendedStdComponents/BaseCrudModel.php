<?php
/**
 * Created by daSilva.Rodrigues
 * Date: 13.07.2016
 */

namespace commonprj\extendedStdComponents;

use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\db\ActiveQueryInterface;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;
use yii\web\ServerErrorHttpException;
use Yii;

/**
 * Class BaseCrudModel
 * Родительский базовый класс для доменного слоя.
 * @package commonprj\components\core\entities
 */
abstract class BaseCrudModel extends Model implements IBaseCrudModel
{

    /**
     * @var BaseDBRepository
     */
    public $repository;

    public $entity;

    /**
     * @var array|null old attribute values indexed by attribute names.
     * This is `null` if the record [[isNewRecord|is new]].
     */
    protected $_oldAttributes;

    protected $_relation;

    /**
     * Удаляет строки из таблиц.
     * Ожидается что перед вызовом метода начата транзакция.
     * В случае не удачного удаления будет rollBack транзакции.
     * @param ActiveRecord[] $rows - Массив ActiveRecord строк для удаления.
     * @throws HttpException - При неудаче возвращает ServerErrorHttpException и делает rollBack.
     */
    public static function deleteRows(array $rows)
    {
        /** @var ActiveRecord $row */
        foreach ($rows as $row) {

            if (!$row->delete()) {
                // todo после прикрутки системы логирования не возвращать getErrors, а писать его в лог.
                throw new ServerErrorHttpException(basename(__FILE__,
                        '.php') . __LINE__ . ' ' . print_r($row->getErrors(),
                        true));
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function fields()
    {
        $fields = parent::fields();
        unset($fields['repository']);

        return $fields;
    }

    /**
     * Возвращает имя класса без неймспейса.
     * @return string
     */
    public function getThisShortClassName()
    {
        preg_match('/\w+$/', get_called_class(), $match);

        return $match[0];
    }

    /**
     * Удаляет запись текущего инстанса вместе со всеми зависимостями.
     */
    public function delete(): bool
    {
        $this->repository->delete($this);

        return true;
    }

    /**
     * @return bool
     */
    public function update(): bool
    {
        return $this->save();
    }

    /**
     * @return bool
     */
    public function save(): bool
    {
         return $this->repository->save($this);
    }


    /**
     * @param null $key
     * @return array|mixed|null
     */
    public function getOldAttributes($key = null)
    {
        if ($key) {
            return $this->_oldAttributes[$key] ?? null;
        } else {
            return $this->_oldAttributes ?? [];
        }

    }

    /**
     * @return array
     */
    public function attributes()
    {
        return $this->repository->attributes($this);
    }

    /**
     * @param null $names
     * @param array $except
     * @return array
     */
    public function getAttributes($names = null, $except = [])
    {
        return $this->repository->getAttributes($this, $names, $except);
    }

    /**
     * @inheritdoc
     * @return EntityQuery
     */
    public static function find()
    {
        $entity = new static();
        return $entity->repository->find($entity);
    }

    /**
     * @inheritdoc
     * @return static|null BaseCrudModel instance matching the condition, or `null` if nothing matches.
     */
    public static function findOne($condition)
    {
        $entity = new static();
        return $entity->repository->findOne($entity, $condition);
    }

    /**
     * @inheritdoc
     * @return static[] an array of BaseCrudModel instances, or an empty array if nothing matches.
     */
    public static function findAll($condition)
    {
        $entity = new static();
        return $entity->repository->findAll($entity, $condition);
    }

    /**
     * создает инстанс сущности
     * @param int $entityId
     * @return BaseCrudModel
     */
    public static function instantiate(int $entityId = null)
    {
        return new static();
    }

    /**
     * передает заполение можели в репозиторий
     * @param BaseCrudModel $entity
     * @param int|array $entitiesId
     * @return void
     */
    public static function populateRecord(BaseCrudModel $entity, $entitiesId)
    {
        $entity->repository->populateRecord($entity, $entitiesId);
    }

    /**
     * переводит модель
     * @param BaseCrudModel $entity
     * @return void
     */
    public static function translateRecord(BaseCrudModel $entity)
    {
        $entity->repository->translateRecord($entity);
    }

    /**
     * @param $name
     * @param $value
     */
    public function setOldAttribute($name, $value)
    {
        $this->_oldAttributes[$name] = $value;
    }

    /**
     * @return array
     */
    public static function primaryKey()
    {
        return ['id'];
    }

    /**
     * @param $key
     * @return array
     */
    public function getRelationData($key)
    {
        return $this->_relation[$key] ?? [];
    }

    /*
    * @param array|null $condition
    * @return mixed
    */
    /**
     * @param array|null $condition
     * @return int|string
     */
    public function count(array $condition = null)
    {
        return $this->repository->count($condition);
    }

    /**
     * @return bool
     */
    public function isNewRecord()
    {
        return $this->_oldAttributes === null;
    }

    /**
     * @param array $values
     * @param bool $safeOnly
     */
    public function setAttributes($values, $safeOnly = true)
    {
        return $this->repository->setAttributes($this, $values, $safeOnly);
    }

    /**
     * Returns a value indicating whether the model has an attribute with the specified name.
     * @param string $name the name of the attribute
     * @return bool whether the model has an attribute with the specified name.
     */
    public function hasAttribute($name)
    {
        return in_array($name, $this->attributes());
    }


    /**
     * Возвращает переводы переводимых полей
     * @return mixed
     */
    public function getTranslations()
    {
        return $this->repository->getTranslations($this);
    }
}
