<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace commonprj\extendedStdComponents;

use DateTime;
use Yii;
use yii\db\ActiveRecordInterface;
use yii\rest\Action;
use yii\web\NotFoundHttpException;

/**
 * Class PlanironAction
 * @package commonprj\extendedStdComponents
 */
class BaseAction extends Action
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->findModel = [$this, 'findModel'];
        Parent::init();
    }

    /**
     * Returns the data model based on the primary key given.
     * If the data model is not found, a 404 HTTP exception will be raised.
     * @param string $id the ID of the model to be loaded. If the model has a composite primary key,
     * the ID must be a string of the primary key values separated by commas.
     * The order of the primary key values should follow that returned by the `primaryKey()` method
     * of the model.
     * @return BaseCrudModel
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function findModel($id)
    {

        /* @var $modelClass ActiveRecordInterface */
        $modelClass = $this->modelClass;
        $keys = $modelClass::primaryKey();
        if (count($keys) > 1) {
            $values = explode(',', $id);
            if (count($keys) === count($values)) {
                $model = $modelClass::findOne(array_combine($keys, $values));
            }
        } elseif ($id !== null) {
            $model = $modelClass::findOne($id);
        }

        if (isset($model)) {
            return $model;
        }

//        throw new NotFoundHttpException("Object not found: $id");
        throw new NotFoundHttpException(basename(__FILE__, '.php') . __LINE__);
    }

    /**
     * @param        $date
     * @param string $format
     * @return bool
     */
    public function validateDate($date, $format = 'd.m.Y')
    {
        $dateTime = DateTime::createFromFormat($format, $date);

        return $dateTime && $dateTime->format($format) == $date;
    }
}
