<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 3/5/2018
 * Time: 3:57 PM
 */

namespace commonprj\extendedStdComponents;


interface IBaseRepository
{
    /**
     * @param IBaseCrudModel $entity
     * @return bool
     */
    public function update(IBaseCrudModel $entity): bool;

    /**
     * @param IBaseCrudModel $entity
     * @return bool
     */
    public function save(IBaseCrudModel $entity, $runValidation = true, $attributes = null): bool;

    /**
     * @param IBaseCrudModel $entity
     * @return bool
     */
    public function delete(IBaseCrudModel $entity): bool;

    /**
     * @inheritdoc
     * @return static|null BaseCrudModel instance matching the condition, or `null` if nothing matches.
     */
    public function findOne(IBaseCrudModel $entity, $condition);

    /**
     * @inheritdoc
     * @return static[] an array of BaseCrudModel instances, or an empty array if nothing matches.
     */
    public function findAll(IBaseCrudModel $entity, $condition);

    /**
     * @param BaseCrudModel $entity
     * @param int|array $data
     */
    public function populateRecord(BaseCrudModel $entity, $data);

}