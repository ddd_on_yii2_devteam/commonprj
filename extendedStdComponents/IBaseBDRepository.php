<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 3/5/2018
 * Time: 3:57 PM
 */

namespace commonprj\extendedStdComponents;


interface IBaseBDRepository
{
    /**
     * @inheritdoc
     * @return EntityQuery
     */
    public function find(IBaseCrudModel $entity);

}