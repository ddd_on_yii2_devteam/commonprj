<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/26/2018
 * Time: 4:08 PM
 */

namespace commonprj\extendedStdComponents;

use commonprj\components\core\entities\element\Element;
use commonprj\services\search\ReturnFilter;
use yii\data\BaseDataProvider;
use yii\db\ActiveQueryInterface;
use yii\db\Connection;
use yii\db\QueryInterface;
use yii\base\InvalidConfigException;
use yii\di\Instance;

class EntityDataProvider extends BaseDataProvider
{

    /**
     * @var EntityQuery the query that is used to fetch data models and [[totalCount]]
     * if it is not explicitly set.
     */
    public $query;
    /**
     * @var string|callable the column that is used as the key of the data models.
     * This can be either a column name, or a callable that returns the key value of a given data model.
     *
     * If this is not set, the following rules will be used to determine the keys of the data models:
     *
     * - If [[query]] is an [[\yii\db\ActiveQuery]] instance, the primary keys of [[\yii\db\ActiveQuery::modelClass]] will be used.
     * - Otherwise, the keys of the [[models]] array will be used.
     *
     * @see getKeys()
     */
    public $key;
    /**
     * @var Connection|array|string the DB connection object or the application component ID of the DB connection.
     * If not set, the default DB connection will be used.
     * Starting from version 2.0.2, this can also be a configuration array for creating the object.
     */
    public $db;

    public $entityIds = [];

    /**
     * Initializes the DB connection component.
     * This method will initialize the [[db]] property to make sure it refers to a valid DB connection.
     * @throws InvalidConfigException if [[db]] is invalid.
     */
    public function init()
    {
        parent::init();
        if (is_string($this->db)) {
            $this->db = Instance::ensure($this->db, Connection::className());
        }
    }

    /**
     * Prepares the data models that will be made available in the current page.
     * @return array the available data models
     * @throws InvalidConfigException
     */
    protected function prepareModels()
    {
        if (!$this->query instanceof QueryInterface) {
            throw new InvalidConfigException('The "query" property must be an instance of a class that implements the QueryInterface e.g. yii\db\Query or its subclasses.');
        }

        /** @var EntityQuery $query */
        $query = clone $this->query;

        if (($pagination = $this->getPagination()) !== false) {
            $pagination->totalCount = $this->getTotalCount();
            if ($pagination->totalCount === 0) {
                return [];
            }
            $query->limit($pagination->getLimit())->offset($pagination->getOffset());
        }

        if (($sort = $this->getSort()) !== false) {
            $query->addOrderBy($sort->getOrders());
        }

        return $query->all();
    }

    /**
     * @inheritdoc
     */
    protected function prepareKeys($models)
    {
        $keys = [];
        if ($this->key !== null) {
            foreach ($models as $model) {
                if (is_string($this->key)) {
                    $keys[] = $model[$this->key];
                } else {
                    $keys[] = call_user_func($this->key, $model);
                }
            }

            return $keys;
        } elseif ($this->query instanceof ActiveQueryInterface) {
            /* @var $class \yii\db\ActiveRecordInterface */
            $class = $this->query->modelClass;
            $pks = $class::primaryKey();
            if (count($pks) === 1) {
                $pk = $pks[0];
                foreach ($models as $model) {
                    $keys[] = $model[$pk];
                }
            } else {
                foreach ($models as $model) {
                    $kk = [];
                    foreach ($pks as $pk) {
                        $kk[$pk] = $model[$pk];
                    }
                    $keys[] = $kk;
                }
            }

            return $keys;
        }

        return array_keys($models);
    }

    /**
     * Returns a value indicating the total number of data models in this data provider.
     * @return int total number of data models in this data provider.
     * @throws InvalidConfigException
     */
    protected function prepareTotalCount()
    {
        if (!$this->query instanceof QueryInterface) {
            throw new InvalidConfigException('The "query" property must be an instance of a class that implements the QueryInterface e.g. yii\db\Query or its subclasses.');
        }

        /** @var EntityQuery $query */
        $query = clone $this->query;

        $entityIds = $query
            ->limit(-1)
            ->offset(-1)
            ->orderBy([])
            ->column();

        $this->entityIds = $entityIds;

        return count($entityIds);
    }

    /**
     * получает массив фильтров
     * @param ReturnFilter $filter
     * @return array
     */
    public function getFilters(ReturnFilter $filter)
    {
        /** @var ElementQuery $query */
        $query = clone $this->query;
        return $query->getFilters($filter->getFilterKeys(), $this->entityIds);
    }

    /**
     * @inheritdoc
     */
    public function setSort($value)
    {
        parent::setSort($value);

        if (($sort = $this->getSort()) !== false && $this->query instanceof ActiveQueryInterface) {
            /* @var $modelClass BaseCrudModel */
            $modelClass = $this->query->modelClass;

            /** @var BaseCrudModel|Element $model */
            $model = $modelClass::instance();
            if (empty($sort->attributes)) {
                $attributes = $model->attributes();

                foreach ((array)$attributes as $attribute) {
                    $sort->attributes[$attribute] = [
                        'asc'   => [$attribute => SORT_ASC],
                        'desc'  => [$attribute => SORT_DESC],
                        'label' => $model->getAttributeLabel($attribute),
                    ];
                }

                if ($this->query->isElementQuery()) {
                    $propertiesAttributes = $model->propertiesAttributes();
                    foreach ((array)$propertiesAttributes as $attribute) {
                        $sort->attributes[$attribute] = [
                            'asc'   => ["property.{$attribute}" => SORT_ASC],
                            'desc'  => ["property.{$attribute}" => SORT_DESC],
                            'label' => $model->getAttributeLabel($attribute),
                        ];
                    }
                }


            } else {
                foreach ((array)$sort->attributes as $attribute => $config) {
                    if (!isset($config['label'])) {
                        $sort->attributes[$attribute]['label'] = $model->getAttributeLabel($attribute);
                    }
                }
            }
        }
    }

}