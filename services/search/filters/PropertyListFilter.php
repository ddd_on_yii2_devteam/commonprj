<?php

namespace commonprj\services\search\filters;

use yii\db\Query;
use Yii;

class PropertyListFilter extends AbstractFilter
{

    protected $table = 'property_list_item';
    protected $field = 'item';

    protected $operator;
    protected $propertyId;
    protected $values;
    protected $classId;

    public function renderQuery($operation, $values, $classId, $extendData = null)
    {

        $table = $this->table;
        $field = $this->field;
        $propertyId = $this->propertyId;

        $query = (new Query())
            ->select('e.element_id')
            ->from("{$table}")
            ->leftJoin('property_value2element e', "e.property_value_id = {$table}.id AND e.property_id = {$table}.property_id")
            ->leftJoin('element2element_class', 'element2element_class.element_id = e.element_id')
            ->where([
                'e.property_id'                          => $propertyId,
                'element2element_class.element_class_id' => $classId,
            ])
            ->andWhere(["{$table}.$field" => $values]);

        return $query;
    }

    public function getDataForFilter($propertyId, $propertyTypeId, $elementIds)
    {
        $table = $this->table;

        $query = (new Query())
            ->select([
                'value' => "{$table}.item",
                'label',
                'COUNT(element_id) AS count',
            ])
            ->from("{$table}")
            ->where(["{$table}.property_id" => $propertyId])
            ->leftJoin('property_value2element',
                "property_value2element.property_value_id = {$table}.id AND {$table}.property_id = property_value2element.property_id");

        if ($elementIds !== null) {
            $query->andWhere(["property_value2element.element_id" => $elementIds]);
        }

        $query->groupBy(['value', 'label']);
        $query->orderBy(['count' => SORT_DESC]);

        $result = $query->all(Yii::$app->dbSearch);

        return [
            'type'   => 'list',
            'values' => $result,
        ];
    }
}