<?php

namespace commonprj\services\search\filters;

use yii\db\Query;

class IdFilter  extends AbstractFilter
{

    protected $table = 'element2element_class';
    protected $field = 'element_id';

    protected $operator;
    protected $propertyId;
    protected $values;
    protected $classId;

    public function renderQuery($operation, $values, $classId, $extendData = null)
    {
        $table = $this->table;
        $field = $this->field;

        $query = (new Query())
            ->select("{$table}.{$field}")
            ->from("{$table}")
            ->andWhere([$operation, "{$table}.{$field}", $values]);

        return $query;
    }
}