<?php

namespace commonprj\services\search\filters;

use yii\db\Query;

class ClassIdFilter  extends AbstractFilter
{

    protected $table = 'element2element_class';
    protected $field = 'element_class_id';

    protected $operator;
    protected $propertyId;
    protected $values;
    protected $classId;

    public function renderQuery($operation, $values, $classId, $extendData = null)
    {

        $table = $this->table;
        $field = $this->field;

        $query = (new Query())
            ->select('element_id')
            ->from("{$table}")
            ->andWhere(["{$table}.$field" => $classId]);

        return $query;
    }
}