<?php

namespace commonprj\services\search\filters;

use Yii;
use yii\db\Query;

class PropertyPriceListFilter extends PropertyIntFilter
{
    protected $table = 'property_value_money';
    protected $field = 'value_in_base_currency';

    /**
     * @param $values
     * @return array|int
     */
    public function beforeQueryForValues($values)
    {
        if (is_array($values)) {
            foreach ($values as $value) {
                $result[] = $this->calcInBaseCurrency($value);
            }
        } else {
            $result = $this->calcInBaseCurrency($values);
        }

        return $result ?? [];
    }

    /**
     * @param $value
     * @return int
     */
    private function calcInBaseCurrency($value)
    {
        $currencyService = $this->getCurrencyService();
        return (int)($currencyService->calcInCurrency($value) * 100);
    }

    public function getDataForFilter($propertyId, $propertyTypeId, $elementIds = null)
    {
        $table = $this->table;

        $query = (new Query())
            ->select([
                'min(value_in_base_currency) AS min',
                'max(value_in_base_currency) AS max',
            ])
            ->from("{$table}")
            ->where(["{$table}.property_id" => $propertyId])
            ->leftJoin('property_value2element',
                "property_value2element.property_value_id = {$table}.id AND {$table}.property_id = property_value2element.property_id");

        if ($elementIds !== null) {
            $query->andWhere(["property_value2element.element_id" => $elementIds]);
        }

        $result = $query->one(Yii::$app->dbSearch);

        foreach ($result as $key => $value) {
            $price[$key] = (double)$value / 100;
        }

        return [
            'type'   => 'range',
            'values' => $price,
        ];
    }
}