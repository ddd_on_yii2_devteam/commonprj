<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 18.01.2018
 * Time: 10:15
 */

namespace commonprj\services\search;

use commonprj\components\core\entities\element\Element;
use commonprj\components\core\entities\elementClass\ElementClass;
use commonprj\components\core\entities\property\Property;
use commonprj\extendedStdComponents\ElementQuery;
use commonprj\extendedStdComponents\EntityQuery;
use commonprj\services\search\filters\IdFilter;
use Yii;
use yii\db\Query;
use yii\helpers\BaseInflector;

/**
 * Class SearchService
 * @package commonprj\services\search
 */
class SearchService
{
    /**
     * @var Element
     */
    private $entity;

    /**
     * @var int
     */
    private $classId = 0;


    public function findEntities(EntityQuery $query)
    {
        if ($query instanceof ElementQuery) {
            $entitiesIds = $this->findElementEntities($query);
        } else {
            $entitiesIds = $this->findBaseEntities($query);
        }

        return $entitiesIds;

    }

    protected function findBaseEntities(EntityQuery $entityQuery)
    {
        $query = $this->buildQuery($entityQuery);

        $query = clone $query;

        $query = $query
            ->select('id')
            ->limit($entityQuery->limit)
            ->offset($entityQuery->offset)
            ->orderBy($entityQuery->orderBy);

        return $query->column(Yii::$app->dbSearch);
    }

    protected function findElementEntities(EntityQuery $entityQuery)
    {
        $query = $this->buildQuery($entityQuery);

        $query = clone $query;

        if ($entityQuery->orderBy) {
            $sortedQuery = (new Query())
                ->select('elements.element_id')
                ->from(['elements' => $query]);

            foreach ($entityQuery->orderBy as $orderByField => $direction) {
                $filter = SearchFilter::getFilterBySysname($orderByField);

                if ($filter instanceof IdFilter) {
                    $sortedQuery = $sortedQuery
                        ->orderBy(['elements.element_id' => $direction]);

                } else {
                    $propertyId = $filter->getPropertyId();
                    $valueTable = $filter->getTable();
                    $valueField = $filter->getField();

                    $sortedQuery = $sortedQuery
                        ->leftJoin('property_value2element', "elements.element_id = property_value2element.element_id AND property_value2element.property_id = $propertyId")
                        ->leftJoin($valueTable, "$valueTable.id = property_value2element.property_value_id")
                        ->orderBy(["$valueTable.$valueField" => $direction]);
                }
            }
        } else {
            $sortedQuery = $query;
        }

        $sortedQuery
            ->limit($entityQuery->limit)
            ->offset($entityQuery->offset);

        return $sortedQuery->column(Yii::$app->dbSearch);
    }

    public function count(EntityQuery $entityQuery, $q)
    {
        $query = $this->buildQuery($entityQuery);

        return $query->count($q, $this->getDb());
    }

    public function column(EntityQuery $entityQuery)
    {
        $query = $this->buildQuery($entityQuery);

        $elementIds = $query->column(Yii::$app->dbSearch);

        return $elementIds;
    }

    /**
     * Получает массив фильтров
     * @param $propsInFilter
     * @param $classId
     * @param null $elementsIds
     * @return array
     * @throws \yii\web\HttpException
     */
    public function getFilterData($propsInFilter, $classId, $elementsIds = null)
    {

        $elementClass = ElementClass::findOne(['id' => $classId]);

        $properties = $elementClass->getProperties();
        $propertiesSysname = [];

        foreach ($properties as $property) {
            $propertiesSysname[$property->sysname] = $property;
        }

        $propertiesToFilter = array_intersect_key($propertiesSysname, array_flip($propsInFilter));

        $filters = [];
        foreach ($propertiesToFilter as $property) {

            $result = SearchFilter::getFilterData($property->id, $property->property_type_id, $elementsIds);

            $filter = [
                'name'         => $property->name,
                'sysname'      => $property->sysname,
                'propertyType' => $property->property_type_id,
                'filterType'   => $result['type'],
                'values'       => $result['values'],
            ];

            $filters[] = $filter;
        }


        return $filters;
    }

    protected function buildQuery(EntityQuery $entityQuery)
    {

        if ($entityQuery instanceof ElementQuery) {
            $query = $this->buildConditionElementQuery($entityQuery);
        } else {
            $query = $this->buildConditionEntitiesQuery($entityQuery);
        }

        return $query;
    }

    protected function buildConditionElementQuery(ElementQuery $elementQuery)
    {
        $searchFilterService = new SearchFilter((array)$elementQuery->where, $elementQuery->classId);

        $query = $searchFilterService->buildFilters();

        return $query;
    }

    protected function buildConditionEntitiesQuery(EntityQuery $entityQuery)
    {
        $query = (new Query())
            ->from($entityQuery->from);

        if ($entityQuery->where) {
            $query = $query->where($this->normalizeEntityWhere($entityQuery->where));
        }

        return $query;
    }

    protected function normalizeEntityWhere($whereCondition)
    {
        $result = [];

        foreach ((array)$whereCondition as $key => $value) {
            if (is_string($key)) {
                $newKey = $this->modifyField($key);
            } else {
                $newKey = $key;
            }

            if (is_array($value)) {
                $newValue = $this->normalizeEntityWhere($value);
            } elseif (is_string($value) && is_numeric($key) && $key < 2) {
                $newValue = $this->modifyField($value);
            } else {
                $newValue = $value;
            }

            $result[$newKey] = $newValue;
        }

        return $result;
    }

    protected function modifyField($field)
    {
        return BaseInflector::camel2id($field, '_');
    }

    /**
     * @param $entity
     * @param array $condition
     * @return bool
     * @throws \Exception
     * @throws \yii\db\Exception
     * $condition = [
     * 'propertyId' => int,
     * 'propertyValueTable' => string,
     * 'comparison' => SearchFilter->getComparison(),
     * ];
     */
    public function isSatisfied($entity, array $condition): bool
    {
        $table = $condition['propertyValueTable'];

        $query = (new Query())
            ->select('pv2e.element_id')
            ->from("{$table}")
            ->leftJoin('property_value2element pv2e', "pv2e.property_value_id = {$table}.id")
            ->leftJoin('element2element_class', 'element2element_class.element_id = pv2e.element_id')
            ->where([
                'pv2e.property_id' => $condition['propertyId'],
                'pv2e.element_id'  => $entity->id,
            ]);

        foreach ($condition['comparison'] as $comparison) {
            $comparison[1] = "{$table}.{$comparison[1]}";
            $query->andWhere($comparison);
        }

        return (bool)$query->createCommand(Yii::$app->dbSearch)->queryOne();
    }


    public function getDb()
    {
        return Yii::$app->dbSearch;
    }

}