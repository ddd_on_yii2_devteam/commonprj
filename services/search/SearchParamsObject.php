<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 19.01.2018
 * Time: 12:31
 */

namespace commonprj\services\search;

use commonprj\components\core\helpers\ClassAndContextHelper;

/**
 * Class SearchParamsObject
 * @package commonprj\services\search
 */
class SearchParamsObject
{
    /**
     * @var SearchFilter[]
     */
    private $filters = [];

    /**
     * @var array
     */
    private $sort;

    /**
     * @var int
     */
    private $page = 1;

    /**
     * @var int
     */
    private $perPage = 12;

    /**
     * @var int
     */
    private $limit;

    /**
     * @var string
     */
    private $modelClass;

    /**
     * @var array
     */
    private $relations = [];

    /**
     * @param SearchFilter $filter
     */
    public function addFilter(SearchFilter $filter)
    {
        $this->filters[] = $filter;
    }

    /**
     * @return SearchFilter[]
     */
    public function getFilters(): array
    {
        return $this->filters;
    }

    /**
     * @param array $filters
     */
    public function setFilters(array $filters): void
    {
        $this->filters = $filters;
    }

    /**
     * @return array
     */
    public function getSort(): array
    {
        return $this->sort;
    }

    /**
     * @param array $sort
     */
    public function setSort(array $sort): void
    {
        $this->sort = $sort;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage(int $page): void
    {
        $this->page = $page;
    }

    /**
     * @return int
     */
    public function getPerPage(): int
    {
        return $this->perPage;
    }

    /**
     * @param int $perPage
     */
    public function setPerPage(int $perPage): void
    {
        $this->perPage = max((int)$perPage, 1);
    }

    /**
     * @return bool
     */
    public function hasSort(): bool
    {
        if ($this->sort) {
            return true;
        }

        return false;
    }

    /**
     * @return array
     */
    public function getRelations(): array
    {
        return $this->relations;
    }

    /**
     * @param string $relation
     */
    public function addRelation(string $relation): void
    {
        $this->relations[] = $relation;
    }

    /**
     * @return bool
     */
    public function hasRelations(): bool
    {
        return (bool)count($this->relations);
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return (array)$this;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->perPage;
    }

    /**
     * @return mixed
     */
    public function getOffset()
    {
        if ($this->page <= 0) {
            $page = 1;
        } else {
            $page = $this->page;
        }

        $perPage = $this->perPage;
        return ($page - 1) * $perPage;
    }

    /**
     * @return bool
     */
    public function hasFilters(): bool
    {
        return (bool)count($this->filters);
    }

    /**
     * @return string
     */
    public function getModelClass(): string
    {
        return $this->modelClass;
    }

    /**
     * @param string $modelClass
     */
    public function setModelClass(string $modelClass): void
    {
        $this->modelClass = $modelClass;
    }

    /**
     * @return mixed
     */
    public function getModel()
    {
        return new $this->modelClass();
    }

    /**
     * @return int
     */
    public function getModelClassId()
    {
        return ClassAndContextHelper::getClassId($this->modelClass);
    }
}