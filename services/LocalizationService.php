<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 1/19/2018
 * Time: 5:42 PM
 */

namespace commonprj\services;

use commonprj\components\core\entities\property\Property;
use commonprj\components\core\entities\propertyListItem\PropertyListItem;
use commonprj\components\core\models\search\PropertyRecord;
use commonprj\components\core\models\search\PropertyValueTextRecord;
use commonprj\components\core\models\search\PropertyValueStringRecord;
use commonprj\extendedStdComponents\IBaseCrudModel;
use Yii;
use commonprj\components\core\models\i18n\SourceRecord;
use commonprj\components\core\models\i18n\StringRecord;
use commonprj\components\core\models\i18n\TextRecord;
use dosamigos\transliterator\TransliteratorHelper;
use yii\db\Expression;
use yii\db\Query;

class LocalizationService
{
    const STRING_TABLE = 'string_';
    const TEXT_TABLE = 'text_';

    private $dbMap = null;
    private $propertyService = null;
    private $sourceBySourceString = null;

    /**
     * @return void
     */
    public function __construct()
    {
        $this->propertyService = Yii::$app->propertyService;
    }

    /**
     * метод, если необходимо, созадет транслитерации на каждый язык
     * или же добавляет транслитерацию только на eng, если перевода нет
     * @param int $propertyId
     * @param string|array $value
     * @return array
     * @throws \yii\web\NotFoundHttpException
     */
    public function generateTranslationList($value, int $propertyId = null)
    {
        if ($propertyId) {
            /** @var PropertyService $propertyService */
            $propertyService = Yii::$app->propertyService;

            $propertySysname = $propertyService->getSysnameById($propertyId);

            //проверям на необходимость перевода свойства
            if ($this->noNeedTranslateProperty($propertySysname)) {
                return $value;
            }

            if (in_array($propertySysname, $this->getI18nConfig('transliterate'))) {
                $translations = $this->transliterate($value);
                $translations['transliterate'] = $translations['eng'];
                return $translations;
            }
        }

        $value = $this->normalizeValue($value);

        if (empty($value['eng'])) {
            $langToTransiterate = 'eng';

            // первоначально пытаемся найти описание на текущкем языке системы
            $currentIsoLang = Yii::$app->language;
            $existingLangs = $this->getExistingLangs();
            $currentLang = $existingLangs[$currentIsoLang] ?? null;
            $translateFromEntity = $value;
            $translateFromEntity = array_filter($translateFromEntity);

            //либо нашли, либо берем первое что есть в массиве
            $firstValue = !empty($value[$currentLang]) ?  $value[$currentLang] : reset($translateFromEntity);

            $value['transliterate'] = $this->transliterateValue($firstValue, $currentLang, $langToTransiterate);
        }

        return $value;
    }

    /**
     * Добавляет вариант перевода для источника
     * @param SourceRecord $sourceRecord
     * @param array $translations
     * @param int|null $recordsetId
     * @param int|null $propertyId
     */
    public function addLocalizationVariants(
        SourceRecord $sourceRecord,
        array $translations,
        $recordsetId = null,
        $propertyId = null
    )
    {
        if (!$this->validateSourceRecord($sourceRecord) || !$this->validateTranslations($translations)) {
            die;
        }

        $sourceId = $sourceRecord->id;
        $isString = $sourceRecord->is_string;

        foreach ($this->getExistingLangs() as $lang) {
            $translation = $translations[$lang] ?? '';
            $this->addLocalizationVariant($sourceId, $isString, $lang, $translation, $recordsetId, $propertyId);
        }
    }

    /**
     * Добавляет вариант перевода для источника
     * @param SourceRecord $sourceRecord
     * @param array $translations
     * @param int|null $recordsetId
     * @param int|null $propertyId
     */
    public function saveLocalizationVariants(
        SourceRecord $sourceRecord,
        array $translations,
        $oldRecordsetId = null,
        $recordsetId = null,
        $propertyId = null
    )
    {
        if (!$this->validateSourceRecord($sourceRecord) || !$this->validateTranslations($translations)) {
            die;
        }

        $sourceId = $sourceRecord->id;
        $isString = $sourceRecord->is_string;

        foreach ($this->getExistingLangs() as $lang) {
            $translation = $translations[$lang] ?? '';

            if ($oldRecordsetId) {
                $existRecord = $this->getLocalizationVariant($sourceId, $isString, $lang, $oldRecordsetId, $propertyId);
            }
            if ($existRecord) {
                $existRecord->value = $translation;
                $existRecord->source_recordset_id = $recordsetId;
                $existRecord->save();
            } else {
                $this->addLocalizationVariant($sourceId, $isString, $lang, $translation, $recordsetId, $propertyId);
            }

        }
    }

    public function getLocalizationVariants(
        SourceRecord $sourceRecord,
        $value,
        $recordsetId = null,
        $propertyId = null
    )
    {
        $sourceId = $sourceRecord->id;
        $isString = $sourceRecord->is_string;
        $result = [];

        foreach ($this->getExistingLangs() as $lang) {
            $existRecord = $this->getLocalizationVariant($sourceId, $isString, $lang, $recordsetId, $propertyId);
            $newValue = $value[$lang] ?? ($this->isCurrentLang($lang) ? $value : null);

            $result[$lang] = empty($newValue) ? ($existRecord->value ?? '') : $newValue;
        }

        if (is_array($value)) {
            $value = array_filter($value);
            $baseValue = reset($value);
        } else {
            $baseValue = $value;
        }

        $result['transliterate'] = empty($result['eng'])
            ? $this->transliterateValue($baseValue, 'eng', 'eng')
            : $result['eng'];//$this->transliterateValue($firstValue, $currentLang, $langToTransiterate);

        return $result;
    }

    public function getTranslatedVariants(
        $sourceRecord,
        $recordSetId,
        $propertyId = null
    )
    {
        $sourceId = $sourceRecord->id;
        $isString = $sourceRecord->is_string;

        foreach ($this->getExistingLangs() as $lang) {
            $existRecord = $this->getLocalizationVariant($sourceId, $isString, $lang, $recordSetId, $propertyId);
            $result[$lang] = $existRecord->value ?? null;
        }

        return $result ?? [];
    }

    /**
     * находит источник по параметрам
     * @param $dbName
     * @param $tableName
     * @param $fieldName
     * @param bool $isString
     * @return null|SourceRecord
     */
    public function getSource($dbName, $tableName, $fieldName, $isString = null)
    {
        $sourceRecordQuery = SourceRecord::find()
            ->where([
                'db_name'    => $dbName,
                'table_name' => $tableName,
                'field_name' => $fieldName,
            ]);

        if ($isString !== null) {
            $sourceRecordQuery
                ->andWhere(['is_string' => $isString]);
        }

        $sourceRecord = $sourceRecordQuery->one();

        return $sourceRecord;
    }

    /**
     * @param string $sourceString
     * @return SourceRecord|null
     */
    public function getSourceBySourceString($sourceString)
    {
        if (!$this->sourceBySourceString) {
            $sourceRecords = SourceRecord::find()->all();
            foreach ($sourceRecords as $sourceRecord) {
                $sourceStringKey = sprintf('%s.%s.%s', $sourceRecord->db_name, $sourceRecord->table_name, $sourceRecord->field_name);
                $this->sourceBySourceString[$sourceStringKey] = $sourceRecord;
            }
        }

        return $this->sourceBySourceString[$sourceString] ?? null;
    }

    /**
     * @param IBaseCrudModel $entity
     * переводит базовые параметры сущности
     */
    public function translateEntity(IBaseCrudModel $entity)
    {
        $sourceTable = $entity->repository->activeRecordClass::tableName();
        $sourceRecordsetId = $entity->searchdbPropertyValueId ?? $entity->id;

        $propertyId = $entity->propertyId ?? null;

        if ($entity instanceof PropertyListItem) {
            $propertyId = $entity->propertyId;
        }

        foreach ($entity->getOldAttributes() as $field => $_) {

            $translatedValue = $this->translateEntityValue($sourceTable, $sourceRecordsetId, $field, $propertyId ?? null);

            // Если есть перевод - устанавливаем перевод
            if ($translatedValue) {
                $entity->setOldAttribute($field, $translatedValue);
                $entity->{$field} = $translatedValue;
            }
        }

    }

    /**
     * @param IBaseCrudModel $entity
     * переводит свойства у сущности
     */
    public function translateEntityProperties(IBaseCrudModel $entity)
    {
        $field = 'value';

        $properties = $entity->getOldProperties();

        foreach ($properties as $key => $property) {

            $sysname = $property->sysname;

            if ($this->noNeedTranslateProperty($sysname)) {
                continue;
            }
            $sourceRecordsetId = $property->getSearchdbPropertyValueId();

            $source = $this->getPropertySourceByProperty($property);
            $sourceTable = $source->table_name;
            $propertyId = $property->id;

            $translatedValue = $this->translateEntityValue($sourceTable, $sourceRecordsetId, $field, $propertyId);

            if ($translatedValue) {
                $property->setOldAttribute($field, $translatedValue);
                $property->{$field} = $translatedValue;
            }
        }

        $entity->properties = array_values($properties);
    }

    /**
     * @param $sourceTable
     * @param $sourceRecordsetId
     * @param $fieldName
     * @param null $propertyId
     * @return null|string
     */
    public function translateEntityValue($sourceTable, $sourceRecordsetId, $fieldName, $propertyId = null)
    {
        $index = sprintf("%s.%s.%s.%s",
            'search',
            $sourceTable,
            $fieldName,
            $sourceRecordsetId
        );

        if ($propertyId) {
            $sysname = $this->propertyService->getSysnameById($propertyId);

            if ($this->noNeedTranslateProperty($sysname)) {
                return null;
            }
            $index .= '.' . $propertyId;
        }
        $translate = Yii::t('entity', $index);

        return $translate != $index ? $translate : null;
    }

    /**
     * Возвращает переведенное значение по параметрам источника и его данным, так же проверяет необходисомть перевода свойств
     * @param $dbName
     * @param $tableName
     * @param $fieldName
     * @param $sourceRecordsetId
     * @param null $propertyId
     * @return null|string
     */
    public function translate($dbName, $tableName, $fieldName, $sourceRecordsetId, $propertyId = null)
    {
        $index = sprintf("%s.%s.%s.%s",
            $dbName,
            $tableName,
            $fieldName,
            $sourceRecordsetId
        );

        if ($propertyId) {
            $sysname = $this->propertyService->getSysnameById($propertyId);
            if ($this->noNeedTranslateProperty($sysname)) {
                return null;
            }
            $index .= '.' . $propertyId;
        }

        $translate = Yii::t('app', $index);
        return $translate != $index ? $translate : null;
    }


    /**
     * Возвращает массив переводов для указанного языка
     * @param string $language
     * @return array
     */
    public function getTranslationData($language)
    {
        StringRecord::$tableName = 'string_' . $language;
        TextRecord::$tableName = 'text_' . $language;

        $records = SourceRecord::find()
            ->with('string')
            ->with('text')
            ->all();

        $data = [];
        foreach ($records as $source) {
            foreach (array_merge($source->string, $source->text) as $translateRecord) {
                $key = sprintf("%s.%s.%s.%s",
                    $source->db_name,
                    $source->table_name,
                    $source->field_name,
                    $translateRecord->source_recordset_id
                );
                if ($translateRecord->property_id) {
                    $key .= '.' . $translateRecord->property_id;
                }
                $data[$key] = $translateRecord->value;
            }
        }

        StringRecord::$tableName = null;
        TextRecord::$tableName = null;
        return $data;
    }

    /**
     * Возвращает массив переводов "по умолчанию" - берет данные из самой сущности лежащей вне базы переводов
     * @return array
     */
    public function getDefaultTranslationData()
    {
        $data = [];
        $sources = SourceRecord::find()
            ->all();

        $propertyToTranslateIds = $this->getTranslationPropertyIds();

        foreach ($sources as $sourceRecord) {
            $query = (new \yii\db\Query())
                ->select(['id'])
                ->addSelect(['value' => $sourceRecord->field_name])
                ->from($sourceRecord->table_name);

            if (in_array($sourceRecord->table_name, [
                PropertyValueTextRecord::tableName(),
                PropertyValueStringRecord::tableName(),
            ])) {
                $query = $query
                    ->addSelect('property_id')
                    ->where(['property_id' => $propertyToTranslateIds]);
            } else {
                continue;
            }

            $results = $query->all($this->getDBComponentByDBName($sourceRecord->db_name));

            foreach ($results as $result) {
                $key = sprintf("%s.%s.%s.%s",
                    $sourceRecord->db_name,
                    $sourceRecord->table_name,
                    $sourceRecord->field_name,
                    $result['id']
                );

                if (isset($result['property_id'])) {
                    $key .= '.' . $result['property_id'];
                }

                $data[$key] = $result['value'];
            }
        }

        return $data;
    }

    /**
     * Получение всех не переведенных значений
     * @param null $lang
     * @param null $source
     * @return array
     */
    public function getUntranslatedItems($lang = null, $source = null)
    {
        $sourceRecords = SourceRecord::find()
            ->all();

        $untranslated = [];

        foreach ($sourceRecords as $sourceRecord) {
            $query = (new Query())
                ->from($sourceRecord->table_name);

            $query->select(['source_recordset_id' => 'id']);
            $query->addSelect(['source_id' => new Expression("'" . $sourceRecord->id . "'")]);

            //если требуется перевод строк или текста убираем непереводимые данные
            if (in_array($sourceRecord->table_name, [
                PropertyValueStringRecord::tableName(),
                PropertyValueTextRecord::tableName(),
            ])) {
                $translationFreePropertyIds = $this->getTranslationFreePropertyIds();
                $query->addSelect(['property_id' => 'property_id']);
                $query->andWhere(['not in', 'property_id', $translationFreePropertyIds]);
            }

            $sourceRecordsetIds = $query->all(
                $this->getDBComponentByDBName($sourceRecord->db_name)
            );

            if ($sourceRecordsetIds) {

                $itemsQuery = $this->getItemsQuery($sourceRecord, $sourceRecordsetIds);

                $result = $itemsQuery
                    ->all($this->getDBComponentByDBName('i18n'));
                $untranslated = array_merge($untranslated, $result);
            }
        }

        return $untranslated;
    }

    /**
     * Добавление варианта перевода напрямую (по данным полученым из метода getUntranslatedItems)
     * @param $sourceId
     * @param $sourceRecordsetId
     * @param null $propertyId
     * @param $translation
     * @param $lang
     * @return bool
     */
    public function addTranslation($sourceId, $sourceRecordsetId, $propertyId = null, $translation, $lang)
    {
        $source = SourceRecord::findOne($sourceId);

        if ($source) {
            $resul = $this->addLocalizationVariant($sourceId, $source->is_string, $lang, $translation, $sourceRecordsetId, $propertyId);
        }

        return $resul ?? false;
    }

    /**
     * @param $condition
     * @param $table
     * @param null $propertySysname
     * @return array
     */
    public function findEntities($condition, $table, $propertySysname = null)
    {
        // Если текущий язык английский, поиск по переводам не делаем - считаем, что это обычный поиск по значению
        if ($this->isCurrentLang()) {
            return null;
        }

        if ($propertySysname) {
            //Если свойство не переводимое - не ищем в переводах
            if (!$this->isTranslatableProperty($propertySysname)) {
                return null;
            }

            $property = Property::find()
                ->where(['sysname' => $propertySysname])
                ->one();

            $source = $this->getPropertySourceByProperty($property);
        } else {
            $field = $condition[1];
            $source = $this->getSource('search', $table, $field);
        }

        if (!$source) {
            return null;
        }

        $currentLang = $this->getCurrentLang();

        $record = $source->is_string ? $this->getStringBlank($currentLang) : $this->getTextBlank($currentLang);

        $newCondition = $condition;

        if (strtolower($newCondition[0]) == 'like') {
            $newCondition[0] = 'ilike';
        }
        $newCondition[1] = 'value';

        $query = $record::find()
            ->where(['source_id' => $source->id])
            ->indexBy('source_recordset_id')
            ->andWhere($newCondition);

        if (isset($property)) {
            $query = $query
                ->andWhere(['property_id' => $property->id]);
        }
        $result = $query->all();
        $elementIds = array_keys($result);

        if ($currentLang == 'eng') {
            $newCondition[1] = $source->field_name;
            $query = (new Query())
                ->from($source->table_name)
                ->where($newCondition)
                ->indexBy('id');

            $searchDbResult = $query->all(Yii::$app->dbSearch);

            $elementIds = array_unique(array_merge($elementIds, array_keys($searchDbResult)));
        }

        return $elementIds;
    }

    /**
     * @param $property
     * @param null $lang
     * @return null|string
     */
    public function getSearchTableByProperty($property, $lang = null)
    {
        if ($this->noNeedTranslateProperty($property->sysname)) {
            return null;
        };

        $source = $this->getPropertySourceByProperty($property);
        $existingLangs = $this->getExistingLangs();

        $lang = $lang ?? $existingLangs[Yii::$app->language];

        $record = $source->is_string ? $this->getStringBlank($lang) : $this->getTextBlank($lang);
        return $record::tableName();
    }

    /**
     * @param $property
     * @return SourceRecord|null
     */
    public function getPropertySourceByProperty($property)
    {
        return $this->getPropertySourceByPropertyType($property->propertyTypeId);
    }

    /**
     * Возвращает источник по типу свойства
     * @param $propertyTypeId
     * @return SourceRecord|null
     */
    public function getPropertySourceByPropertyType($propertyTypeId)
    {
        $tableName = $propertyTypeId == Property::PROPERTY_TYPE_STRING ? 'property_value_string' : 'property_value_text';

        return $this->getSource('search', $tableName, 'value');
    }

    /**
     * проверяет свойство на наличие в списке не переводимых
     * @param string $propertySysname
     * @return bool
     */
    private function noNeedTranslateProperty($propertySysname)
    {
        return in_array($propertySysname, $this->getI18nConfig('skipTransalte'));
    }

    public function isTranslatableProperty($propertySysname)
    {
        return !$this->noNeedTranslateProperty($propertySysname);
    }

    /**
     * Возвращает трехбуквенное значение текущего языка
     * @return string
     */
    public function getCurrentLang()
    {
        $currentIsoLang = Yii::$app->language;
        $existingLangs = $this->getExistingLangs();
        return $existingLangs[$currentIsoLang] ?? $this->getDefaultLang();
    }

    public function getDefaultIsoLang()
    {
        return 'eng-US';
    }

    public function getDefaultLang()
    {
        return 'eng';
    }

    /**
     * возвращает текущий язык в формате ***-**
     * @return string
     */
    public function getCurrentIsoLang()
    {
        return Yii::$app->language;
    }

    public function isCurrentLang($lang = null)
    {
        $lang = $lang ?? $this->getDefaultLang();
        return $this->getCurrentLang() == $lang;
    }

    /**
     * Устанавливает язык приложения
     */
    public function setCurrentLang($lang = null)
    {
        $existingLangs = $this->getExistingLangs();

        if (isset($existingLangs[$lang])) {
            Yii::$app->language = $lang;
        }

    }

    /**
     * @return array
     */
    private function getTranslationFreePropertyIds()
    {
        $result = [];
        foreach (array_merge(
                     $this->getI18nConfig('skipTransalte'),
                     $this->getI18nConfig('transliterate')) as $item) {
            $result[$item] = $this->propertyService->getIdBySysname($item);
        }

        return array_filter($result);
    }

    /**
     * @return array
     */
    private function getTranslationPropertyIds()
    {
        $result = PropertyRecord::find()
            ->select('property.id')
            ->joinWith('propertyType')
            ->where(['property_type.parent_id' => Property::PROPERTY_TYPE_PROPERTY_ID])
            ->indexBy('sysname')
            ->column();

        foreach (array_merge(
                     $this->getI18nConfig('skipTransalte'),
                     $this->getI18nConfig('transliterate')) as $item) {
            unset($result[$item]);
        }

        return array_filter($result);
    }

    /**
     * @param $sourceRecord
     * @param $sourceRecordsets
     * @return Query
     */
    private function getItemsQuery($sourceRecord, $sourceRecordsets)
    {
        $fromArr = [];
        $fromArrKeys = null;
        foreach ($sourceRecordsets as $sourceRecordset) {
            if (!$fromArrKeys) {
                $fromArrKeys = array_keys($sourceRecordset);
            }
            $fromArr[] = sprintf("(%s)", implode(', ', $sourceRecordset));
        }

        $from = sprintf("(VALUES %s) as t(%s)",
            implode(', ', $fromArr),
            implode(', ', $fromArrKeys));

        $query = (new Query())
            ->from([$from]);

        $leftJoinArr = [];
        foreach ($fromArrKeys as $fromArrKey) {
            $query = $query->addSelect('t.' . $fromArrKey);
            $leftJoinArr[] = sprintf("%%s.%s = t.%s", $fromArrKey, $fromArrKey);
        }

        $tablePrefix = $sourceRecord->is_string ? self::STRING_TABLE : self::TEXT_TABLE;

        foreach ($this->getExistingLangs() as $existingLang) {
            $tableAlias = "t_" . $existingLang;
            $leftJoinWithTableNameArr = [];
            foreach ($leftJoinArr as $_) {
                $leftJoinWithTableNameArr[] = sprintf($_, $tableAlias);
            }

            $leftJoinOn = implode(' AND ', $leftJoinWithTableNameArr);
            $query
                ->addSelect([$existingLang => $tableAlias . ".value"])
                ->leftJoin([$tablePrefix . $existingLang . " as  " . $tableAlias], $leftJoinOn)
                ->orWhere(new Expression(sprintf("coalesce(\"%s\".\"value\", '') = ''", $tableAlias)));
        }

        return $query;
    }

    /**
     * Возвращает название компонента подключения к БД по ее названию
     * @param string $name
     * @return string
     */
    private function getDBComponentByDBName($name)
    {
        if (!$this->dbMap) {
            $components = Yii::$app->getComponents();

            foreach ($components as $key => $component) {
                $componentClass = $component['class'] ?? null;
                if ($componentClass == 'yii\db\Connection') {
                    $dbName = self::getDsnAttribute('dbname', $component['dsn']);
                    $this->dbMap[$dbName] = $key;
                }
            }
        }

        return Yii::$app->{$this->dbMap[$name]};
    }

    /**
     * возвращает название базы по имени компонента-подключения
     * @param string $name
     * @param string $dsn
     * @return null|string
     */
    private static function getDsnAttribute($name, $dsn)
    {
        if (preg_match('/' . $name . '=([^;]*)/', $dsn, $match)) {
            return $match[1];
        } else {
            return null;
        }
    }

    /**
     * преобразует полученное значение в массив по языкам
     * @param string|array $value
     * @return array
     */
    private function normalizeValue($value)
    {
        $currentLang = $this->getCurrentLang();

        $result = [];

        if (!is_array($value)) {
            $result[$currentLang] = $value;
        } else {
            $result = $value;
        }

        return $result;
    }

    /**
     * @param $valueData
     * @return array
     */
    protected function transliterate($valueData)
    {
        $result = [];

        if (is_array($valueData)) {
            $resultData = $valueData;
            $value = $valueData[$this->getCurrentLang()] ?? reset($valueData);
        } else {
            $value = $valueData;
            $resultData[$this->getCurrentLang()] = $valueData;
        }

        foreach ($this->getExistingLangs() as $isoLang => $lang) {
            $valueFromData = $resultData[$lang] ?? null;

            $result[$lang] = !empty($valueFromData) ? $valueFromData : $this->transliterateValue($value, $lang, $lang);
        }

        return $result;
    }

    /**
     * транслитерирует значение
     * список транслитераторов intl transliterator_list_ids()
     * @param $value
     * @param $lang
     * @param $isoLang
     * @return string
     */
    private function transliterateValue($value, $lang, $langToTransliterate)
    {
        $intlMapping = $this->getI18nConfig('intlMapping');
        $isoLang = $intlMapping[$langToTransliterate];

        // If intl extension load
        if (extension_loaded('intl') === true) {
            $options = $isoLang;
            $transliterate = transliterator_transliterate($options, $value);
        } else {
            $transliterate = TransliteratorHelper::process($value, '', $isoLang);
        }

        return $transliterate;
    }

    /**
     * @param $sourceId
     * @param $isString
     * @param $lang
     * @param $translation
     * @param $recordset_id
     * @param $property_id
     * @return bool
     */
    private function addLocalizationVariant($sourceId, $isString, $lang, $translation, $recordset_id, $property_id)
    {

        $record = $isString ? $this->getStringBlank($lang) : $this->getTextBlank($lang);
        $params = [
            'source_id'           => $sourceId,
            'source_recordset_id' => $recordset_id,
            'property_id'         => $property_id ?? 0,
        ];

        $existRecord = $record->findOne($params);

        if (!$existRecord) {
            $params['value'] = $translation;
            $record->setAttributes($params);
            $result = $record->save();
        } else {
            $existRecord->setAttribute('value', $translation);
            $result = $existRecord->save();
        }

        return $result ?? false;
    }

    /**
     * @param $sourceId
     * @param $isString
     * @param $lang
     * @param $recordset_id
     * @param $property_id
     * @return mixed
     */
    private function getLocalizationVariant($sourceId, $isString, $lang, $recordset_id, $property_id)
    {
        $record = $isString ? $this->getStringBlank($lang) : $this->getTextBlank($lang);
        $params = [
            'source_id'           => $sourceId,
            'source_recordset_id' => $recordset_id,
            'property_id'         => $property_id ?? 0,
        ];

        $existRecord = $record->findOne($params);

        return $existRecord;
    }

    /**
     * @param $lang
     * @return StringRecord
     */
    private function getStringBlank($lang)
    {
        $recordTableName = self::STRING_TABLE . $lang;
        StringRecord::$tableName = $recordTableName;

        return new StringRecord();
    }

    /**
     * @param $lang
     * @return TextRecord
     */
    private function getTextBlank($lang)
    {
        $recordTableName = self::TEXT_TABLE . $lang;
        TextRecord::$tableName = $recordTableName;

        return new TextRecord();
    }

    /**
     * @param SourceRecord $sourceRecord
     * @return bool
     */
    private function validateSourceRecord(SourceRecord $sourceRecord)
    {
        return true;
    }

    /**
     * @param array $translations
     * @return bool
     */
    private function validateTranslations(array $translations)
    {
        return true;
    }

    /**
     * @param null $key
     * @return array
     */
    private function getI18nConfig($key = null)
    {
        $i18nConfig = Yii::$app->params['i18n'];
        return $key ? $i18nConfig[$key] : $i18nConfig;
    }

    /**
     * @return array
     */
    public function getExistingLangs()
    {
        $result = [];
        foreach ($this->getI18nConfig('existingLangs') as $lang) {
            list($code, $country) = explode('-', $lang);
            $result[$lang] = $code;
        }

        return $result;
    }

}