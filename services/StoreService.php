<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 4/3/2018
 * Time: 12:03 PM
 */

namespace commonprj\services;

use commonprj\components\core\entities\property\Property;
use commonprj\components\crm\entities\employee\Employee;
use commonprj\components\crm\entities\manufacturer\Manufacturer;
use Yii;
use yii\web\NotFoundHttpException;

class StoreService
{
    protected $session = null;

    protected $sessionFields = [
        'id'       => '0',
        'userId'   => 'admin@admin.com',
        'language' => 'eng-US',
        'country'  => 'us',
        'currency' => 'USD',
        'company'  => 'Furni.00000',
    ];

    public function __construct()
    {
        $this->session = Yii::$app->session;
        $this->session->setTimeout(86400);
    }

    public function getData()
    {

        foreach ($this->sessionFields as $key => $defaultValue) {

            if (!$this->session->get($key)) {

                if (method_exists($this, $method = "get" . ucfirst($key))) {
                    $value = call_user_func([$this, $method], $defaultValue);
                } else {
                    $value = $defaultValue;
                }

                $this->set($key, $value);
            }

            $result[$key] = $this->session->get($key);
        }

        return $result;
    }

    public function setData($data)
    {

        foreach ($this->sessionFields as $key => $defaultValue) {
            $dataValue = $data[$key] ?? null;

            if ($dataValue) {

                if (method_exists($this, $method = "get" . ucfirst($key))) {
                    $value = call_user_func([$this, $method], $dataValue);
                } else {
                    $value = $dataValue;
                }

                if ($value) {
                    $this->set($key, $value);
                }
            }
        }
    }

    public function getCurrentLanguageCode($defaultValue)
    {
        $language = $this->get('language');

        return $language['item'] ?? $language ?? $defaultValue;
    }

    public function getCurrentCountryCode($defaultValue)
    {
        $country = $this->get('country');

        return $country['item'] ?? $defaultValue;
    }

    public function getCurrentCurrencyCode($defaultValue)
    {
        $currency = $this->get('currency');

        return $currency['item'] ?? $defaultValue;
    }

    public function getCurrentCompany($defaultValue = null)
    {
        $company = $this->get('company');

        return $company;
    }

    public function getCurrentCompanyId($defaultValue = null)
    {
        $company = $this->getCurrentCompany();

        if (!$company) {
            $company = $this->getCompany($this->sessionFields['company']);
        }

        return $company->id;
    }

    public function getCurrentUserId($defaultValue = null)
    {
        $userId = $this->get('userId');

        return $userId;
    }

    private function set($key, $value)
    {
        $this->session->set($key, $value);
    }

    private function get($key, $defaultValue = null)
    {
        if (!$this->session->get($key)) {
            $this->set($key, $defaultValue);
        }

        return $this->session->get($key, $defaultValue);
    }

    private function getLanguage($itemValue)
    {
        //запоминаем текущий язык
        $oldValue = $this->get('laguage');

        $itemValue = is_array($itemValue) ? $itemValue['item'] : $itemValue;
        $currentValue = $this->get('language');
        $currentItemValue = is_array($currentValue) ? $currentValue['item'] : $currentValue;

        if ($itemValue != $currentItemValue) {
            //устанавливаем возможный язык для получения данных в этом языке
            $this->set('language', $itemValue);

            $response = Property::findAll(['sysname__eq' => 'language']);
            $propertyData = $response['items'][0] ?? null;
            $property = (new Property($propertyData));
            $listData = $property->getListItems();

            foreach ($listData as $listDatum) {
                if ($listDatum['item'] == $itemValue) {
                    list($language, $country) = explode('-', $listDatum['item']);
                    $listDatum['language'] = $language;
                    return $listDatum;
                }
            }
            //если не удалось найти язык, устанавливаем как было
            $this->set('language', $oldValue);
            throw new NotFoundHttpException("lang {$itemValue} not found");
        }

        return $currentValue;
    }

    private function getCountry($itemValue)
    {
        $itemValue = is_array($itemValue) ? $itemValue['item'] : $itemValue;
        $currentValue = $this->get('country');
        $currentItemValue = is_array($currentValue) ? $currentValue['item'] : $currentValue;

        if ($itemValue != $currentItemValue) {

            $response = Property::findAll(['sysname__eq' => 'country']);
            $propertyData = $response['items'][0] ?? null;
            $property = (new Property($propertyData));

            $listData = $property->getListItems();

            foreach ($listData as $listDatum) {
                if ($listDatum['item'] == $itemValue) {

                    return $listDatum;
                }
            }

            throw new NotFoundHttpException("country {$itemValue} not found");
        }

        return $currentValue;
    }

    private function getCurrency($itemValue)
    {
        $itemValue = is_array($itemValue) ? $itemValue['item'] : $itemValue;
        $currentValue = $this->get('currency');
        $currentItemValue = is_array($currentValue) ? $currentValue['item'] : $currentValue;

        if ($itemValue != $currentItemValue) {
            $response = Property::findAll(['sysname__eq' => 'currency']);
            $propertyData = $response['items'][0] ?? null;

            $property = (new Property($propertyData));

            $listData = $property->getListItems();

            foreach ($listData as $listDatum) {
                if ($listDatum['item'] == $itemValue) {
                    return $listDatum;
                }
            }

            throw new NotFoundHttpException("currency {$itemValue} not found");
        }

        return $currentValue;

    }

    private function getCompany($itemValue)
    {
        $itemValue = is_array($itemValue) ? $itemValue['id'] : $itemValue;
        $currentValue = $this->get('company');
        $currentItemValue = is_object($currentValue) ? $currentValue->id : $currentValue;

        if ($itemValue != $currentItemValue) {
            if (is_numeric($itemValue)) {
                $params = ['id__eq' => $itemValue];
            } else {
                $params = ['organizationCode__eq' => $itemValue];
            }

            $response = Manufacturer::findAll($params);
            $manufacturer = $response['items'][0] ?? null;

            return $manufacturer;
        }

        return $currentValue;
    }

    private function getUserId($itemValue)
    {
        $currentValue = $this->get('userId');

        if ($itemValue != $currentValue) {
            if (is_numeric($itemValue)) {
                $employeeId = $itemValue;
            } else {
                $params = ['email__eq' => $itemValue];

                $response = Employee::findAll($params);
                $employee = $response['items'][0] ?? null;
                $employeeId = $employee->id ?? null;
            }

            return $employeeId;
        }


        return $currentValue;
    }

    private function getId($itemValue)
    {
        return $this->session->getId();
    }
}