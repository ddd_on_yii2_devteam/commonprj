<?php

namespace commonprj\modules\media;

use yii\base\Widget;

class ImageWidget extends Widget
{

    public $model;
    public $recognizeColors = false;

    public function init() {
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function run() {
        return $this->render('widget', [
            'model'           => $this->model,
            'recognizeColors' => $this->recognizeColors,
        ]);
    }

}

?>