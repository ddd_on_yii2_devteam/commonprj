<?php

namespace commonprj\modules\media\controllers;

use Yii;
use yii\rest\ActiveController;
use yii\web\UploadedFile;
use commonprj\modules\media\models\Image;
use commonprj\modules\media\ImageWidget;

/**
 * Class ImageController
 * @package commonprj\modules\media\controllers
 */
class ImageController extends ActiveController
{

    public $fields = [];
    public $modelClass = Image::class;

    public function beforeAction($event) {
        $model = $this->getModel();
        foreach ($model->fields() as $field) {
            $this->fields[$field] = isset($this->module->{$field}) ? $this->module->{$field} : null;
        }
        return parent::beforeAction($event);
    }

    public function actions() {
        return [];
    }

    public function actionView() {
        Yii::$app->response->format = yii\web\Response::FORMAT_HTML;
        $model = $this->getModel();
        return ImageWidget::widget([
                    'model'           => $model,
                    'recognizeColors' => $this->module->recognizeColors
        ]);
    }

    public function actionCreate() {
        $model = $this->getModel();
        $model->file = UploadedFile::getInstance($model, 'file');
        if ($data = $model->upload(Yii::$app->request->post('Image'))) {
            return json_encode($data);
        }
        return false;
    }

    public function actionDelete($id) {
        $model = $this->getModel();
        if ($data = $model->delete($id)) {
            return json_encode($data);
        }
    }

    private function getModel() {
        return new $this->modelClass($this->fields);
    }

}
