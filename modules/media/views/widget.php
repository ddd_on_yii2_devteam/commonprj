<?php

use yii\widgets\ActiveForm;

$form = ActiveForm::begin([
    'action'  => ['image/create'],
    'options' => [
        'enctype' => 'multipart/form-data',
    ]
]);
echo $form->field($model, 'file')->fileInput()->label(false);
echo $recognizeColors ? $form->field($model, 'recognizeColors')->hiddenInput(['value' => $recognizeColors])->label(false) : '';
?>

    <button>Submit</button>

<?php ActiveForm::end() ?>